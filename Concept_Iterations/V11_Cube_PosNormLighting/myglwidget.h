// myglwidget.h

#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGlShaderProgram>
#include <QOpenGLFunctions>
#include <QMatrix4x4>


class MyGLWidget : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit MyGLWidget(QWidget *parent = 0);
    ~MyGLWidget();
signals:

public slots:

protected:
            void initializeGL();
            void paintGL();
            void resizeGL(int width, int height);

            virtual QSize sizeHint() const { return QSize(500, 500); }

            virtual void mousePressEvent(QMouseEvent *event);
            virtual void mouseMoveEvent(QMouseEvent *event);
            virtual void mouseDoubleClickEvent(QMouseEvent* pEvent );
            QVector3D    computeNormals( QVector3D p0, QVector3D p1, QVector3D p2 );
            void         collectData( GLfloat pglfData[][3], int iIndex, QVector<QVector3D>& rDataVec );

private:

    QOpenGLVertexArrayObject    m_QVAO;
    QOpenGLBuffer               m_fPositionBuffer;
    QOpenGLBuffer               m_fNormalBuffer;
    QOpenGLBuffer               m_iIndexBuffer;
    QOpenGLShaderProgram        m_Program;

    GLuint                      _positionAttr;
    GLuint                      _matrixAttr;
    GLuint                      _normalAttr;

    QMatrix4x4                  m_Projection;
    QMatrix4x4                  m_ModelView;

    GLsizei                     m_gliSize;
    int                         m_iXRot;
    int                         m_iYRot;
    int                         m_iZRot;
    QPoint                      m_qptLastPos;
    QPoint                      m_qptDblClickPos;
    QSize                       m_ViewportSize;

    int                         m_iColorToggle;

   void setXRotation(int iAngle);
   void setYRotation(int iAngle);
   void setZRotation(int iAngle);
   static void normalizeAngle(int& riAngle);
   QVector3D calculateRayCast( QPoint& rqptDblClickPos );
   bool      intersectsBoundingSphere( QVector3D& rfRayWorld );

};

#endif // MYGLWIDGET_H

