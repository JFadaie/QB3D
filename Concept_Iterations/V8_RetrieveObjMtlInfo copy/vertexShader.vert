#version 330

in vec3 position;
uniform mat4 mvpMatrix;
uniform int toggle;

out vec3 color;

void main( void )
{
    gl_Position = mvpMatrix*vec4( position, 1.0 );
    //toggle = 1;

    if (toggle == 0) {
        //color = vec3( position.x + 0.5, 1.0, position.y + 0.5 );
        color = vec3( 0.5, 0.5, 0.5 );
    } else {
        //color = vec3( position.x + 0.1, 0.1, position.y + 0.9 );
        color = vec3( position.x + 0.5, 1.0, position.y + 0.5 );
    }

}
