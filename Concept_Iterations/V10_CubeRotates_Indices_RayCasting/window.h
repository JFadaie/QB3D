// window.h

#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QSlider>
#include <QVBoxLayout>


class Window : public QWidget
{
    Q_OBJECT

public:
    explicit Window(QWidget *parent = 0);
    ~Window();
    virtual    QSize sizeHint() const;


protected:
    void keyPressEvent(QKeyEvent *event);

private:
    QVBoxLayout* m_pVBoxLayout;
};

#endif // WINDOW_H
