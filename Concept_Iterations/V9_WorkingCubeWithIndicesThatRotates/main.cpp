// main.cpp

#include "window.h"
#include <assert.h>

#include <QApplication>
#include <QDesktopWidget>
#include <QGLFormat>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include <QVector>
#include <QList>




#define     OBJ_FILE    "/Users/joshfadaie/BlenderFiled/ColorCube_Trial1.obj"
#define     MTL_FILE    "/Users/joshfadaie/BlenderFiled/ColorCube_Trial1.mtl"

// Forward declarations.
void getOBJLineCount( QString& rqsLine );
void forEachOBJLine( QString& qsFile, void(*processOBJLine)(QString&) );
void getOBJLineData( QString& rqsLine ) ;
void getMTLData();
void cleanUp();

// Contains a count of objects data.
typedef struct ObjectDataInfo {
    int iVertices;
    int iPositions;
    int iTexels;
    int iNormals;
    int iFaces;
} ObjDataInfo;

QVector< QList<float>* > fPosListVector;
QVector< QList<float>* > fTexelsListVector;
QVector< QList<float>* > fNormalsListVector;
QVector< QList<int>* > iFacesListVector;

ObjDataInfo sDataCount = {0, 0, 0, 0, 0};


/*************************
 * Description:
 *
 *
 */
void getOBJLineCount(QString& rqsLine) {

    QString qsType = rqsLine.mid( 0 , 2 );
    qDebug() << qsType;

    if( qsType.compare("v ") == 0 )
        sDataCount.iPositions++;
    else if( qsType.compare("vt") == 0 )
        sDataCount.iTexels++;
    else if( qsType.compare("vn") == 0 )
        sDataCount.iNormals++;
    else if( qsType.compare("f ") == 0 )
        sDataCount.iFaces++;


   sDataCount.iVertices = sDataCount.iFaces*3;
}


/*************************
 * Description: Reads the data, line by line, from the Object,
 *              (.obj) File provided by the macro OBJ_FILE. Each
 *              line is processed until the end of the file. The data
 *              is stored in a buffer if it is valid. If the file does
 *              not open, the function returns without doing anything and
 *              provides a debug statement to the console.
 *
 * Parameters:
 *      qsFile: QString representing the file to be opened.
 *      processLine(QString& ): function pointer. Function applied to each
 *                              line of the file.
 */

void forEachOBJLine( QString& qsFile, void(*processOBJLine)(QString&) ) {

    QFile objFile( qsFile );

    if ( !objFile.open( QIODevice::ReadWrite | QIODevice::Text )) {
         qDebug() << "The File did not open successfully... returning.";
         return;
    }

    QTextStream inStream( &objFile );


    // Read each line of text until the end of the file.
    while( !inStream.atEnd() ) {
        QString qsLine = inStream.readLine();
        qDebug() << qsLine;
        if ( !qsLine.isNull() ) {
            processOBJLine( qsLine );
        }
    }

    objFile.close();
}


/*************************
 * Description: Processes the line of data provided token by token,
 *              which are seperated by whitespace. If the data contained
 *              in each specifier is valid it is stored in the appropriate
 *              data buffer.
 *
 * Parameters:
 *      rqsLine: The QString reference that is processed. The line
 *               cannot be null, but
 */

void getOBJLineData( QString& rqsLine ) {

    assert( !rqsLine.isNull() );

    QString qsType = rqsLine.mid( 0 , 2 );
    QString qsData = rqsLine.mid( 2 );
    qsData = qsData.trimmed();


    if(  qsType.compare("v ") == 0 ||
         qsType.compare("vt") == 0 ||
         qsType.compare("vt") == 0 ) {
        QList<float>* pOBJData = new QList<float>;
        QStringList qsList = qsData.split( " " );
        QStringListIterator i( qsList );

        while( i.hasNext()) {
             float fValue = i.next().toFloat();
             qDebug() << fValue;
             pOBJData->append( fValue );
        }

        if( qsType.compare("v ") == 0 )
            fTexelsListVector << pOBJData;
        else if( qsType.compare("vt") == 0 )
            fPosListVector << pOBJData;
        else if( qsType.compare("vn") == 0 )
            fNormalsListVector << pOBJData;

    } else if( qsType.compare("f ") == 0 ) {
        QList<int>* pOBJData = new QList<int>;

        // Split data into list of v/vt/vn first
        QStringList qsList = qsData.split( " " );
        QStringListIterator i( qsList );

        while( i.hasNext()) {
            QString qsFaceData = i.next();
            qDebug() << qsFaceData;
            QStringList qsList = qsFaceData.split( "/" );
            QStringListIterator i( qsList );

            while (i.hasNext() ) {
                int iValue = i.next().toInt();
                qDebug() << iValue;
                pOBJData->append( iValue );
            }
        }
        iFacesListVector << pOBJData;
    }
}


/*************************
 * Description:
 *
 *
 * Parameters:
 *
 *
 */
void getMTLData() {



}


/*************************
 * Description: Releases any dynamically allocated objects/data.
 *
 */
void cleanUp() {

    QVector<QList<float>* >::iterator fIter;

    for (fIter = fPosListVector.end() - 1; fIter >= fPosListVector.begin(); --fIter ) {
        fPosListVector.erase( fIter );
        delete (*fIter);
    }

    for (fIter = fTexelsListVector.end() - 1; fIter >= fTexelsListVector.begin(); --fIter ) {
        fTexelsListVector.erase( fIter );
        delete (*fIter);
    }

    for (fIter = fNormalsListVector.end() - 1; fIter >= fNormalsListVector.begin(); --fIter ) {
        fNormalsListVector.erase( fIter );
        delete (*fIter );
    }

    QVector< QList<int>* >::iterator iIter;

    for (iIter = iFacesListVector.end() - 1; iIter >= iFacesListVector.begin(); --iIter ) {
        iFacesListVector.erase( iIter );
        delete (*iIter);
    }
}

int main(int argc, char *argv[])
{
    /*
    // Initialize Variables;
    float azfPosMat[sDataCount.iPositions][3];    // XYZ
    float azfTexelsMat[sDataCount.iTexels][2];          // UV
    float azfNormalsMat[sDataCount.iNormals][3];        // XYZ
    int aziFacesMat[sDataCount.iFaces][9];              // PTN PTN PTN
    */

    QString qsFile = OBJ_FILE;


    // Testing begins:
    forEachOBJLine( qsFile, &getOBJLineCount );

    qDebug() << "v: " << sDataCount.iVertices
             << "n: " << sDataCount.iNormals
             << "pos: " << sDataCount.iPositions
             << "vt: " << sDataCount.iTexels
             << "f: " << sDataCount.iFaces;


    forEachOBJLine( qsFile, &getOBJLineData );


    QApplication app(argc, argv);

    Window window;
    window.resize(window.sizeHint());
    int desktopArea = QApplication::desktop()->width() *
                     QApplication::desktop()->height();
    int widgetArea = window.width() * window.height();

    window.setWindowTitle("OpenGL with Qt");

    if (((float)widgetArea / (float)desktopArea) < 0.75f)
        window.show();
    else
        window.showMaximized();

     int returnVal = app.exec();
     cleanUp();
     return returnVal;
}
