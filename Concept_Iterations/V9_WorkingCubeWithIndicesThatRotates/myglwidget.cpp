// myglwidget.cpp

#include <QtWidgets>
#include <QtOpenGL>
#include <QDebug>

#include "myglwidget.h"

MyGLWidget::MyGLWidget(const QGLFormat& format, QWidget *parent) :
    QGLWidget( format, parent)
{


}

MyGLWidget::MyGLWidget(QWidget *parent)
// Specififies display format of OpenGL rendering context.
// Allows OpenGL context with multisampling support.Multisampling
// samples each pixel at the edge of a polygon multiple times. A slight offset
// is applied each pass (smaller than the pixel). In averaging these samples, a
// smoother color transition results at the edges.
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
   // m_pGLFuncs = context()->functions();
    //qDebug() << QGLFormat::openGLVersionFlags();


}

MyGLWidget::~MyGLWidget()
{
    m_fPositionBuffer.destroy();
    m_QVAO.destroy();
    m_Program.removeAllShaders();
}



// Sets up the OpenGL rendering context, defines display lists, etc.
//Gets called once before the first time resizeGL() or paintGL() is called.
void MyGLWidget::initializeGL()
{


    // Convenience function for specifying the clearing color to OpenGL.
    // Calls glClearColor (in RGBA mode) or glClearIndex (in color-index
    // mode) with the color c. Applies to this widgets GL context.
    qglClearColor(Qt::black);

    // Enables depth comparisions and updating the depth buffer.
    glEnable(GL_DEPTH_TEST);

    // Culls polygons based on their winding in window coords
    glEnable(GL_CULL_FACE);

    // Smooth shading, the default, causes the computed colors of vertices
    // to be interpolated as the primitives are rasterized.
    glShadeModel(GL_SMOOTH);

    // If enabled and no vertex shader is active, use the current lighting
    // parameters to compute the vertex color or index. Otherwise, simply
    // associate the current color or index with each vertex. Prepares
    // openGL for lighting calculations.
    glEnable(GL_LIGHTING);

    // Enables light source 0.
    glEnable(GL_LIGHT0);

    // x, y, z and w coordinates of the light source.
    // If w == 0, the light source is directional, if w == 1,
    // it is positional.
    static GLfloat lightPosition[4] = { 0, 0, 17, 0};

    // Positions the light source 0, at 'lightPosition'. Default
    // color is white. For different color use, glLight*().
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);


    m_Program.addShaderFromSourceFile(QOpenGLShader::Vertex,  ":/qrc/vertexShader.vert" );
    m_Program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/qrc/fragmentShader.frag" );
    m_Program.bind();
    m_Program.link();


    // Returns location of attributes name within the shader program's
    // attribute list.
    _positionAttr = m_Program.attributeLocation( "position" );

    // Should draw a square.
    const GLfloat glfPositionData[7][3] = {
        { -1, -1 ,0 }, { 1, -1, 0 },  // Bottom of square.
        { 1, 1, 0 }, { -1, 1 ,0 } };  // Top of square.

    const GLuint gluiIndices[6] = { 0, 1, 2, 2, 3, 0 };


    // Creates a new Vertex Array Object
    m_QVAO.create();
    m_QVAO.bind();

    qDebug() << " Size of array" << sizeof(glfPositionData);
    qDebug() << " Size of Glfloat" << sizeof(GLfloat);

   // Create the position vertex buffer.
   m_fPositionBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);

   // Creates buffer object in OpenGL server. Similar to glGenVertexArrays(1, &m_id).
   m_fPositionBuffer.create();
   m_fPositionBuffer.setUsagePattern(QOpenGLBuffer::StreamDraw);

   // Binds the buffer associated with this object to the current opengl context.
   // Similar to calling glBindVertexArray(m_id).
   m_fPositionBuffer.bind();

   // Allocates count bytes of space to the buffer, initialized to the contents of data.
   m_fPositionBuffer.allocate(glfPositionData, sizeof(glfPositionData) );
    //m_fPositionBuffer.allocate(glfPositionData, 6*sizeof(GLfloat ) );


   // Create the indices vertex buffer.
   m_iIndexBuffer = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
   m_iIndexBuffer.create();
   m_iIndexBuffer.setUsagePattern(QOpenGLBuffer::StreamDraw);
   m_iIndexBuffer.bind();
   m_iIndexBuffer.allocate(gluiIndices, sizeof(gluiIndices));


   // Link vertex shader attribute to the data contained in the currently
   // boud VBO. Then enable the attribute.
   m_Program.setAttributeBuffer( _positionAttr, GL_FLOAT, 0, 3);
   m_Program.enableAttributeArray( _positionAttr );

   qDebug() << m_Program.log();
}



// Renders the OpenGL scene. Gets called whenever the widget needs to be updated.
void MyGLWidget::paintGL()
{
    // Clears indicated buffer's to preset values. Sets the bitplane area of the window to
    // values previously selected by glClearColor, glClearDepth, glClearStencil.
    // GL_COLOR_BUFFER_BIT: indicates the buffers currently enabled for color writing.
    // GL_DEPTH_BUFFER_BIT: indicates the depth buffer.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_Program.bind();

    // Initial matrix is ModelView. ModelView matrix deals with rotations,
    // translations, etc.
    glMatrixMode(GL_MODELVIEW);

    // Replaces the current matrix with the identity matrix. Semantically
    // equivalent to calling glLoadMatrix with the identity matrix.
    glLoadIdentity();


    // Provide the vertices/ primitives to draw on the canvas.
    //glDrawArrays(GL_TRIANGLES, 0, 6);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );
}



// Sets up the OpenGL viewport, projection, etc. Gets called whenever
// the widget has been resized (and also when it is shown for the first
// time because all newly created widgets get a resize event automatically)
void MyGLWidget::resizeGL(int width, int height)
{
    int side = qMin(width, height);

    // Determines the portion of the window to which OpenGL is drawing to.
    glViewport((width - side) / 2, (height - side) / 2, side, side);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
#ifdef QT_OPENGL_ES_1

    // Describes a transformation that produces a parallel projection.
    // Parameters: left, right, bottom, top, near, far.
    // 1st Pair: specifies coords for left and right vertical clipping planes
    // 2nd Pair: specify coords for bottom and top horizontal clipping planes
    // 3rd Pair: specify distances to specify distances to nearer and farther depth
    //           clipping planes. Neg. values are behind the viewer.
    glOrthof(-2, +2, -2, +2, 1.0, 15.0);
#else
    glOrtho(-2, +2, -2, +2, 1.0, 15.0);
#endif
    glMatrixMode(GL_MODELVIEW);
}



