// window.cpp

#include <QtWidgets>


#include "window.h"

#include "myglwidget.h"

Window::Window(QWidget *parent) :
    QWidget(parent)
{

    m_pVBoxLayout = new QVBoxLayout;

    // Sets the version and specifications for opengl.
    QGLFormat glFormat;
    glFormat.setVersion(3, 3);
    glFormat.setProfile(QGLFormat::CoreProfile);

    // Enables multisample buffer support.
    glFormat.setSampleBuffers( true );

    // Constructs new opengl widget with format specifactions.
    MyGLWidget* pGLWidget = new MyGLWidget( glFormat );

    m_pVBoxLayout -> addWidget( pGLWidget );

    setLayout( m_pVBoxLayout );

}

QSize Window::sizeHint() const
{
    QSize recommendedSize;

    // If layout set, provide window size.
    if ( layout() != 0 ) {
        recommendedSize = QSize(400, 300);
        return recommendedSize;
    }

    return QWidget::sizeHint();
}

Window::~Window()
{

}

void Window::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}
