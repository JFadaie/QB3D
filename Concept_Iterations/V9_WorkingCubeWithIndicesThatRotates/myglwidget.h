// myglwidget.h

#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QGLWidget>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGlShaderProgram>
#include <QOpenGLFunctions>


class MyGLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit MyGLWidget(const QGLFormat& format, QWidget* parent = 0);
    explicit MyGLWidget(QWidget *parent = 0);
    ~MyGLWidget();
signals:

public slots:

protected:
            void initializeGL();
            void paintGL();
            void resizeGL(int width, int height);




private:

    QOpenGLVertexArrayObject    m_QVAO;
    QOpenGLBuffer               m_fPositionBuffer;
    QOpenGLBuffer               m_iIndexBuffer;
    QOpenGLShaderProgram        m_Program;
    QOpenGLFunctions*           m_pGLFuncs;

    GLuint                      _positionAttr;

};

#endif // MYGLWIDGET_H

