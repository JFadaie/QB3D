#version 330

in vec3 color;
in vec3 posEyeCoord;
in vec3 normEyeVec;

// Fixed point light properties
vec3 lightPosEyeCoord = vec3( 2.0, 0.0, 10.0 ); // View Matrix is identity.

vec3 Ls = vec3( 1.0, 1.0, 1.0 );  // White specular color
vec3 Ld = vec3( 0.7, 0.7, 0.7 );  // Dull-white diffuse color
vec3 La = vec3( 0.2, 0.2, 0.2 );  // Grey ambient color

// Surface Reflectance
vec3 Ks = vec3( 1.0, 1.0, 1.0 );  // Fully reflect white specular light
vec3 Kd;
vec3 Ka = vec3( 1.0, 1.0, 1.0 );  // Fully reflect ambient light
float fSpecularExp = 100.0; // Specular 'power'

uniform int toggle;
uniform int bcktoggle;
uniform int VAOToggle;

out vec4 oFragmentColor; // Final color of surface


void main( void )
{
/*
    if (toggle == 0 ) {
        Kd = vec3( 0.0, 0.3, 0.9 );  // Blue diffuse surface reflectance
    } else {
        Kd = vec3( 1.0, 0.5, 0.0 );  // Orange diffuse surface reflectance
    }
*/
    if (VAOToggle == 0 && toggle == 0) {
        Kd = vec3( 0.0, 0.3, 0.9 );  // Blue diffuse surface reflectance
    } else if  ( VAOToggle == 0 && toggle == 1 ){
        Kd = vec3( 1.0, 0.5, 0.0 );  // Orange diffuse surface reflectance
    } else if ( VAOToggle == 1 && bcktoggle == 0 ) {
        Kd = vec3( 0.8, 0.2, 0.0 );  //
    } else if ( VAOToggle == 1 && bcktoggle == 1 ) {
        Kd = vec3( 0.8, 0.2, 0.6 );
    }

    // ambient intensity
    vec3 Ia = La * Ka;

    // Calculate diffuse intensity
    vec3 distToLightEyeCoord = lightPosEyeCoord - posEyeCoord;
    //vec3 distToLightEyeCoord = -lightPosEyeCoord + posEyeCoord;
    vec3 dirToLightEyeVec = normalize(distToLightEyeCoord);

    float IdDotProd = dot( dirToLightEyeVec, normEyeVec ); // Value between 0 and 1.
    IdDotProd = max( IdDotProd, 0.0 ); // Ensure value is not negative.

    // diffuse intensity
    vec3 Id = Ld * Kd * IdDotProd; // Final Diffuse Intensity

    // Calculate specular intensity
    vec3 lightReflectionEyeVec = reflect( -dirToLightEyeVec, normEyeVec );
    vec3 surfaceToViewerEyeVec = normalize( -posEyeCoord );

    float IsDotProd = dot( lightReflectionEyeVec, surfaceToViewerEyeVec );
    IsDotProd = max( IsDotProd, 0.0 );
    float specularFactor = pow( IsDotProd, fSpecularExp );

    // specular intensity
    vec3 Is = Ls * Ks * specularFactor;

    oFragmentColor = vec4( Is + Id + Ia, 1.0 );

}
