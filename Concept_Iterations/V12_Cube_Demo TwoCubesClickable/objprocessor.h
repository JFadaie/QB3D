
#pragma once

#include <QObject>
#include <QVector>
#include <QString>


// Contains a count of objects data.
typedef struct ObjectDataInfo {
    int iVertices;
    int iPositions;
    int iTexels;
    int iNormals;
    int iFaces;
} ObjDataInfo;


class OBJProcessor : public QObject {

    Q_OBJECT

public:
    enum ProcessType {
        Vertices = 0,
        Normals,
        Texels,
        All
    };

    OBJProcessor( QString& rqsFileName,
                  QObject* pParent,
                  ProcessType eType = OBJProcessor::All );

    ~OBJProcessor() { }

    QVector<float>& getVerticesData() { return m_fPosVector; }
    QVector<float>& getNormalsData() { return m_fNormalsVector; }
    QVector<float>& getTexelsData() { return m_fTexelsVector; }

private:

    QVector<float> m_fPosVector;
    QVector<float> m_fTexelsVector;
    QVector<float> m_fNormalsVector;
    //QVector<int> m_iFacesVector;

    ObjDataInfo sDataCount;

    // Forward declarations.
    void getOBJLineCount( QString& rqsLine );
    void forEachOBJLine( QString& qsFile, void(*processOBJLine)(QString&) );
    void getOBJLineData( QString& rqsLine,
                         QVector<float>& rfPosVector,
                         QVector<float>& rfNormalsVector,
                         QVector<float>& rfTexelsVector,
                         QVector<int>& riFacesVector );
    void getMTLData();

    void processAllOBJData( QVector<float>& rfPosVector,
                            QVector<float>& rfNormalsVector,
                            QVector<float>& rfTexelsVector,
                            QVector<int>& riFacesVector );

    void processPosOBJData( QVector<float>& rfPosVector,
                            QVector<int>& riFacesVector );

    void processNormalsOBJData( QVector<float>& rfNormalsVector,
                                QVector<int>& riFacesVector );

    void processTexelsOBJData( QVector<float>& rfTexelsVector,
                               QVector<int>& riFacesVector);
};


