#include <assert.h>

#include <QFile>
#include <QTextStream>
#include <QDebug>

#include "objprocessor.h"

#define     NUM_VERT_DIM    3
#define     NUM_NORM_DIM    3
#define     NUM_TEXEL_DIM   2
#define     MAX_DIM         3

/*************************
 * Description: Constructor
 *
 *
 */
OBJProcessor::OBJProcessor(QString& rqsFileName,
                           QObject* pParent,
                           ProcessType eType ) :
                                QObject( pParent )
{

    sDataCount.iPositions = 0;
    sDataCount.iNormals = 0;
    sDataCount.iTexels = 0;
    sDataCount.iPositions = 0;
    sDataCount.iFaces = 0;

    QVector<float> fPosVector;
    QVector<float> fNormalsVector;
    QVector<float> fTexelsVector;
    QVector<int> iFacesVector;

    QFile objFile( rqsFileName );

    if ( !objFile.open( QIODevice::ReadWrite | QIODevice::Text )) {
         qDebug() << "The File did not open successfully... returning.";
         return;
    }

    QTextStream inStream( &objFile );


    // Read each line of text until the end of the file.
    while( !inStream.atEnd() ) {
        QString qsLine = inStream.readLine();
        qDebug() << qsLine;
        if ( !qsLine.isNull() ) {
            getOBJLineData( qsLine,
                            fPosVector,
                            fNormalsVector,
                            fTexelsVector,
                            iFacesVector );
        }
    }

    objFile.close();

    processAllOBJData( fPosVector,
                       fNormalsVector,
                       fTexelsVector,
                       iFacesVector );
 #if 0
    switch( eType ) {
        case OBJProcessor::All:
            processAllOBJData( fPosVector,
                               fNormalsVector,
                               fTexelsVector,
                               iFacesVector );
            break;
        case OBJProcessor::Vertices:
            processPosOBJData( fPosVector,
                               iFacesVector );
            break;
        case OBJProcessor::Normals:
            processNormalsOBJData( fNormalsVector,
                               iFacesVector );
            break;
        case OBJProcessor::Texels:
            processTexelsOBJData( fTexelsVector,
                               iFacesVector );
            break;
    }
#endif

}



/*************************
 * Description: Gets the number of position, vertices, normals, texture
 *              coordinates, and faces in the provided file. Store the
 *              information in the a struct.
 *
 * Parameters:
 *      rqsLine: reference to the name of the file processed.
 */

void OBJProcessor::getOBJLineCount(QString& rqsLine) {

    QString qsType = rqsLine.mid( 0 , 2 );
    qDebug() << qsType;

    if( qsType.compare("v ") == 0 )
        sDataCount.iPositions++;
    else if( qsType.compare("vt") == 0 )
        sDataCount.iTexels++;
    else if( qsType.compare("vn") == 0 )
        sDataCount.iNormals++;
    else if( qsType.compare("f ") == 0 )
        sDataCount.iFaces++;


   sDataCount.iVertices = sDataCount.iFaces*3;
}


/*************************
 * Description: Reads the data, line by line, from the Object,
 *              (.obj) File provided by the macro OBJ_FILE. Each
 *              line is processed until the end of the file. The data
 *              is stored in a buffer if it is valid. If the file does
 *              not open, the function returns without doing anything and
 *              provides a debug statement to the console.
 *
 * Parameters:
 *      qsFile: QString representing the file to be opened.
 *      processLine(QString& ): function pointer. Function applied to each
 *                              line of the file.
 */

void OBJProcessor::forEachOBJLine( QString& qsFile,
                                   void(*processOBJLine)(QString&) ) {

    QFile objFile( qsFile );

    if ( !objFile.open( QIODevice::ReadWrite | QIODevice::Text )) {
         qDebug() << "The File did not open successfully... returning.";
         return;
    }

    QTextStream inStream( &objFile );


    // Read each line of text until the end of the file.
    while( !inStream.atEnd() ) {
        QString qsLine = inStream.readLine();
        qDebug() << qsLine;
        if ( !qsLine.isNull() ) {
            processOBJLine( qsLine );
        }
    }

    objFile.close();
}


/*************************
 * Description: Processes the line of data provided token by token,
 *              which are seperated by whitespace. If the data contained
 *              in each specifier is valid it is stored in the appropriate
 *              data buffer.
 *
 * Parameters:
 *      rqsLine: The QString reference that is processed. The line
 *               cannot be null, but
 */

void OBJProcessor::getOBJLineData( QString& rqsLine,
                                   QVector<float>& rfPosVector,
                                   QVector<float>& rfNormalsVector,
                                   QVector<float>& rfTexelsVector,
                                   QVector<int>& riFacesVector ) {

    assert( !rqsLine.isNull() );

    QString qsType = rqsLine.mid( 0 , 2 );
    QString qsData = rqsLine.mid( 2 );
    qsData = qsData.trimmed();


    if(  qsType.compare("v ") == 0 ||
         qsType.compare("vt") == 0 ||
         qsType.compare("vn") == 0 ) {
        QStringList qsList = qsData.split( " " );
        QStringListIterator i( qsList );

        while( i.hasNext()) {
            float fValue = i.next().toFloat();
            //qDebug() << fValue;

            if( qsType.compare("v ") == 0 ) {
                 rfPosVector << fValue;
            } else if( qsType.compare("vt") == 0 ) {
                 rfTexelsVector << fValue;
            } else if( qsType.compare("vn") == 0 ) {
                 qDebug() << "Normal: " << fValue;
                 rfNormalsVector << fValue;
            }
        }

    } else if( qsType.compare("f ") == 0 ) {

        // Split data into list of v/vt/vn first
        QStringList qsList = qsData.split( " " );
        QStringListIterator i( qsList );

        while( i.hasNext()) {
            QString qsFaceData = i.next();
            qDebug() << qsFaceData;
            QStringList qsList = qsFaceData.split( "/" );
            QStringListIterator i( qsList );

            while (i.hasNext() ) {
                int iValue = i.next().toInt();
                qDebug() << iValue;
                riFacesVector << iValue;
            }
        }
    }


}

/*************************
 * Description:
 *
 *
 * Parameters:
 *
 *
 */

void OBJProcessor::processAllOBJData( QVector<float> &rfPosVector,
                                      QVector<float> &rfNormalsVector,
                                      QVector<float> &rfTexelsVector,
                                      QVector<int> &riFacesVector )
{
    qDebug() << "BEGIN PROCESSING" << "\n\n";
    for ( int iIndex = 0; iIndex < riFacesVector.size(); iIndex += MAX_DIM ) {

        // Get the first three values v, vt, vn.
        // Decrement value by 1, and multiply by offset. Remember each,
        // indice represents a group of vertex, normal, and texel dimensions.
        // Vertex: XYZ, Normal: IJK, Texels: UV.
         int iVIndex0 = (riFacesVector[iIndex] - 1)*NUM_VERT_DIM;
         int iVtIndex0 = (riFacesVector[iIndex + 1] - 1)*NUM_TEXEL_DIM;
         int iNIndex0 = (riFacesVector[iIndex + 2] - 1)*NUM_NORM_DIM;
        //qDebug() << "Index Val: " << riFacesVector[iIndex];
        qDebug() << "Normal Index Val: " << iNIndex0;

        for (int iIncr = 0; iIncr < MAX_DIM; ++iIncr  ) {
            // search for these value in respective vector
            float fVValue = rfPosVector[iVIndex0 + iIncr];
            //qDebug() << "Value: " << fVValue;
            float fNValue = rfNormalsVector[iNIndex0 + iIncr];
            qDebug() << "Final Normal: " << fNValue;
            // store value in member buffer
            m_fPosVector << fVValue;
            m_fNormalsVector << fNValue;

            if ( iIncr < NUM_TEXEL_DIM && iVtIndex0 >= 0) {
                float fVtValue = rfTexelsVector[iVtIndex0 + iIncr];
                m_fTexelsVector << fVtValue;
            }
        }
    }
}



void OBJProcessor::processPosOBJData( QVector<float>& rfPosVector,
                        QVector<int>& riFacesVector )
{

}

void OBJProcessor::processNormalsOBJData( QVector<float>& rfNormalsVector,
                            QVector<int>& riFacesVector )
{

}

void OBJProcessor::processTexelsOBJData( QVector<float>& rfTexelsVector,
                           QVector<int>& riFacesVector)
{

}

/*************************
 * Description:
 *
 *
 * Parameters:
 *
 *
 */
void OBJProcessor::getMTLData() {



}

