// main.cpp

#include "window.h"
#include <assert.h>

#include <QApplication>
#include <QSurfaceFormat>
#include <QDesktopWidget>
#include <QFile>
#include <QDataStream>
#include <QDebug>



int main(int argc, char *argv[])
{


    QApplication app(argc, argv);

    QSurfaceFormat format;
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    Window window;
    window.resize(window.sizeHint());
    int desktopArea = QApplication::desktop()->width() *
                     QApplication::desktop()->height();
    int widgetArea = window.width() * window.height();

    window.setWindowTitle("OpenGL with Qt");

    if (((float)widgetArea / (float)desktopArea) < 0.75f)
        window.show();
    else
        window.showMaximized();

     int returnVal = app.exec();
     return returnVal;
}
