User interface for animating 3D programmable LED Cubes. Unlike current 3D cube 
designs, the software interface allows the user to design visuals with no 
programming. (3D Interaction, C++, Qt, OpenGL, Blender, collision detection, 
phong lighting).