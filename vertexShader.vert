#version 330

in vec3 position;
in vec3 normal;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform int toggle;
uniform mat4 bcktMatrix;
uniform int VAOToggle;

out vec3 color;
out vec3 posEyeCoord;
out vec3 normEyeVec;

void main( void )
{
    posEyeCoord = vec3( mvMatrix * vec4( position, 1.0 ) );
    normEyeVec = vec3( mvMatrix * vec4( normal, 0.0));



//    if ( VAOToggle == 0) {
        gl_Position = pMatrix * vec4( posEyeCoord, 1.0 );
    //} else {
      //  gl_Position = pMatrix * mvMatrix * bcktMatrix * vec4( position, 1.0 ) ;
    //}

    //toggle = 1;

    if (toggle == 0) {
        //color = vec3( position.x + 0.5, 1.0, position.y + 0.5 );
        color = vec3( 0.5, 0.5, 0.5 );
    } else {
        //color = vec3( position.x + 0.1, 0.1, position.y + 0.9 );
        color = vec3( position.x + 0.5, 1.0, position.y + 0.5 );
    }

}
