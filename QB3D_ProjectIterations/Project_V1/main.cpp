/***************************************
 *  File: main.cpp
 *  Author: Josh Fadaie
 *  Date: 02/08/2015
 *  Project: QB3D
 *
 *  Description: Main loop of execution.
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/


#include "mainwindow.h"
#include <QApplication>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    MainWindow w;
    w.show();

    return a.exec();
}
