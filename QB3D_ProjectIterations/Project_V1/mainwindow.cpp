/***************************************
 *  File: mainwindow.cpp
 *  Author: Josh Fadaie
 *  Date: 02/08/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "objprocessor.h"


//#define     OBJ_FILE    "/Users/joshfadaie/BlenderFiled/ColorCubeX2_V2.obj"
//#define     OBJ_FILE    "/Users/joshfadaie/BlenderFiled/QB3D_V3.obj"
#define     OBJ_FILE    "/Users/joshfadaie/BlenderFiled/QB3D_V3_OBJ.obj"

MainWindow::MainWindow(QWidget* pParent ) :
    QMainWindow( pParent ),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QString qsFile( OBJ_FILE );

    ui->pGLCubeWidget->InitGLWidget( qsFile );
}

MainWindow::~MainWindow()
{
    delete ui;
}
