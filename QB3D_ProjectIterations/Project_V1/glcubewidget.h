/***************************************
 *  File: GLCubeWidget.h
 *  Author: Josh Fadaie
 *  Date: 02/12/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/


#pragma once


#include <QOpenGLWidget>
#include <QOpenGlShaderProgram>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QVector3D>
#include <QPoint>

#include "geometryengine.h"
#include "snapshotmanager.h"

class GLCubeWidget : public QOpenGLWidget,
                     public QOpenGLFunctions {
    Q_OBJECT

public:
                        GLCubeWidget( QWidget* pParent );
                       ~GLCubeWidget() {}

    void                InitGLWidget( QString& rqsOBJFilePath );

protected:
    virtual void        initializeGL();
    virtual void        paintGL();
    virtual void        resizeGL(int width, int height);

    virtual void        mousePressEvent( QMouseEvent* pEvent );
    virtual void        mouseMoveEvent( QMouseEvent* pEvent );
    virtual void        mouseDoubleClickEvent( QMouseEvent* pEvent );

private:
    enum   DrawType {
        DrawAll = 0,
        DrawObject
    };
    QList<QString> m_qsObjTypeQualifiers;
    GeometryEngine*             m_pEngine;
    QOpenGLShaderProgram*       m_pProgram;
    SnapShotManager             m_SnapShotManager;
    QString                     m_qsOBJFile;

    QMatrix4x4                  m_ModelMatrix;
    QMatrix4x4                  m_ViewMatrix;
    QMatrix4x4                  m_ProjectionMatrix;

    // Variables used to determine what to draw when the paint event occurs.
    DrawType                    m_eDrawType;
    int                         m_iDrawObjIndex;

    bool                        m_bSelected;
    int                         m_iObjType;
    int                         m_iXRot;
    int                         m_iYRot;
    int                         m_iZRot;
    QPoint                      m_qptLastPos;
    QPoint                      m_qptDblClickPos;
    QSize                       m_ViewportSize;
    QList<bool>                 m_ObjSelectedList;

    void                setXRotation(int iAngle);
    void                setYRotation(int iAngle);
    void                setZRotation(int iAngle);


    QVector3D calculateRayCast( QPoint& rqptDblClickPos );

    // Possibly take in a center point as well, and camera position in world coords.
    void      intersectsBoundingSpheres( QVector3D& rfRayWorld,
                                        QList<bool>& rbIntercepts,
                                        QList<float>& rfIntercepts );
    float     calcLargestObjectIntercept( float fB, float fC );
    int       findIntersectingObject( QList<bool>& rbIntercepts,
                                      QList<float>& rfIntercepts );

    int       getObjectType( const QString& rqsObjName );
};

