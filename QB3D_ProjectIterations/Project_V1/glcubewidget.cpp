/***************************************
 *  File: GLCubeWidget.cpp
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include <QMouseEvent>
#include <QPoint>

#include <float.h>
#include <qmath.h>
#include "glcubewidget.h"

GLCubeWidget::GLCubeWidget( QWidget* pParent ) :
    QOpenGLWidget( pParent )
{
    m_iXRot = 0;
    m_iYRot = 0;
    m_iZRot = 0;

    m_iDrawObjIndex = 0;
    m_eDrawType = GLCubeWidget::DrawAll;
    m_qsObjTypeQualifiers << "LED" << "Base" << "WireFrame";
}


void GLCubeWidget::InitGLWidget( QString& rqsOBJFilePath )
{
    m_qsOBJFile = rqsOBJFilePath;

}


void GLCubeWidget::initializeGL()
{
    initializeOpenGLFunctions();

    // Convenience function for specifying the clearing color to OpenGL.
    // Calls glClearColor (in RGBA mode) or glClearIndex (in color-index
    // mode) with the color c. Applies to this widgets GL context.
    glClearColor( 0.88, 0.88, 0.88, 1);

    // Enables depth comparisions and updating the depth buffer.
    glEnable(GL_DEPTH_TEST);

    // Culls polygons based on their winding in window coords
    glEnable(GL_CULL_FACE);

    // Smooth shading, the default, causes the computed colors of vertices
    // to be interpolated as the primitives are rasterized.
    glShadeModel(GL_SMOOTH);

   // glEnable( GL_SCISSOR_TEST );

    // Create shader program, add shaders, bind to the context, and link.
    m_pProgram = new QOpenGLShaderProgram( this );
    m_pProgram->addShaderFromSourceFile(QOpenGLShader::Vertex,  ":/qrc/vertexShader.vert" );
    m_pProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/qrc/fragmentShader.frag" );
    m_pProgram->bind();
    m_pProgram->link();

    m_pEngine = new GeometryEngine( m_qsOBJFile,
                                    m_pProgram,
                                    this );

    // Initialize Object Selected List to false.
    for ( int iIndex = 0; iIndex < m_pEngine->size(); ++iIndex ) {
        m_ObjSelectedList << false;
    }
}


void GLCubeWidget::paintGL()
{
    // Clears indicated buffer's to preset values. Sets the bitplane area of the window to
    // values previously selected by glClearColor, glClearDepth, glClearStencil.
    // GL_COLOR_BUFFER_BIT: indicates the buffers currently enabled for color writing.
    // GL_DEPTH_BUFFER_BIT: indicates the depth buffer.
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_pProgram->bind();
    QVector3D CubeCenter3D = QVector3D( 0.0, -5.0, -10.0 );
    // Do Transformations.
    // Begin model matrix transformations.
    m_ModelMatrix = QMatrix4x4();
    m_ModelMatrix.translate( CubeCenter3D );
    m_ModelMatrix.rotate(m_iXRot / 18.0, 1.0, 0.0, 0.0);
    m_ModelMatrix.rotate(m_iYRot / 18.0, 0.0, 1.0, 0.0);
    m_ModelMatrix.rotate(m_iZRot / 18.0, 0.0, 0.0, 1.0);

    // Begin view matrix transformations.
    QVector3D eyePos3D = QVector3D( 0, 0, -50.0 );
    QVector3D upDir3D = QVector3D( 0, 1, 0 );
    m_ViewMatrix = QMatrix4x4();
    m_ViewMatrix.lookAt( eyePos3D, CubeCenter3D, upDir3D );

    // Set uniform values
    m_pProgram->setUniformValue( "uMMat", m_ModelMatrix );
    m_pProgram->setUniformValue( "uVMat", m_ViewMatrix );
    m_pProgram->setUniformValue( "uPMat", m_ProjectionMatrix );

#if 0
    m_pProgram.setUniformValue( "toggle", m_iColorToggle );
    m_pProgram.setUniformValue( "bcktoggle", m_iBckColorToggle );
    m_pProgram.setUniformValue( "VAOToggle", m_iVAOToggle );
#endif

    if ( m_eDrawType == DrawAll ) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // Draw the geometry.
        for ( int iIndex = 0; iIndex < m_pEngine->size(); ++iIndex ) {
            m_bSelected = m_ObjSelectedList[iIndex];
            m_iObjType = getObjectType(m_pEngine->getObjectName( iIndex ));
            m_pProgram->setUniformValue( "ubSelected", m_bSelected );
            m_pProgram->setUniformValue( "uiObjType", m_iObjType );
            m_pEngine->drawObject( iIndex );
        }
    } else if ( m_eDrawType == DrawObject ) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //glScissor(m_ViewportSize.width()*0.7, m_ViewportSize.height()*0.7,
        //          m_ViewportSize.width()*0.3, m_ViewportSize.height()*0.3);
        m_bSelected = m_ObjSelectedList[m_iDrawObjIndex];
        qDebug() << "Is object selected: " << m_bSelected;
        m_pProgram->setUniformValue( "ubSelected", m_bSelected );
        m_pProgram->setUniformValue( "uiObjType", m_iObjType );
        m_pEngine->drawObject( m_iDrawObjIndex );
    }
}



// Sets up the OpenGL viewport, projection, etc. Gets called whenever
// the widget has been resized (and also when it is shown for the first
// time because all newly created widgets get a resize event automatically)
void GLCubeWidget::resizeGL(int w, int h)
{
    m_ViewportSize = QSize( w, h );

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 0.1, zFar = 100, fov = 65.0;

    // Reset projection
    m_ProjectionMatrix.setToIdentity();

    // Set perspective projection
    m_ProjectionMatrix.perspective(fov, aspect, zNear, zFar);

    m_eDrawType = GLCubeWidget::DrawAll;
    update();
}



void GLCubeWidget::mousePressEvent(QMouseEvent* pEvent)
{
    m_qptLastPos = pEvent->pos();
}


void GLCubeWidget::mouseMoveEvent(QMouseEvent* pEvent)
{
    int dx = pEvent->x() - m_qptLastPos.x();
    int dy = pEvent->y() - m_qptLastPos.y();

    if (pEvent->buttons() & Qt::LeftButton) {
        setXRotation(m_iXRot + 18 * dy);
        setYRotation(m_iYRot + 18 * dx);
    } else if (pEvent->buttons() & Qt::RightButton) {
        setXRotation(m_iXRot + 18 * dy);
        setZRotation(m_iZRot + 18 * dx);
    }

    m_qptLastPos = pEvent->pos();
}


void GLCubeWidget::setXRotation(int iAngle)
{
    if (iAngle != m_iXRot) {
        m_iXRot = iAngle;
        m_eDrawType = GLCubeWidget::DrawAll;
        update();
    }
}

void GLCubeWidget::setYRotation(int iAngle)
{
    if (iAngle != m_iYRot) {
        m_iYRot = iAngle;
        m_eDrawType = GLCubeWidget::DrawAll;
        update();
    }
}

void GLCubeWidget::setZRotation(int iAngle)
{
    if (iAngle != m_iZRot) {
        m_iZRot = iAngle;
        m_eDrawType = GLCubeWidget::DrawAll;
        update();
    }
}


void GLCubeWidget::mouseDoubleClickEvent(QMouseEvent* pEvent )
{

    // Step 0: 2D Viewport Coordinates between 0:width, 0:height.
    QPoint qptDblClickPos = pEvent->pos();
    qDebug() << "ViewPort Dbl Click Coords: " << qptDblClickPos;

    QVector3D fRayObject3D = calculateRayCast( qptDblClickPos );

    QList<bool> bIntercepts;
    QList<float> fIntercepts;
    for ( int iIndex = 0; iIndex < m_pEngine->size(); ++iIndex ) {
        bIntercepts << false;
        fIntercepts << 0.0;
    }

    intersectsBoundingSpheres( fRayObject3D,
                               bIntercepts,
                               fIntercepts );


    int iObjIndex = findIntersectingObject( bIntercepts,
                                            fIntercepts );
    // TODO: Determine how to hold whether an object is selected or not
    // how to select which object type is being drawn.
    if ( iObjIndex > -1 ) {
        qDebug() << " An Object was selected!";
        m_ObjSelectedList[iObjIndex] = !m_ObjSelectedList[iObjIndex];
        m_iDrawObjIndex = iObjIndex;
        m_eDrawType = GLCubeWidget::DrawAll;  // TODO: Change to DrawObject
        update();                             // when figure out how to draw
    }                                         // only objects that need updating.
}


QVector3D GLCubeWidget::calculateRayCast( QPoint& rqptDblClickPos )
{
    // Step 1: 3D Normalised Device Coords between [-1:1,-1:1,-1:1].
    float fNormDeviceX = ((2.0f *rqptDblClickPos.x())/m_ViewportSize.width()) - 1.0f;
    float fNormDeviceY = 1.0f - ((2.0f *rqptDblClickPos.y())/m_ViewportSize.height());
    float fNormDeviceZ = 1.0f;
    QVector3D fRayNormDevCoords3D( fNormDeviceX,
                                 fNormDeviceY,
                                 fNormDeviceZ );
    qDebug() << "Normalized Device Coords: " << fRayNormDevCoords3D;

    // Step 2: 4D Homogeneous Clip Coordinates between [-1:1,-1:1,-1:1, -1:1].
    QVector4D fRayHomogClipCoords4D( fRayNormDevCoords3D.x(),
                                   fRayNormDevCoords3D.y(),
                                   -1.0, 1.0 );
    qDebug() << "Homogeneous Clip Coords: " << fRayHomogClipCoords4D;

    // Step 3: 4D Eye (Camera) Coordinates between [-x:x, -y:y, -z:z, -w:w].
    bool bIsInverted = false;
    QMatrix4x4 invProjMatrix = m_ProjectionMatrix.inverted( &bIsInverted );
    QVector4D fRayEye4D;
    if ( bIsInverted || !invProjMatrix.isIdentity() ) {
        fRayEye4D = invProjMatrix*fRayHomogClipCoords4D;
        fRayEye4D = QVector4D(fRayEye4D.toVector2D(), -1.0, 0.0);
        qDebug() << " 4D Eye Coords: " << fRayEye4D;
    } else {
        qDebug() << "Projection matrix could not be inverted!";
        return QVector3D();
    }

    // Step 4: 4D World Coordinates between [-x:x, -y:y, -z:z, -w:w].
    bIsInverted = false;
    QMatrix4x4 invViewMatrix = m_ViewMatrix.inverted( &bIsInverted );
    QVector4D fRayWorld4D;
    if ( bIsInverted || !invViewMatrix.isIdentity() ) {
        fRayWorld4D = (invViewMatrix*fRayEye4D);
        qDebug() << " 4D World Coords: " << fRayWorld4D;
    } else {
        qDebug() << "View matrix could not be inverted!";
        return QVector3D();
    }
    QVector3D fRayWorld3D = fRayWorld4D.toVector3D();
    fRayWorld3D.normalize();


    // Step 5: 4D Object Coordinates between [-x:x, -y:y, -z:z, -w:w].
    QMatrix4x4 invModelMatrix = m_ModelMatrix.inverted( &bIsInverted );
    QVector4D fRayObject4D;
    if ( bIsInverted || !invModelMatrix.isIdentity() ) {
        fRayObject4D = (invModelMatrix*fRayWorld4D);
        qDebug() << " 4D Object Coords: " << fRayObject4D;
    } else {
        qDebug() << "Model matrix could not be inverted!";
        return QVector3D();
    }
    QVector3D fRayObject3D = fRayObject4D.toVector3D();
    fRayObject3D.normalize();

    qDebug();
    return fRayObject3D;
}


void GLCubeWidget::intersectsBoundingSpheres( QVector3D& rfRayObject3D,
                                              QList<bool>& rbIntercepts,
                                              QList<float>& rfIntercepts )
{

    // Determine center of object and radius.
    QVector4D camOriginEye4D = QVector4D(0, 0, 0, 1 );
    QVector3D camOriginObject3D;

    bool bIsVInverted = false;
    bool bIsMInverted = false;
    QMatrix4x4 invViewMatrix = m_ViewMatrix.inverted( &bIsVInverted );
    QMatrix4x4 invModelMatrix = m_ModelMatrix.inverted( &bIsMInverted );

    if ( bIsVInverted && bIsMInverted ) {
        QVector4D camOriginObject4D;
        camOriginObject4D = (invModelMatrix*invViewMatrix*camOriginEye4D);

        camOriginObject3D = camOriginObject4D.toVector3D();
        qDebug() << "Transform Center of Obj and Camera: ";
    } else {
        qDebug() << "ModelView matrix could not be inverted!";
        return;
    }

    QVector3D objCenterObject3D;
    float fRadius;
    float fIntercept = 0;
    bool bIsIntersecting = false;
    for ( int iIndex = 0; iIndex < m_pEngine->size(); ++iIndex ) {
        // TODO: make a check to do ray casting for objects with specific name
        // If name isn't valid then insert false and -1.0 into list

        bIsIntersecting = false;
        fIntercept = 0.0;

        objCenterObject3D = m_pEngine->getObjectCenter( iIndex );
        qDebug() << "Object Center: " << objCenterObject3D;

        fRadius = m_pEngine->getObjectRadius( iIndex );
        qDebug() << "Object Radius: " << fRadius;

        QVector3D originSubCenter3D = camOriginObject3D - objCenterObject3D;

        // Check if ray intersects sphere.
        float fB = QVector3D::dotProduct( rfRayObject3D, originSubCenter3D );

        float fCOCDotProd = QVector3D::dotProduct(originSubCenter3D,
                                                  originSubCenter3D );
        float fC = fCOCDotProd - fRadius;

        qDebug() << "(fB*fB - fC ) " << (fB*fB - fC );
        if ( (fB*fB - fC ) >= 0 ) {
            bIsIntersecting = true;
            fIntercept = calcLargestObjectIntercept( fB, fC);
        }

        rbIntercepts[iIndex] = bIsIntersecting;
        rfIntercepts[iIndex] = fIntercept;
    }
}



float GLCubeWidget::calcLargestObjectIntercept( float fB, float fC ) {
    float fBSq2 = fB * fB;
    float fSqrt = float (qSqrt( ( fBSq2 - fC ) ));
    float fT1 = (-fB) + fSqrt;
    float fT2 = (-fB) - fSqrt;
    return qMax( fT1, fT2 );

}


int GLCubeWidget::findIntersectingObject( QList<bool>& rbIntercepts,
                                          QList<float>& rfIntercepts )
{
    float fMaxIntercept = FLT_MIN;
    int iObjIndex = -1;
    qDebug() << "F Intercepts Size: " << rfIntercepts.size();
    qDebug() << "B Intercepts Size: " << rbIntercepts.size();

    for ( int iIndex = 0; iIndex < rbIntercepts.size(); ++iIndex ) {
        if ( rbIntercepts[iIndex] ) {
            if ( rfIntercepts[iIndex] > fMaxIntercept  ) {
                fMaxIntercept = rfIntercepts[iIndex];
                iObjIndex = iIndex;
            }
        }

    }
    return iObjIndex;
}


int GLCubeWidget::getObjectType( const QString& rqsObjName )
{
    for ( int iIndex = 0;
          iIndex < m_qsObjTypeQualifiers.size(); ++iIndex ) {
        QString qsObjQualifier = m_qsObjTypeQualifiers[iIndex];
        if ( rqsObjName.contains( qsObjQualifier)) {
            return iIndex;
        }
    }
    return -1; // Will use default color.
}
