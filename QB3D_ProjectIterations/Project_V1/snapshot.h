#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#include <QObject>

class SnapShot : public QObject {
    Q_OBJECT
public:
    explicit SnapShot(QObject* pParent, int iNumLEDs );
    ~SnapShot();
    void setSelectedLEDs( QList<bool>& m_bSelectedList );
    const QList<bool>& getSelectedLEDS() {
        return m_bSelectedLEDList;
    }


signals:

public slots:

private:
    QList<bool> m_bSelectedLEDList;

};

#endif // SNAPSHOT_H
