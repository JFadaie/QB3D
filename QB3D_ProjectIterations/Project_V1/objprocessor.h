/***************************************
 *  File: objprocesser.h
 *  Author: Josh Fadaie
 *  Date: 02/08/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#pragma once

#include <QObject>
#include <QVector>
#include <QVector3D>
#include <QString>


// Struct containing pointers to an Objects data.
// Objects are defined in the wavefront file as being
// prepended with a 'o' for "object" character.
typedef struct {
    QVector<QVector3D>* pVertexData;
    QVector<QVector2D>* pTexelData;
    QVector<QVector3D>* pNormalData;
} ObjDataPtrs;


class OBJProcessor : public QObject {
    Q_OBJECT

public:
    enum ProcessType {
        Vertices = 1,
        Normals = 2,
        VertNorms = 3,
        Texels = 4,
        All = 7
    };

                        OBJProcessor( const QString& rqsFileName,
                                      QObject* pParent = 0,
                                      ProcessType eType = OBJProcessor::VertNorms );

                       ~OBJProcessor();

    QVector<QVector3D>* getVerticesData(int iObjIndex );
    QVector<QVector3D>* getNormalsData( int iObjIndex );
    QVector<QVector2D>* getTexelsData( int iObjIndex );
    void                getObjCenterAndRadius( int iObjIndex,
                                               QVector3D& rCenter3D,
                                               float& rfRadius );
    QString             getObjName( int iObjIndex ) {
                           if ( iObjIndex < 0 || iObjIndex >= m_CubeObjectNames.size() ) {
                               return QString("");
                           } else {
                               return m_CubeObjectNames[iObjIndex]; } }

    // Returns the number of objects retrieved by the OBJ File processor.
    int                 size() { return m_CubeObjectBuffer.size(); }

private:
    // List of pointers to OBJ data. Each structure in the list contains pointers to
    // an objects vertex, texel, and normal vectors.
    QVector<ObjDataPtrs> m_CubeObjectBuffer;

    QVector<QString> m_CubeObjectNames;

    // NOTE: Could have a starting index so that face vector
    // does not always read starting at index zero.
    // Face indices for objects will each be offset by the
    // number of vertices of the previous object.
    void getOBJLineData( QString& rqsLine,
                         QVector<QVector3D>& rfPosVector,
                         QVector<QVector3D>& rfNormalsVector,
                         QVector<QVector2D>& rfTexelsVector,
                         QVector<QVector3D>& riFacesVector );

    void processAllOBJData( QVector<QVector3D>& rfPosVector,
                            QVector<QVector3D>& rfNormalsVector,
                            QVector<QVector2D>& rfTexelsVector,
                            QVector<QVector3D>& riFacesVector);

    void processVNOBJData( QVector<QVector3D>& rfPosVector,
                           QVector<QVector3D>& rfNormalsVector,
                           QVector<QVector3D>& riFacesVector);

#if 0
    void processPosOBJData( QVector<float>& rfPosVector,
                            QVector<int>& riFacesVector );

    void processNormalsOBJData( QVector<float>& rfNormalsVector,
                                QVector<int>& riFacesVector );

    void processTexelsOBJData( QVector<float>& rfTexelsVector,
                               QVector<int>& riFacesVector);
 #endif
};


