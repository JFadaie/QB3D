#ifndef COLLISIONDETECTOR_H
#define COLLISIONDETECTOR_H

#include <QObject>

class CollisionDetector : public QObject
{
    Q_OBJECT
public:
    explicit CollisionDetector(QObject *parent = 0);
    ~CollisionDetector();

signals:

public slots:
};

#endif // COLLISIONDETECTOR_H
