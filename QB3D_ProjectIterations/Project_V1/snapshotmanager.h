#ifndef SNAPSHOTMANAGER_H
#define SNAPSHOTMANAGER_H

#include <QObject>

#include "snapshot.h"

class SnapShotManager : public QObject {
    Q_OBJECT
public:
    explicit SnapShotManager(QObject *parent = 0);
    ~SnapShotManager();
     SnapShot* getSnapShot( int iIndex ) { // Get by name of key
        return m_SnapShotPtrs[iIndex];
     }
     void addSnapShot( int iNumLEDs ); // Add a snapshot by # leds and name


signals:

public slots:

private:
    //QMap<QString, QList<SnapShot*> m_SnapShotMap;
    QList<SnapShot*> m_SnapShotPtrs;
};

#endif // SNAPSHOTMANAGER_H
