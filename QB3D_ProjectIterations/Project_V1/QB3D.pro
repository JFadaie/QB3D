#-------------------------------------------------
#
# Project created by QtCreator 2015-02-08T15:01:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QB3D
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    objprocessor.cpp \
    geometryengine.cpp \
    glcubewidget.cpp \
    snapshot.cpp \
    snapshotmanager.cpp \
    singleton.cpp \
    collisiondetector.cpp \
    macros.cpp

HEADERS  += mainwindow.h \
    objprocessor.h \
    geometryengine.h \
    glcubewidget.h \
    snapshot.h \
    snapshotmanager.h \
    singleton.h \
    collisiondetector.h \
    macros.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
