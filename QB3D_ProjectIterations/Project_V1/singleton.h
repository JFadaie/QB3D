#pragma once


template <typename T> class Singleton {

public:
    static T* GetInstance() {
        if ( !m_pInstance ) {
            m_pInstance = new T();
        }
        return m_pInstance;
    }



private:
    static T* m_pInstance;

    Singleton() {}
    virtual ~Singleton() {}

    Singleton( Singleton const & );     // Don't implement.
    void operator=( Singleton const& ); // Don't implement.

};

