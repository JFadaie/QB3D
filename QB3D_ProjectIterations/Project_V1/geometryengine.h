/***************************************
 *  File: GeometryEngine.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#pragma once

#include <QObject>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QVector>
#include <QVector3D>

#include "singleton.h"
#include "objprocessor.h"

#if 0
// Note: not currently used but maybe eventually.
typedef struct {
    QOpenGLBuffer*              pBufferObject;
    GLuint                      glAttribute;
} GLBufferData;

#endif

// Structure containing data necessary to render and transform an
// object. Object name is used for extra identification, and the
// center and radius values are used for proper ray casting/determining
// user interaction.
typedef struct ObjData {
    QOpenGLVertexArrayObject* pVAO;
    int                       iNumTriangles;
    QOpenGLBuffer             pVertexBuffer;
    QOpenGLBuffer             pNormalBuffer;
    QOpenGLBuffer             pTexelBuffer;
    QString                   qsObjName;
    QVector3D                 center3D;
    QVector4D                 transformedCenter4D;
    float                     fRadiusFromCenter;
} ObjectData;


// VAO info is a collection of data necessary to define a Vertex
// array object for an object. Implemented here for compatibility
// with CreateVAO. Although the OBJProcessor data could be
// retrieved internally, VAOInfo might eventually be passed by the
// user directly and not through an OBJ file.
typedef struct VAOInfo {
    QVector<QVector3D>*             pfVertexList;
    QVector<QVector3D>*             pfNormalList;
    QVector<QVector2D>*             pfTexelList;
    QString                         qsObjName;
    QVector3D                       center3D;
    float                           fRadiusFromCenter;
} VAOInfo;


class GeometryEngine : public QObject {
    Q_OBJECT

public:
    explicit GeometryEngine(QObject* pParent = 0);
    explicit GeometryEngine( QString& rqsOBJPath,
                             QOpenGLShaderProgram* pProgram,
                             QObject* pParent = 0);
    ~GeometryEngine();

    void                      setShaderProgram( QOpenGLShaderProgram* pProgram ) {
                                    m_pProgram = pProgram; }
    void                      initEngine( QString& rqsOBJPath,
                                          QOpenGLShaderProgram* pProgram,
                                          QObject* pParent = 0 );
    QString                   getObjectName( int iObjIndex );
    //QString&                  getObjectName( QVector3D originalCenter3D );
    ObjectData&               getObjectData( int iObjIndex );
    //ObjData&                  getObjectData( QVector3D originalCenter3D );
    QVector3D                 getObjectCenter(int iObjIndex );
    //QOpenGLVertexArrayObject& getObjectVAO( QVector3D originalCenter3D );
    //QOpenGLVertexArrayObject& getObjectVAO( int iObjIndex );
    //QOpenGLVertexArrayObject& getObjectVAO( QString& rqsName );
    float                     getObjectRadius( int iObjIndex );
    void                      drawObject( int iObjIndex );
    //void                      drawObject( QOpenGLVertexArrayObject* pVAO );
    void                      createVAO( VAOInfo& rInfo );
    int                       size() { return m_ObjectList.size(); }


private:
    QVector<ObjectData*> m_ObjectList;
    QOpenGLShaderProgram* m_pProgram;
    void                      printVec3DList( QVector<QVector3D>* pVec3DList,
                                              const QString& rqsDebugTitle );
    void                      getProcessorVAOInfo( int iObjIndex,
                                                   OBJProcessor& rProcessor,
                                                   VAOInfo& rInfo);

    // TODO: possibly have variables to access and set these values.
    GLuint                          m_gluiPosAttribute;
    GLuint                          m_gluiNormalAttribute;
    GLuint                          m_gluiTexelAttribute;
    QOpenGLBuffer::UsagePattern     m_glBufferUsagePattern;

    // TODO: possibly have variables to access and set these values.
    // Default values are given as:
    //              inPosition3D
    //              inNormal3D
    //              inTexel3D
    QString                         m_qsPosAttrName;
    QString                         m_qsNormalAttrName;
    QString                         m_qsTexelAttrName;

signals:

public slots:


};


