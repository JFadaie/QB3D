
#pragma once

#include <QObject>
#include <QVector>
#include <QVector3D>
#include <QString>


// Contains a count of objects data.
typedef struct ObjectDataInfo {
    int iVertices;
    int iPositions;
    int iTexels;
    int iNormals;
    int iFaces;
} ObjDataInfo;


// Typedef for future use.
//typedef QVector<QVector3D> VectorArray3D;


class OBJProcessor : public QObject {

    Q_OBJECT

public:
    enum ProcessType {
        Vertices = 0,
        Normals,
        Texels,
        All
    };

    OBJProcessor( QString& rqsFileName,
                  QObject* pParent,
                  ProcessType eType = OBJProcessor::All );

    ~OBJProcessor() { }

    QVector<float>& getVerticesData() { return m_fPosVector; }
    QVector<float>& getNormalsData() { return m_fNormalsVector; }
    QVector<float>& getTexelsData() { return m_fTexelsVector; }

    QVector<float>& getVerticesData2() { return m_fPosVector2; }
    QVector<float>& getNormalsData2() { return m_fNormalsVector2; }
    QVector<float>& getTexelsData2() { return m_fTexelsVector2; }
    QVector<QVector3D>& getCubeBufferData() { return m_fCubeBufferData; }


private:
    QVector<QVector3D> m_fCubeBufferData;  // Map index QVector3D index to center,
                                           // center transform, and radius/ something else.
    //QMap<int, QOpenglBufferObject* > m_fDataBufferMap;  // Map index to buffer pointer

    QVector<float> m_fPosVector;
    QVector<float> m_fTexelsVector;
    QVector<float> m_fNormalsVector;

    QVector<float> m_fPosVector2;
    QVector<float> m_fTexelsVector2;
    QVector<float> m_fNormalsVector2;
    //QVector<int> m_iFacesVector;

    ObjDataInfo sDataCount;

    // Forward declarations.
    void getOBJLineCount( QString& rqsLine );
    void forEachOBJLine( QString& qsFile, void(*processOBJLine)(QString&) );
    void getOBJLineData( QString& rqsLine,
                         QVector<float>& rfPosVector,
                         QVector<float>& rfNormalsVector,
                         QVector<float>& rfTexelsVector,
                         QVector<int>& riFacesVector );
    void getMTLData();

    void processAllOBJData( QVector<float>& rfPosVector,
                            QVector<float>& rfNormalsVector,
                            QVector<float>& rfTexelsVector,
                            QVector<int>& riFacesVector,
                            int iBufferIndex );

    void processPosOBJData( QVector<float>& rfPosVector,
                            QVector<int>& riFacesVector );

    void processNormalsOBJData( QVector<float>& rfNormalsVector,
                                QVector<int>& riFacesVector );

    void processTexelsOBJData( QVector<float>& rfTexelsVector,
                               QVector<int>& riFacesVector);
};


