// myglwidget.h

#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGlShaderProgram>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QVector3D>


class MyGLWidget : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit MyGLWidget(QWidget *parent = 0);
    ~MyGLWidget();
signals:

public slots:

protected:
            void initializeGL();
            void paintGL();
            void resizeGL(int width, int height);

            virtual QSize sizeHint() const { return QSize(500, 500); }

            virtual void mousePressEvent(QMouseEvent *event);
            virtual void mouseMoveEvent(QMouseEvent *event);
            virtual void mouseDoubleClickEvent(QMouseEvent* pEvent );
            QVector3D    computeNormals( QVector3D p0, QVector3D p1, QVector3D p2 );
            void         collectData( GLfloat pglfData[][3], int iIndex, QVector<QVector3D>& rDataVec );

private:
    QTimer*                     m_pTimer;
    // VAO back.
    QOpenGLVertexArrayObject    m_bckQVAO;
    QOpenGLBuffer               m_bckfPositionBuffer;
    QOpenGLBuffer               m_bckfNormalBuffer;


    QOpenGLVertexArrayObject    m_QVAO;
    QOpenGLBuffer               m_fPositionBuffer;
    QOpenGLBuffer               m_fNormalBuffer;
    QOpenGLBuffer               m_iIndexBuffer;
    QOpenGLShaderProgram        m_Program;

    GLuint                      _positionAttr;
    GLuint                      _matrixAttr;
    GLuint                      _normalAttr;

    QMatrix4x4                  m_Projection;
    QMatrix4x4                  m_ModelView;
    QMatrix4x4                  m_backTranslateMatrix;

    GLsizei                     m_gliSize;
    int                         m_iXRot;
    int                         m_iYRot;
    int                         m_iZRot;
    QPoint                      m_qptLastPos;
    QPoint                      m_qptDblClickPos;
    QSize                       m_ViewportSize;

    int                         m_iBckColorToggle;
    int                         m_iColorToggle;
    int                         m_iVAOToggle;

   void setXRotation(int iAngle);
   void setYRotation(int iAngle);
   void setZRotation(int iAngle);
   static void normalizeAngle(int& riAngle);
   QVector3D calculateRayCast( QPoint& rqptDblClickPos );

   // Possibly take in a center point as well, and camera position in world coords.
   bool      intersectsBoundingSphere( QVector3D& rfRayWorld, float& fIntercept );
   bool      intersectsBoundingSphere2( QVector3D& rfRayWorld, float& fIntercept );
   float     calcLargestIntercept( float fB, float fC );
};

#endif // MYGLWIDGET_H

