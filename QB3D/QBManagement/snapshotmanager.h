/**************************************************
 *  File: snapshotmanager.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QObject>

#include "snapshot.h"
#include "qbglobals.h"

class AnimationPacket;

class SnapShotManager : public QObject {
    Q_OBJECT
public:
    explicit SnapShotManager(QObject *parent = 0);
    ~SnapShotManager();
     SnapShot* getSnapShot( int iIndex ) { // Get by name of key
        return m_SnapShotPtrs[iIndex];
     }
     void addSnapShot(); // Add a snapshot by # leds and name
     void insertSnapShot();
     void deleteSnapShot();
     SnapShot currentSnapShot();
     void setCurrentSnapShot( const SnapShot& rNewSnapShot );
     int  size() { return m_SnapShotPtrs.size(); }
     void setCurrentIndex( const int iIndex );
     void suggest(
                AnimationPacket* const pPacket,
                QB::SnapShotOp         eOperation );
     void isCommitted();
signals:
    //void     currenntSnapShot( const SnapShot& rCurrSnapShot );

public slots:
     void    onCurrentIndexChanged( const int iIndex );


private:
    //QMap<QString, QList<SnapShot*> m_SnapShotMap;
    QList<SnapShot*> m_SnapShotPtrs;
    int              m_iCurrentIndex;
    bool             m_bCommitted;
    AnimationPacket* m_pSuggestedPacket;
};

