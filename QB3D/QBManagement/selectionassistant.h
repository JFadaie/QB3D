/**************************************************
 *  File: selectionassistant.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once
#include <QObject>
#include "snapshot.h"
#include "communicationpacket.h"
#include "singleton.h"
#include "qbglobals.h"


// Possibly use a struct to communicate data between
// GLCubeWidget and SelectionAssistant.
// Key entered, coord, snapshot (option or original depending on key )
// Selection mode bool,

class VisualHandler;
class AnimationPacket;

class SelectionAssistant : public QObject {

    Q_OBJECT
    friend class Singleton<SelectionAssistant>;

public:

    ~SelectionAssistant();
    static SelectionAssistant*
                        getAssistant() {
                            SelectionAssistant* const pAssistant =
                                        Singleton<SelectionAssistant>::GetInstance();
                            return pAssistant; }


signals:
#if 0
    void    updateSnapShot(
                    const SnapShot& rSnapShot,
                    bool            bSnapSelected = false );
#endif
    void                animationPacket(
                            const AnimationPacket& rAniPacket,
                            QB::SnapShotOp         eOperation = QB::ADD );
    void                commitEffect();
    void                undoEffect();

public slots:
    void                onPanelChanged(
                            const QString& rqsHandlerKey,
                            const QString& rqsHandlerState );
    void                onPanelStateChanged(
                            const QString& rqsHandlerState );
    void                onEffectUpdated() { /* TODO: */ }

    void                onUserSelection( const LEDCoords& rLedCoords );
   // void    onNewSnapShot( const SnapShot& rSnapShot );


protected:
    virtual bool        eventFilter( QObject* pObject, QEvent* pEvent );

private:
                        SelectionAssistant( QObject* pParent = 0 );

    VisualHandler*      m_pVisualHandler;
    bool                m_bPrimed; // Has the assistant recieved an LEDCoord.
    AnimationPacket*    createAniPacket(
                                const QList<SnapShot*>& rSnapShotList );



   // QString         m_qsCurrHandlerKey;
   // QString         m_qsCurrHandlerState;



};

