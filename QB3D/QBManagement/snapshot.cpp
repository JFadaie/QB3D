#include <QtAlgorithms>

#include "snapshot.h"
#include "geometryengine.h"
#include "collisiondetector.h"
#include "macros.h"

// TODO: make these work for any size of Cube.
#define   XPOS_MASK         0x03        // 0000 0011
#define   YPOS_MASK         0x0C        // 0000 1100
#define   YPOS_RSHIFT       2
#define   ZPOS_MASK         0x30        // 0011 0000
#define   ZPOS_RSHIFT       4

/***************************************************************
 *
 * Function: LEDCoords::bounds
 *
 * Description: Determines the min and max coordinate from the
 *              list provided for the given axis.
 *
 * Parameters:
 *      iAxis: integer representing the axis the bound is calculated.
 *      rCoordList: list of coordinates iterated over.
 *      riMin: reference to returned minimum value.
 *      riMax: reference to returned maximum value.
 */

void LEDCoords::bounds(
                   LEDCoords::Axis eAxis,
                   const QList<LEDCoords>& rCoordList,
                   int& riMin,
                   int& riMax )
{
    int iMax = 0;
    int iMin = INT_MAX;

    // Determine which coordinate axis is queried and get the
    // associated position.
    for ( int i = 0; i < rCoordList.size(); ++i ) {
        int iCoord;
        const LEDCoords& rCoord = rCoordList[i];
        switch ( eAxis ) {
            case XAXIS: {
                    iCoord = rCoord.iX;
                }
                break;
            case YAXIS: {
                    iCoord = rCoord.iY;
                }
                break;
            case ZAXIS: {
                    iCoord = rCoord.iZ;
                }
                break;
        }
        // Update the min and max position.
        if ( iCoord > iMax )
            iMax = iCoord;
        if ( iCoord < iMin )
            iMin = iCoord;
    }

    // Set the min and max bound.
    riMin = iMin;
    riMax = iMax;
}

/***************************************************************
 *
 * Function: SnapShot::constructor/
 *                     copy constructor/
 *                     move constructor
 * Description:
 *
 * Parameters:
 *
 */

SnapShot::SnapShot( QObject* pParent ) : QObject( pParent )
{
    GeometryEngine* pEngine = GeometryEngine::getEngine();


    m_pNameToCoordsMap = new QMap<QString, LEDCoords>();

    int iXDim = 0;
    int iYDim = 0;
    int iZDim = 0;
    pEngine->getCubeDimensions( iXDim, iYDim, iZDim );

    // Resize the LED Array to match the size of the virtual cube.
    resize( iXDim, iYDim, iZDim );
    
    // Iterate over the Geometry Engine objects. Store the names of only LED objects.
    // Initialize their states to false.
    for (int iIndex = 0; iIndex < pEngine->size(); ++iIndex ) {

        QString qsObjectName = pEngine->getObjectName( iIndex );
        if ( CollisionDetector::isSelectableObject( qsObjectName ) ) {
            QRegExp ledRegExp = GetLEDCoordRegex();
            if ( ledRegExp.indexIn( qsObjectName) != -1 ) {

                // Get the coordinates for the LEDData struct.
                QString qsCoordinates = ledRegExp.cap(1);
                int iX = qsCoordinates[0].digitValue();
                int iY = qsCoordinates[1].digitValue();
                int iZ = qsCoordinates[2].digitValue();

                //qDebug() << "Name Coords: " << qsCoordinates;
                m_LEDArray[iX][iY][iZ] = OFF;

                LEDCoords coords;
                coords.iX = iX;
                coords.iY = iY;
                coords.iZ = iZ;
                m_pNameToCoordsMap->insert( qsObjectName, coords );
             }
        }
    }
}

SnapShot::SnapShot( const SnapShot& rOther ) : QObject() {
    m_LEDArray = rOther.m_LEDArray;

    m_pNameToCoordsMap = new QMap< QString, LEDCoords>();
    // Begin copying coords map.

    QList<QString> keyList = rOther.m_pNameToCoordsMap->keys();
    QString qsKey;
    for ( int i = 0; i < keyList.size(); ++i ) {
        qsKey = keyList[i];
        LEDCoords ledCoords = rOther.m_pNameToCoordsMap->value( qsKey );
        m_pNameToCoordsMap->insert( qsKey, ledCoords );
    }
}

SnapShot::SnapShot( SnapShot&& rOther ) : QObject()
{
    swap( *this, rOther );
}


// When adding the states of the provided snapshots should be either on or
// off
/***************************************************************
 *
 * Function: SnapShot::addOptionToOriginal
 *
 * Description: Adds the modified list of snapshots to the original
 *              list of snapshots per index. The function assumes
 *              that the size of the
 *
 * Parameters:
 *
 */

void SnapShot::addOptionToOriginal(
                QList<SnapShot *>&          rResultList,
                const QList<SnapShot *>&    rSnapOrigList,
                const QList<SnapShot *>&    rSnapOptList)
{
    for ( int i = 0; i < rSnapOrigList.size(); ++i ) {
        SnapShot* const pResult   = rResultList[i];
        SnapShot* const pOriginal = rSnapOrigList[i];
        SnapShot* const pOption   = rSnapOptList[i];

        addOptionToOriginal(
                            *pResult,
                            *pOriginal,
                            *pOption );
    }
}

void SnapShot::addOptionToOriginal(
                SnapShot&       rResult,
                const SnapShot& rSnapOrig,
                const SnapShot& rSnapOpt )
{
    for ( int i = 0; i < rSnapOrig.size(); ++i ) {
        // if provided led is on, turn turn the corresponding snap shot LED off.
        QString qsLEDName = rSnapOrig.getLEDName( i );
        LEDState eOrigState = rSnapOrig.getLEDState( i );
        LEDState eOptState = rSnapOpt.getLEDState( i );

        // Assert the proper state of the Snapsshots LEDs.
        Q_ASSERT( eOrigState == OFF || eOrigState == SELECTED );
        Q_ASSERT( eOptState == OFF || eOptState == SELECTED );
        LEDState eResultState = OFF;
        if ( eOrigState == OFF && eOptState == OFF )
            eResultState = OFF;
        else if ( eOrigState == OFF && eOptState == SELECTED )
            eResultState = OPTION;
        else if ( eOrigState == SELECTED && eOptState == OFF )
            eResultState = SELECTED;
        else //  ( eOrigState == SELECTED && eOptState == SELECTED )
            eResultState = SELECTED_OPTION;

        rResult.setLEDState( qsLEDName, eResultState );
     }
}


/***************************************************************
 *
 * Function: SnapShot::subOptFromOriginal
 *
 * Description:
 *
 * Parameters:
 *
 */

void SnapShot::subOptFromOriginal(
                    SnapShot&       rResult,
                    const SnapShot& rSnapOrig,
                    const SnapShot& rSnapOpt )
{

    for ( int i = 0; i < rSnapOrig.size(); ++i ) {
        // if provided led is on, turn turn the corresponding snap shot LED off.
        QString qsLEDName = rSnapOrig.getLEDName( i );
        LEDState eOrigState = rSnapOrig.getLEDState( i );
        LEDState eOptState = rSnapOpt.getLEDState( i );

        // Assert the proper state of the Snapsshots LEDs.
        Q_ASSERT( eOrigState == OFF || eOrigState == SELECTED );
        Q_ASSERT( eOptState == OFF || eOptState == SELECTED );
        LEDState eResultState = OFF;
        if ( eOrigState == OFF && eOptState == OFF )
            eResultState = OFF;
        else if ( eOrigState == OFF && eOptState == SELECTED )
            eResultState = OPTION_SUB;
        else if ( eOrigState == SELECTED && eOptState == OFF )
            eResultState = SELECTED;
        else //  ( eOrigState == SELECTED && eOptState == SELECTED )
            eResultState = SELECTED_OPTION_SUB;

        rResult.setLEDState( qsLEDName, eResultState );
     }

}

void SnapShot::setSelectedLEDs( const LEDArray& rLEDArray )
{
    for (int iX = 0; iX < rLEDArray.size(); ++iX ) {
        for ( int iY = 0; iY < rLEDArray[0].size(); ++iY ) {
            for ( int iZ = 0; iZ < rLEDArray[0][0].size(); ++iZ ) {
                m_LEDArray[iX][iY][iZ] = rLEDArray[iX][iY][iZ];
            }
        }
    }
}


void SnapShot::toggleSelLED( const QString& rqsObjectName )
{
    Q_ASSERT( m_pNameToCoordsMap->contains( rqsObjectName ) );
    LEDCoords coords = m_pNameToCoordsMap->value( rqsObjectName );
    qDebug() << "Coordinate Pos: "
             << coords.iX << ":"<< coords.iY << ":" << coords.iZ;
    LEDState eLEDState = getLEDState( coords );
    if ( eLEDState > OFF)
        setLEDState( rqsObjectName, OFF );
    else
        setLEDState( rqsObjectName, SELECTED );
}


void SnapShot::setLEDState( const QString& rqsObjectName,
                            LEDState eLEDState )
{
    Q_ASSERT( m_pNameToCoordsMap->contains( rqsObjectName ) );
    LEDCoords p = m_pNameToCoordsMap->value( rqsObjectName );
    m_LEDArray[p.iX][p.iY][p.iZ] = eLEDState;
}


LEDState SnapShot::getLEDState( const QString& rqsObjectName ) const
{
    // Assumes LED is off.
    LEDState eLEDState = OFF;

    if ( m_pNameToCoordsMap->contains( rqsObjectName ) ) {
        LEDCoords p = m_pNameToCoordsMap->value( rqsObjectName );
        eLEDState = m_LEDArray[p.iX][p.iY][p.iZ];
    }
    return eLEDState;
}


LEDState SnapShot::getLEDState( int iIndex ) const
{
    Q_ASSERT( iIndex >= 0 && iIndex < size() );
    LEDState eState = OFF;
    int iX, iY, iZ;
    translateIndex( iIndex, iX, iY, iZ );
    eState = m_LEDArray[iX][iY][iZ];
    return eState;
}


QString SnapShot::getLEDName( int iIndex ) const
{
    Q_ASSERT( iIndex >= 0 && iIndex < size() );

    int iX, iY, iZ;
    QString qsName;
    translateIndex( iIndex, iX, iY, iZ );
    const QList<QString>& rListNames = getLEDNames();
    for ( int iIndex = 0; iIndex < rListNames.size(); ++iIndex ) {
        qsName = rListNames[iIndex];
        const LEDCoords& rP = m_pNameToCoordsMap->value( qsName );
        if ( iX == rP.iX && iY == rP.iY && iZ == rP.iZ ) {
            break;
        }
    }
    return qsName;
}

QList<QString> SnapShot::getLEDNames() const
{
    const QList<QString>& listNames = m_pNameToCoordsMap->keys();
    return listNames;
}


void SnapShot::resize( int iXDim, int iYDim, int iZDim )
{
    // Resize the LED Array to be 4x4x4 in dimensions.
    m_LEDArray.resize( iXDim );
    for ( int iX = 0; iX < iXDim; ++iX ) {
        m_LEDArray[iX].resize(iYDim);

        for ( int iY = 0; iY < iYDim; ++iY ) {
            m_LEDArray[iX][iY].resize( iZDim );
        }
    }
}


SnapShot& SnapShot::operator= ( SnapShot other )
{
    swap( *this, other );
    return *this;
}

#if 0
SnapShot& SnapShot::operator= ( const SnapShot& rOther )
{
    if ( this != &rOther ) {

        m_LEDArray = rOther.m_LEDArray;

        delete m_pNameToCoordsMap;

        QMap<QString, LEDCoords>* pNewMap = new QMap< QString, LEDCoords>();

        // Begin copying coords map.
        QList<QString> keyList = rOther.m_pNameToCoordsMap->keys();
        QString qsKey;
        for ( int i = 0; i < keyList.size(); ++i ) {
            qsKey = keyList[i];
            LEDCoords ledCoords = rOther.m_pNameToCoordsMap->value( qsKey );
            pNewMap->insert( qsKey, ledCoords );
        }

        m_pNameToCoordsMap = pNewMap;
    }

    return *this;
}
#endif



void swap( SnapShot& rSnapShot1, SnapShot& rSnapShot2 )
{
    qSwap( rSnapShot1.m_LEDArray, rSnapShot2.m_LEDArray );
    qSwap( rSnapShot1.m_pNameToCoordsMap, rSnapShot2.m_pNameToCoordsMap );
}


void SnapShot::translateIndex( int iIndex, int& riX, int& riY, int& riZ ) const
{
    riX = iIndex & XPOS_MASK;
    riY = (iIndex & YPOS_MASK ) >> YPOS_RSHIFT;
    riZ = (iIndex & ZPOS_MASK ) >> ZPOS_RSHIFT;
  //  qDebug() << "iIndex:" << iIndex;
  //  qDebug() << "iX:" << riX;
  //  qDebug() << "iY:" << riY;
  //  qDebug() << "iZ:" << riZ;
    Q_ASSERT( riX >= 0 && riX < sizeX() );
    Q_ASSERT( riY >= 0 && riY < sizeY() );
    Q_ASSERT( riZ >= 0 && riZ < sizeZ() );
}

void SnapShot::clear()
{
    for ( int i = 0; i < size(); ++i ) {
        QString qsLEDName = getLEDName( i );
        setLEDState( qsLEDName, LEDState::OFF );
     }
}


void SnapShot::finalize()
{
    for ( int i = 0; i < size(); ++i ) {
        const QString& rqsName = getLEDName( i );
        LEDState eState = getLEDState( i );
        qDebug() << " LED Name: " << rqsName;
        qDebug() << " LED State: " << eState;
        if ( eState > OFF )
            eState = SELECTED;
        else
            eState = OFF;
        setLEDState( rqsName, eState );
    }
}
