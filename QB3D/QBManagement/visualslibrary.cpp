/**************************************************
 *  File: visualslibrary.cpp
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include "visualslibrary.h"
#include "selectionhandler.h"
#include "animationhandler.h"
#include "creationlibrary.h"
#include "visualtitles.h"
#include "qbglobals.h"



/***************************************************************
 *
 * Function: Constructor/Destructor
 *
 * Description: constructor/destructor.
 *
 * Parameter:
 *      pParent: pointer to the parent.
 */

VisualsLibrary::VisualsLibrary( QObject* pParent ) :
    QObject( pParent )
{
}


VisualsLibrary::~VisualsLibrary()
{
    BaseFactory::TemplateLibrary* pLibV = BaseFactory::getVisualMap();
    delete pLibV;
}


/***************************************************************
 *
 * Function: VisualsLibrary::getVisualHandler
 *           VisualsLibrary::getSelectionHandler
 *           VisualsLibrary::getAnimationHandler
 *
 * Description: Retrieves the Handler associated with the provided
 *              key.
 *
 * Parameter:
 *      rqsKey: Key used to obtain the associated visual handler.
 */

SelectionHandler* VisualsLibrary::getSelectionHandler( const QString& rqsKey )
{
    VisualHandler* pHandlerObj = createStoredType( rqsKey, SELECTION_HANDLER );
    SelectionHandler* pHandler = nullptr;
    if ( pHandlerObj )
        pHandler = qobject_cast<SelectionHandler* >( pHandlerObj );
    return pHandler;
}

AnimationHandler* VisualsLibrary::getAnimationHandler( const QString& rqsKey )
{
    VisualHandler* pHandlerObj = createStoredType( rqsKey, ANIMATION_HANDLER );
    AnimationHandler* pHandler = nullptr;
    if ( pHandlerObj )
        pHandler = qobject_cast<AnimationHandler* >( pHandlerObj );
    return pHandler;
}

VisualHandler* VisualsLibrary::getVisualHandler( const QString &rqsKey )
{
    VisualHandler* pHandlerObj = createStoredType( rqsKey );
    return pHandlerObj;
}

/***************************************************************
 *
 * Function: VisualsLibrary::getPanelData
 *
 * Description: Retrieves the panel data associated with the
 *              provided key. The panel data contains the list
 *              of effect states, the effect type, etc.
 *
 * Parameter:
 *      rqsKey: Key used to obtain the associated visual handlers
 *              panel data.
 */

PNLHANDLER_DATA* VisualsLibrary::getPanelData( const QString &rqsKey )
{
  VisualHandler* const pVisHandler = createStoredType( rqsKey );
  PNLHANDLER_DATA* const pPanelData = pVisHandler->getPanelData();
  delete pVisHandler;
  return pPanelData;
}

/***************************************************************
 * Function: VisualsLibrary::contains
 *
 * Description: Returns true if the visual handler assocaited with
 *              key can be created from the library.
 *
 * Parameter:
 *      rqsKey: key used to obtain the associated visual handler.
 */

bool VisualsLibrary::contains( const QString& rqsKey )
{
    BaseFactory::TemplateLibrary* const pLib = BaseFactory::getVisualMap();
    return pLib->contains( rqsKey );
}

/***************************************************************
 *
 * Function: VisualsLibrary::retrieveType
 *
 * Description:
 *
 *
 * Parameter:
 *
 */
#if 0
VisualHandler* VisualsLibrary::retrieveType( const QString& rqsKey, const QString& rqsType )
{
    if ( m_pVisualsHandler != nullptr ) {
        const QString& rqsHandlerKey  = m_pVisualsHandler->handlerKey();
        const QString& rqsHandlerType = m_pVisualsHandler->handlerType();
        if ( rqsHandlerKey == rqsKey && rqsHandlerType == rqsType )
            return m_pVisualsHandler;
    }
    return nullptr;
}

VisualHandler* VisualsLibrary::retrieveType( const QString& rqsKey )
{
    if ( m_pVisualsHandler != nullptr ) {
        const QString& rqsHandlerKey  = m_pVisualsHandler->handlerKey();
        if ( rqsHandlerKey == rqsKey )
            return m_pVisualsHandler;
    }
    return nullptr;
}
#endif

/***************************************************************
 *
 * Function: VisualsLibrary::createStoredType
 *
 * Description: Creates an object from the factory corresponding to the
 *              provided key and of the provided string type. The function
 *              will throw an error if the key or type provided are
 *              invalid.
 *
 * Note: The object of the provided type and key is first searched for in the
 *       library of object already created. If found, this object is returned.
 *
 * Parameter:
 *      rqsKey: reference to the key corresponding to the object created.
 *      rqsType: reference to the type of object created with the key.
 */

VisualHandler* VisualsLibrary::createStoredType(
                                                const QString& rqsKey,
                                                const QString& rqsType )
{
    // The type must be valid.
    Q_ASSERT( QB::ValidHandlerType( rqsType ) );

    VisualHandler* (*createType)() = nullptr;
    BaseFactory::TemplateLibrary* pLib = BaseFactory::getVisualMap();


    // The creation key needs to be valid.
    Q_ASSERT( pLib->contains( rqsKey ) );
    createType = pLib->value( rqsKey );

    // Create the object and insert into the map.
    VisualHandler* const pHandlerObject = createType();

    // Ensure the created visual handler is of the proper type.
    Q_ASSERT( pHandlerObject->handlerType() == rqsType );

    return pHandlerObject;
}


VisualHandler* VisualsLibrary::createStoredType( const QString& rqsKey )
{
    VisualHandler* (*createType)() = nullptr;
    BaseFactory::TemplateLibrary* pLib = BaseFactory::getVisualMap();


    // The creation key needs to be valid.
    Q_ASSERT( pLib->contains( rqsKey ) );
    createType = pLib->value( rqsKey );

    // Create the object and insert into the map.
    VisualHandler* const pHandlerObject = createType();

    return pHandlerObject;
}
