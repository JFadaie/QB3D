/**************************************************
 *  File: creationlibrary.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once
#include <QMap>
#include <QObject>
#include "visualhandler.h"

template<typename T > VisualHandler* createType() {
    return new T;
}


struct BaseFactory {
    friend class VisualsLibrary;
    typedef QMap<QString, VisualHandler*(*)() > TemplateLibrary;

private:
    static TemplateLibrary* pVisualLib;

protected:
    static TemplateLibrary * getVisualMap() {
        if(!pVisualLib) { pVisualLib = new TemplateLibrary; }
        return pVisualLib; }
};

template<typename T>
struct RegisterVisualType: BaseFactory {
    friend class VisualsLibrary;

    RegisterVisualType( const QString qsKey ) {
        TemplateLibrary* pLib = getVisualMap();
        pLib->insert( qsKey, &createType<T>);
    }
};
