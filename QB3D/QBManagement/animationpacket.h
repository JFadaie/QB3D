#pragma once

#include <QObject>

#include "qbglobals.h"

class SnapShot;

class AnimationPacket : public QObject {
    Q_OBJECT
public:
    explicit AnimationPacket( Object* pParent = 0 );
    AnimationPacket( const AnimationPacket& rOther );
    AnimationPacket( AnimationPacket&& rOther );
    ~AnimationPacket();

    const QString<SnapShot*>& constKeyFrames() {
                                    return m_KeyFrameList; }
    int                       keyFrameSize() {
                                    return m_KeyFrameList.size(); }
    QString                   key() { return m_qsKey; }
    QString                   type() { return m_qsType; }
    QB::AnimationSpeed        speed() { return m_eSpeed; }
    int                       repetitions() { return m_iRepetitions; }
    int                       startFrameIndex() { return m_iFrameIndex; }
    int                       frameSize() { return m_iFrameSize; }
    bool                      contains( int iFrameIndex );
    SnapShot*                 frame( int iFrameIndex );
    void                      setKeyFrameList( const QList<SnapShot*>& rSnapList );
    void                      setStartFrameIndex( int iFrameIndex );
    void                      setKey( const QString& rqsKey );
    void                      setRepetitions( int iRepetitions );
    void                      setSpeed( QB::AnimationSpeed eSpeed );
    void                      operator= ( AnimationPacket other );
    friend void               swap( AnimationPacket& rPacket1, AnimationPacket& rPacket2 );
signals:

public slots:

private:
    QList<SnapShot*>        m_KeyFrameList;
    QString                 m_qsKey;
    QString                 m_qsType;
    QB::AnimationSpeed      m_eSpeed;
    int                     m_iRepetitions;
    int                     m_iFrameIndex;
    int                     m_iFrameSize;

    void                    calcFrameSize();
};

