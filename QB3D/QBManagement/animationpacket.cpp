#include "animationpacket.h"

AnimationPacket::AnimationPacket(QObject* pParent) : QObject( pParent )
{
    m_eSpeed = QB::FPS;
    m_iRepetitions = 1;
    m_iFrameIndex = 0;
    m_iFrameSize = -1;
}

AnimationPacket::~AnimationPacket()
{

}


void AnimationPacket::setKeyFrameList( const QList<SnapShot*>& rSnapList )
{
    m_KeyFrameList = rSnapList;
    calcFrameSize();
}

void AnimationPacket::setStartFrameIndex( int iFrameIndex )
{
    m_iFrameIndex = iFrameIndex;
}

void AnimationPacket::setKey( const QString& rqsKey )
{
    m_qsKey = rqsKey;
}

void AnimationPacket::setType( const QString& rqsType )
{
    m_qsType = rqsType;
}

void AnimationPacket::setRepetitions( int iRepetitions )
{
    m_iRepetitions = iRepetitions;
}

void AnimationPacket::setSpeed( QB::AnimationSpeed eSpeed )
{
    m_eSpeed = eSpeed;
    calcFrameSize();
}


void AnimationPacket::calcFrameSize()
{
    m_iFrameSize = ( ( m_KeyFrameList.size() * MAX_FPS / m_eSpeed );
}


bool AnimationPacket::contains( int iFrameIndex )
{

}

SnapShot*  AnimationPacket::frame( int iFrameIndex )
{

}
