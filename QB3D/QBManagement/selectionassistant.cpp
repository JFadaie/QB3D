#include <QEvent>
#include <QKeyEvent>

#include "selectionassistant.h"
#include "visualslibrary.h"
#include "visualtitles.h"
#include "panelhandler.h"
#include "animationhandler.h"
#include "selectionhandler.h"
#include "qbglobals.h"


// TODO: Determine if I just want to have a master and a slave SelectionAssistant
// The master would be in the GLWidget and the slave in the mainwindow? Or the other
// way around? Good idea. That way I do not have to repeat things. And the
// communication objects are encapsulated better.

SelectionAssistant::SelectionAssistant( QObject* pParent) :
    QObject( pParent )
{
    m_bPrimed = false;
    m_OrigSnapShot = SnapShot();
    m_pVisualHandler = nullptr;

    // Initialization might not be necessary.
    VisualsLibrary* const pLib = VisualsLibrary::getLibrary();
    m_pVisualHandler = pLib->getSelectionHandler( PLANE_SELECTOR );
}

SelectionAssistant::~SelectionAssistant()
{
    if ( m_pVisualHandler != nullptr )
        delete m_pVisualHandler;
}

bool SelectionAssistant::eventFilter( QObject *pObject, QEvent *pEvent )
{
    QEvent::Type eType = pEvent->type();

    if ( eType == QEvent::KeyPress && m_bPrimed ) {
        QKeyEvent* pKeyEvent = static_cast<QKeyEvent*>(pEvent);
        int eKey = pKeyEvent->key();
        const QString& rqsHandlerType = m_pVisualHandler->handlerType();

        switch( eKey ) {
            case Qt::Key_Escape: {
                qDebug() << "Escape Key Pressed";
                m_bPrimed = false;
                emit undoEffect();
            }
                break;
            case Qt::Key_Return:
            case Qt::Key_Enter: {
                qDebug() << "Return/Enter Key Pressed";
                m_bPrimed = false;
                emit commitEffect();
            }
                break;
            case Qt::Key_A: {
                qDebug() << "A Key Pressed";
                if ( rqsHandlerType == SELECTION_HANDLER ) {
                    SelectionHandler* pSelectionHandler =
                            qobject_cast<SelectionHandler*>( m_pVisualHandler );
                    SnapShot* pCurrSnap = pSelectionHandler->getPreviousSnapshot();
                    QList<SnapShot* > snapShotList;
                    snapShotList.append( pCurrSnap );
                    const AnimationPacket& rAniPacket = createAniPacket( snapShotList );
                    emit animationPacket( rAniPacket );
                }
                else {
                    // ANIMATION_TYPE - TODO:
                }
            }
                break;
            case Qt::Key_D: {
                qDebug() << "D Key Pressed";
                if ( rqsHandlerType == SELECTION_HANDLER ) {
                    SelectionHandler* pSelectionHandler =
                            qobject_cast<SelectionHandler*>( m_pVisualHandler );
                    SnapShot* pCurrSnap = pSelectionHandler->getNextSnapshot();
                    QList<SnapShot* > snapShotList;
                    snapShotList.append( pCurrSnap );
                    const AnimationPacket& rAniPacket = createAniPacket( snapShotList );
                    emit animationPacket( rAniPacket );
                 }
                else {
                    // ANIMATION_TYPE - TODO:
                }
            }
                break;
            default:
                qDebug() << "Default Case in KeyPressEvent";
                break;
        }
    }

    return false;
}

void SelectionAssistant::onPanelStateChanged(
                            const QString& rqsHandlerState )
{
    m_pVisualHandler->setEffectState( rqsHandlerState );
    if ( m_bPrimed )  // Reset the displayed snapshot to the original.
        emit undoEffect();
    m_bPrimed = false;
    qDebug() << "SelectionAssistant::onPanelStateChanged: New Handler State";

}

void SelectionAssistant::onPanelChanged(
                            const QString& rqsHandlerKey,
                            const QString& rqsHandlerState )
{
    VisualsLibrary* const pLib = VisualsLibrary::getLibrary();
    m_pVisualHandler->deleteLater(); // Delete the current Visuals Handler.
    m_pVisualHandler = pLib->getVisualHandler( rqsHandlerKey );
    m_pVisualHandler->setEffectState( rqsHandlerState );
    if ( m_bPrimed )  // Reset the displayed snapshot to the original.
        emit undoEffect();
    m_bPrimed = false;
    qDebug() << "SelectionAssistant::onPanelChanged: New Handler Key";
}

// NOTE: this needs to be a state machine as it should only apply key press events
// if there are options and if it hasalready been primed with a LEDCoord(s).

void SelectionAssistant::onUserSelection( const LEDCoords& rLedCoords )
{
    if ( m_pVisualHandler == nullptr ) {
        qDebug() << "SelectionAssistant::onUserSelection - m_pVisualHandler is null!";
        return; // Nothing to do.
    }

    // Determine whether the Visual Handler is an Animation or Selection Handler.
    if ( m_pVisualHandler->handlerType() == SELECTION_HANDLER ) {
        SelectionHandler* const pSelHandler =
                    qobject_cast<SelectionHandler*>( m_pVisualHandler );
        SELHANDLER_DATA selData;
        selData.ledCoord = rLedCoords;
        selData.qsEffectState = pSelHandler->getEffectState();

        bool bValidEffect;
        m_bPrimed = pSelHandler->primeSelector( selData, bValidEffect );

        if ( bValidEffect ) {
            SnapShot* pCurrSnap = pSelHandler->getCurrentSnapshot();
            QList<SnapShot* > snapShotList;
            snapShotList.append( pCurrSnap );
            const AnimationPacket& rAniPacket = createAniPacket( snapShotList );
            emit animationPacket( rAniPacket );
        }
    }
    else {
        // ANIMATION_TYPE - TODO:
    }


}

#if 0
// Handles user moving the snapshot manager coorelated slider.
void SelectionAssistant::onNewSnapShot( const SnapShot &rSnapShot )
{
    m_OrigSnapShot = rSnapShot;
    m_bPrimed = false;
    emit updateSnapShot( m_OrigSnapShot );
}

#endif


// Handles user moving the snapshot manager coorelated slider.
AnimationPacket SelectionAssistant::createAniPacket( const QList<SnapShot *> &rSnapShotList )
{
    // Determine whether there is 1 or more snapshots in the list
    // if not 1, then the visual handler is an animaton, otherwise it is a selector
    //
    AnimationPacket aniPacket;

    if ( rSnapShotList.size() > 1) {
       // aniPacket.setSpeed(  ); TODO
       // aniPacket.setRepetitions( );
        aniPacket.setType( ANIMATION_HANDLER );
    }
    else {
        aniPacket.setType( SELECTION_HANDLER );
    }

    aniPacket.setKeyFrameList( rSnapShotList );
    aniPacket.setKey( m_pVisualHandler->handlerKey() );
    return aniPacket;
}


