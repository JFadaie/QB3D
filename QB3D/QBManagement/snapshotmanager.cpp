#include "snapshotmanager.h"
#include "qbglobals.h"

SnapShotManager::SnapShotManager(QObject *parent) : QObject(parent)
{
    m_iCurrentIndex = 0;
    m_bCommitted = true;
    m_pSuggestedPacket = nullptr;

    for ( int i = 0; i < DEFAULT_NUM_SNAPSHOTS; ++i ) {
        SnapShot* pSnapShot = new SnapShot( this );
        m_SnapShotPtrs.append( pSnapShot );
    }
}

SnapShotManager::~SnapShotManager()
{

}


void SnapShotManager::addSnapShot()
{
    SnapShot* pSnapShot = new SnapShot( this );
    m_SnapShotPtrs.append( pSnapShot );
}


void SnapShotManager::insertSnapShot()
{
    SnapShot* pSnapShot = new SnapShot( this );
    m_SnapShotPtrs.insert( m_iCurrentIndex, pSnapShot );
}

void SnapShotManager::deleteSnapShot()
{
    SnapShot* pSnapShot = m_SnapShotPtrs.takeAt( m_iCurrentIndex );
    delete pSnapShot;
}

void SnapShotManager::onCurrentIndexChanged( const int iIndex )
{
    m_iCurrentIndex = iIndex;
}

void SnapShotManager::setCurrentIndex( const int iIndex )
{
    m_iCurrentIndex = iIndex;
}

void SnapShotManager::setCurrentSnapShot( const SnapShot& rNewSnapShot )
{
    SnapShot* const pSnapShot = m_SnapShotPtrs[m_iCurrentIndex];
    (*pSnapShot) = rNewSnapShot;
}

SnapShot SnapShotManager::currentSnapShot()
{
    // This needs to be updated by searching the list of animation packets

    // SnapShot resultSnap;
    // loop over the list of animation packets
        // get the first animation packet
        // if the animation packet contains index frame
    SnapShot* const pSnapShot = m_SnapShotPtrs[m_iCurrentIndex];
    return pSnapShot;
}

void SnapShotManager::suggest(
                            AnimationPacket * const pPacket,
                            QB::SnapShotOp          eOperation )
{
    // Create copy of suggested packet
    // set the packet equal to the
    // add
}

void SnapShotManager::isCommitted()
{
    return m_pSuggestedPacket == nullptr;
}
