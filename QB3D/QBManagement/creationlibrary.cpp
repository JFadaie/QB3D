/**************************************************
 *  File: creationlibrary.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include "creationlibrary.h"

BaseFactory::TemplateLibrary* BaseFactory::pVisualLib = nullptr;
