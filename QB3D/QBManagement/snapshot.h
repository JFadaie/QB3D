/**************************************************
 *  File: snapshot.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QObject>
#include <QVector>
#include <QMap>

struct LEDData;

#if 0
// Possibly use this for better encapsulation.
struct LEDData {
    friend class SnapShot;
private:

};
#endif

struct LEDCoords {
    int iX;
    int iY;
    int iZ;
    enum Axis {
        XAXIS,
        YAXIS,
        ZAXIS
    };

    bool equal( const LEDCoords& rOther ) const {
        if ( iX == rOther.iX &&
             iY == rOther.iY &&
             iZ == rOther.iZ )
            return true;
        return false; }
    static void boundsX( const QList<LEDCoords>& rCoordList,
                         int& riMinX,
                         int& riMaxX ) {
                            bounds( LEDCoords::XAXIS, rCoordList, riMinX, riMaxX ); }
    static void boundsY( const QList<LEDCoords>& rCoordList,
                         int& riMinY,
                         int& riMaxY ) {
                            bounds( LEDCoords::YAXIS, rCoordList, riMinY, riMaxY ); }
    static void boundsZ( const QList<LEDCoords>& rCoordList,
                         int& riMinZ,
                         int& riMaxZ ) {
                            bounds( LEDCoords::ZAXIS, rCoordList, riMinZ, riMaxZ ); }
        ;
private:
    static void bounds( LEDCoords::Axis iAxis,
                        const QList<LEDCoords>& rCoordList,
                        int& riMin,
                        int& riMax );
};

// TODO: possibly use a Qt QFlag for this.
enum LEDState {
    SELECTED_OPTION_SUB = -2,
    OPTION_SUB = -1,
    OFF = 0,
    SELECTED = 1,
    OPTION = 2,
    SELECTED_OPTION = 3
};

typedef QVector<QVector<QVector<LEDState> > >    LEDArray;

class SnapShot : public QObject {
    Q_OBJECT
public:
    explicit                    SnapShot( QObject* pParent = 0 );
                                SnapShot( const SnapShot& rOther );
                                SnapShot( SnapShot&& rOther );
                                ~SnapShot() {
                                    delete m_pNameToCoordsMap;
                                }

    static void                 subOptFromOriginal(
                                    SnapShot&       rResult,
                                    const SnapShot& rSnapOrig,
                                    const SnapShot& rSnapOpt );
    static void                 addOptionToOriginal(
                                    SnapShot&       rResult,
                                    const SnapShot& rSnapOrig,
                                    const SnapShot& rSnapOpt );
    static void                 addOptionToOriginal(
                                    QList<SnapShot*>&        rResultList,
                                    const QList<SnapShot* >& rSnapOrigList,
                                    const QList<SnapShot* >& rSnapOptList);

    void                        clear();
    void                        finalize();
    void                        setSelectedLEDs( const LEDArray& rLEDArray );

    const LEDArray&             getSelectableLEDSConst() const {
                                    return m_LEDArray;
                                }

    LEDArray&                   getSelectableLEDS() {
                                    return m_LEDArray;
                                }

    void                        toggleSelLED( const QString& rqsObjectName );
    void                        setLEDState( const QString& rqsObjectName,
                                             LEDState eLEDState );

    LEDState                   getLEDState( const QString& rqsObjectName ) const;
    LEDState                   getLEDState( int iIndex ) const ;
    LEDState                   getLEDState( LEDCoords& rCoords ) const {
                                    LEDState eState = m_LEDArray[rCoords.iX][rCoords.iY][rCoords.iZ];
                                    return eState;
                                }


    QString                     getLEDName( int iIndex ) const;
    LEDCoords                   getLEDCoords( const QString& rqsObjectName ) {
                                    Q_ASSERT( m_pNameToCoordsMap->contains( rqsObjectName ) );
                                    return m_pNameToCoordsMap->value( rqsObjectName ); }


    QList<QString>              getLEDNames() const;

    // Assumes the sizes are consistent across dimensions.
    int                         size() const {
                                    return sizeX() * sizeY() * sizeZ(); }
    int                         sizeX() const {
                                    return m_LEDArray.size(); }
    int                         sizeY() const {
                                    return m_LEDArray[0].size(); }
    int                         sizeZ() const {
                                    return m_LEDArray[0][0].size(); }

    SnapShot&                   operator= ( SnapShot other );
    //SnapShot&                   operator - and + and -= and +=

    friend void                 swap( SnapShot& rSnapShot1, SnapShot& rSnapShot2 );
signals:

public slots:

private:
    QMap<QString, LEDCoords>*   m_pNameToCoordsMap;
    LEDArray                    m_LEDArray;


    void                    resize( int iXDim, int iYDim, int iZDim );
    void                    translateIndex( int iIndex, int& riX, int& riY, int& riZ  ) const;
};

