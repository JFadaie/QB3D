/**************************************************
 *  File: singleton.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once


template <class T> class Singleton {

public:
    static T* GetInstance() {
        if ( !s_pInstance ) {
            s_pInstance = new T;
        }
        return s_pInstance;
    }



private:
    static T* s_pInstance;

    Singleton() {}
    virtual ~Singleton() {
        if ( s_pInstance ) {
            s_pInstance = 0;
        }
    }

    Singleton( const Singleton& );     // Don't implement.
    Singleton& operator=( const Singleton& ); // Don't implement.

};

template<class T> T* Singleton<T>::s_pInstance = 0;
