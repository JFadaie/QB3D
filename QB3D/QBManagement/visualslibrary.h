/**************************************************
 *  File: visualslibrary.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QObject>
#include <QMultiMap>
#include <QDebug>

#include "singleton.h"
#include "visualhandler.h"

class AnimationHandler;
class SelectionHandler;

class VisualsLibrary : public QObject {
    Q_OBJECT
    friend class Singleton<VisualsLibrary>;

public:

    ~VisualsLibrary();

    static VisualsLibrary*  getLibrary() {
         VisualsLibrary* pLibrary = Singleton<VisualsLibrary>::GetInstance();
         return pLibrary;
     }

    SelectionHandler*       getSelectionHandler( const QString& rqsKey );
    AnimationHandler*       getAnimationHandler( const QString& rqsKey );
    VisualHandler*          getVisualHandler( const QString& rqsKey );
    PNLHANDLER_DATA*        getPanelData( const QString& rqsKey );

    //VisualHandler*          getCurrentHandler() { return m_pVisualsHandler; }

    bool                    contains( const QString& rqsKey );

private:
    VisualsLibrary( QObject* pParent = 0 );

    // TODO: update create and retrieve or create function to retrieve if the object
    // is already in the checked out list.
    VisualHandler* createStoredType( const QString& rqsKey, const QString& rqsType );
    VisualHandler* createStoredType( const QString& rqsKey );
  //  VisualHandler* retrieveType( const QString& rqsKey, const QString& rqsType );
  //  VisualHandler* retrieveType( const QString& rqsKey );
private slots:
   // void onHandlerDestroyed( QObject* pHandler = 0 );
};

