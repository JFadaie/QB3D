INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/creationlibrary.h \
    $$PWD/selectionassistant.h \
    $$PWD/singleton.h \
    $$PWD/snapshot.h \
    $$PWD/snapshotmanager.h \
    $$PWD/visualslibrary.h \
    $$PWD/animationpacket.h

SOURCES += \
    $$PWD/creationlibrary.cpp \
    $$PWD/selectionassistant.cpp \
    $$PWD/singleton.cpp \
    $$PWD/snapshot.cpp \
    $$PWD/snapshotmanager.cpp \
    $$PWD/visualslibrary.cpp \
    $$PWD/animationpacket.cpp
