#-------------------------------------------------
#
# Project created by QtCreator 2015-02-08T15:01:54
#
#-------------------------------------------------

CONFIG   += c++11
QT       += core gui

QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
LIBS += -stdlib=libc++ -mmacosx-version-min=10.7

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QB3D
TEMPLATE = app

INCLUDEPATH += $$PWD/QBGlobal
DEPENDPATH  += $$PWD/QBGlobal
INCLUDEPATH += $$PWD/QBVisualEffects
DEPENDPATH  += $$PWD/QBVisualEffects
INCLUDEPATH += $$PWD/QBUserInterfaces
DEPENDPATH  += $$PWD/QBUserInterfaces
INCLUDEPATH += $$PWD/QBManagement
DEPENDPATH  += $$PWD/QBManagement
INCLUDEPATH += $$PWD/QBGeometryEngine
DEPENDPATH  += $$PWD/QBGeometryEngine
INCLUDEPATH += $$PWD/QBMainWindow
DEPENDPATH  += $$PWD/QBMainWindow

#SOURCES += main.cpp\
#        mainwindow.cpp \
#    objprocessor.cpp \
#    geometryengine.cpp \
#    glcubewidget.cpp \
#    snapshot.cpp \
#    snapshotmanager.cpp \
#    collisiondetector.cpp \
#    macros.cpp \
#    ctrlpnlmgr.cpp \
#    controlpanel.cpp \
#    ledarray.cpp \
#    effectspnlmgr.cpp \
#    effectspanel.cpp \
#    visualslibrary.cpp \
#    selectionhandler.cpp \
#    selectionassistant.cpp \
#    panelhandler.cpp \
#   # QBGlobal/qbglobals.cpp \
#    creationlibrary.cpp \
#    planeselector.cpp \
#    animationhandler.cpp \
#    communicationpacket.cpp \
#    controlhandler.cpp \
#    visualctrlmngr.cpp \
#    aniplanecontroller.cpp


#HEADERS  += mainwindow.h \
#    objprocessor.h \
#    geometryengine.h \
#    glcubewidget.h \
#    snapshot.h \
#    snapshotmanager.h \
#    singleton.h \
#    collisiondetector.h \
#    macros.h \
#    ctrlpnlmgr.h \
#    controlpanel.h \
#    ledarray.h \
#    effectspnlmgr.h \
#    effectspanel.h \
#    visualslibrary.h \
#    selectionhandler.h \
#    visualtitles.h \
#    databuffer.h \
#    selectionassistant.h \
#   # QBGlobal/qbglobals.h \
#    panelhandler.h \
#    creationlibrary.h \
#    planeselector.h \
#    animationhandler.h \
#    communicationpacket.h \
#    controlhandler.h \
#    visualctrlmngr.h \
#    aniplanecontroller.h

#FORMS    += mainwindow.ui \
#    controlpanel.ui \
#    effectspanel.ui

RESOURCES += \
    resources.qrc

DISTFILES += \
    QBGlobal.pri \
    QBVisualEffects.pri \
    QBUserInterfaces.pri \
    QBManagement.pri

include(QBGlobal/QBGlobal.pri)
include(QBVisualEffects/QBVisualEffects.pri)
include(QBUserInterfaces/QBUserInterfaces.pri)
include(QBManagement/QBManagement.pri)
include(QBGeometryEngine/QBGeometryEngine.pri)
include(QBMainWindow/QBMainWindow.pri)
