#version 330

in vec3 ioPos3D_Eye;
in vec3 ioNorm3D_Eye;

out vec4 outColor4D;


// Fixed point light properties
vec3 lightPos3D_Eye = vec3( 0.0, 0.0, 10.0 );

vec3 Ls3D = vec3( 1.0, 1.0, 1.0 );  // White specular color
vec3 Ld3D = vec3( 0.7, 0.7, 0.7 );  // Dull-white diffuse color
vec3 La3D = vec3( 0.2, 0.2, 0.2 );  // Grey ambient color

// Surface Reflectance
vec3 Ks3D = vec3( 1.0, 1.0, 1.0 );  // Fully reflect white specular light
vec3 Kd3D;
vec3 Ka3D = vec3( 1.0, 1.0, 1.0 );  // Fully reflect ambient light
float fSpecularExp = 100.0; // Specular 'power'


// Uniform values
uniform int uiObjType;
uniform int uiLEDState;

// Forward Declarations.
void setKd3D(in int iObjType, in int iLEDState );


void main( void )
{
    // Ambient Intensity.
    vec3 Ia3D = La3D * Ka3D;

    // Calculate diffuse intensity
    vec3 distToLight3D_Eye = lightPos3D_Eye - ioPos3D_Eye;
    vec3 dirToLight3D_Eye = normalize( distToLight3D_Eye );

    // Note: Value is between 0 and 1.
    float fIdDotProd = dot( dirToLight3D_Eye, ioNorm3D_Eye );

    // Ensure value is not negative.
    fIdDotProd = max( fIdDotProd, 0.0 );


    // Diffuse Intensity.
    setKd3D( uiObjType, uiLEDState );
    vec3 Id3D = Ld3D * Kd3D * fIdDotProd;


    // Calculate Specular Intensity.
    vec3 lightReflection3D_Eye = reflect( -dirToLight3D_Eye,
                                          ioNorm3D_Eye );
    vec3 surfaceToViewer3D_Eye = normalize( -ioPos3D_Eye );

    float fIsDotProd = dot( lightReflection3D_Eye,
                            surfaceToViewer3D_Eye );
    fIsDotProd = max( fIsDotProd, 0.0 );
    float fSpecFactor = pow( fIsDotProd, fSpecularExp );

    // Speculat Intensity.
    vec3 Is3D = Ls3D * Ks3D * fSpecFactor;


    //outColor4D = vec4( 0.6, 0.1, 0.9, 1.0 );
    outColor4D = vec4( Is3D + Id3D + Ia3D, 1.0 );

}


// Determines the diffuse color property based on the object type
// chosen for drawing. If the object type is an LED, the color
// used will be darker blue if the object is selected by the user,
// otherwise ight blue.
void setKd3D(in int iObjType, in int iLEDState ) {

    switch ( iObjType) {
        case 0: {  // LED Type
            if ( iLEDState == int( -2) ) {
                // Light red diffuse reflectance.
                Kd3D = vec3( 1.0, 0.0, 0.0 );
            }
            else if ( iLEDState == int( - 1 ) ) {
                // Dark red diffuse reflectance.
                Kd3D = vec3( 0.8, 0.0, 0.0 );
            }
            else if ( iLEDState == int(1) ) {       // 01
                // Dark blue diffuse reflectance.
                Kd3D = vec3( 0.0, 0.2, 1.0 );
            }
            else if ( iLEDState == int(2) ) {  // 10
                // Green diffuse surface reflectance.
                Kd3D = vec3( 0.2, 1.0, 0.6 );
            }
            else if ( iLEDState == int(3) ) {   // 11
                // Light Green/Aqua surface reflectance.
                Kd3D = vec3( 0.0, 1.0, 1.0 );
            }
            else {                                  // 00
                // Blue diffuse surface reflectance.
                Kd3D = vec3( 0.2, 0.6, 1.0 );
            }

        }
            break;
        case 1: {  // Base Type
            // Dark brown diffuse reflectance.
           // Kd3D = vec3( 0.65, 0.16, 0.16 );
            Kd3D = vec3( 0.31, 0.16, 0.07 );
        }
            break;
        case 2: {  // Wiring Type
            // Steel color diffuse reflectance.
            Kd3D = vec3( 0.38, 0.38, 0.38 );
        }
            break;
        default: {
            // Green color.
            Kd3D = vec3( 0.0, 0.6, 0.0 );
        }
            break;
    }
}
