#include <float.h>

#include <QVector4D>
#include <qmath.h>

#include "geometryengine.h"
#include "collisiondetector.h"
#include "macros.h"



struct CollisionData {
    QString qsObjectName;
    float fIntercept;
};

CollisionDetector::CollisionDetector( const QMatrix4x4& rModelMat,
                                      const QMatrix4x4& rViewMat,
                                      const QMatrix4x4& rProjMat,
                                      QObject *parent ) :
                QObject(parent)
{
    m_ModelMatrix = rModelMat;
    m_ViewMatrix = rViewMat;
    m_ProjMatrix = rProjMat;

    m_qsCollisionObject = QString();

}


CollisionDetector::~CollisionDetector()
{

}

bool CollisionDetector::isSelectableObject( const QString &rqsObjectName )
{
    QString qsLEDQualifier = QString( LED_TYPE_QUALIFIER );
    return rqsObjectName.contains( qsLEDQualifier );
}


bool CollisionDetector::detectObjectCollision( const QPoint& rRayPoint,
                                               const QSize& rViewportSize )
{
    bool bCollision = false;
    QList<CollisionData> dataList;

    QVector3D fRayObject3D = calculateRayCast( rRayPoint,
                                               rViewportSize );


    intersectsBoundingSpheres( fRayObject3D,
                               dataList );


    QString qsObjectName = findIntersectingObject( dataList );

    if ( !qsObjectName.isNull() ) {
        bCollision = true;
        m_qsCollisionObject = qsObjectName;
    }

    return bCollision;
}




/***************************************************************
 * Function: CollisionDetector::calculateRayCast
 *
 * Description:
 *
 */

QVector3D CollisionDetector::calculateRayCast( const QPoint& rRayPoint,
                                               const QSize& rViewportSize )
{
    // Step 1: 3D Normalised Device Coords between [-1:1,-1:1,-1:1].
    float fNormDeviceX = ((2.0f *rRayPoint.x())/rViewportSize.width()) - 1.0f;
    float fNormDeviceY = 1.0f - ((2.0f *rRayPoint.y())/rViewportSize.height());
    float fNormDeviceZ = 1.0f;
    QVector3D fRayNormDevCoords3D( fNormDeviceX,
                                 fNormDeviceY,
                                 fNormDeviceZ );
    qDebug() << "Normalized Device Coords: " << fRayNormDevCoords3D;

    // Step 2: 4D Homogeneous Clip Coordinates between [-1:1,-1:1,-1:1, -1:1].
    QVector4D fRayHomogClipCoords4D( fRayNormDevCoords3D.x(),
                                     fRayNormDevCoords3D.y(),
                                     -1.0, 1.0 );
    qDebug() << "Homogeneous Clip Coords: " << fRayHomogClipCoords4D;

    // Step 3: 4D Eye (Camera) Coordinates between [-x:x, -y:y, -z:z, -w:w].
    bool bIsInverted = false;
    QMatrix4x4 invProjMatrix = m_ProjMatrix.inverted( &bIsInverted );
    QVector4D fRayEye4D;
    if ( bIsInverted || !invProjMatrix.isIdentity() ) {
        fRayEye4D = invProjMatrix*fRayHomogClipCoords4D;
        fRayEye4D = QVector4D( fRayEye4D.toVector2D(), -1.0, 0.0);
        qDebug() << " 4D Eye Coords: " << fRayEye4D;
    } else {
        qDebug() << "Projection matrix could not be inverted!";
        return QVector3D();
    }

    // Step 4: 4D World Coordinates between [-x:x, -y:y, -z:z, -w:w].
    bIsInverted = false;
    QMatrix4x4 invViewMatrix = m_ViewMatrix.inverted( &bIsInverted );
    QVector4D fRayWorld4D;
    if ( bIsInverted || !invViewMatrix.isIdentity() ) {
        fRayWorld4D = (invViewMatrix*fRayEye4D);
        qDebug() << " 4D World Coords: " << fRayWorld4D;
    } else {
        qDebug() << "View matrix could not be inverted!";
        return QVector3D();
    }
    QVector3D fRayWorld3D = fRayWorld4D.toVector3D();
    fRayWorld3D.normalize();


    // Step 5: 4D Object Coordinates between [-x:x, -y:y, -z:z, -w:w].
    QMatrix4x4 invModelMatrix = m_ModelMatrix.inverted( &bIsInverted );
    QVector4D fRayObject4D;
    if ( bIsInverted || !invModelMatrix.isIdentity() ) {
        fRayObject4D = (invModelMatrix*fRayWorld4D);
        qDebug() << " 4D Object Coords: " << fRayObject4D;
    } else {
        qDebug() << "Model matrix could not be inverted!";
        return QVector3D();
    }
    QVector3D fRayObject3D = fRayObject4D.toVector3D();
    fRayObject3D.normalize();

    qDebug();
    return fRayObject3D;
}


void CollisionDetector::intersectsBoundingSpheres( QVector3D& rfRayObject3D,
                                                   QList<CollisionData>& rDataList )
{

    GeometryEngine* pEngine = GeometryEngine::getEngine();

    // Determine center of object and radius.
    QVector4D camOriginEye4D = QVector4D(0, 0, 0, 1 );
    QVector3D camOriginObject3D;

    bool bIsVInverted = false;
    bool bIsMInverted = false;
    QMatrix4x4 invViewMatrix = m_ViewMatrix.inverted( &bIsVInverted );
    QMatrix4x4 invModelMatrix = m_ModelMatrix.inverted( &bIsMInverted );

    if ( bIsVInverted && bIsMInverted ) {
        QVector4D camOriginObject4D;
        camOriginObject4D = (invModelMatrix*invViewMatrix*camOriginEye4D);

        camOriginObject3D = camOriginObject4D.toVector3D();
        qDebug() << "Transform Center of Obj and Camera: ";
    } else {
        qDebug() << "ModelView matrix could not be inverted!";
        return;
    }

    QVector3D objCenterObject3D;
    float fRadius;
    float fIntercept = 0;
    for ( int iIndex = 0; iIndex < pEngine->size(); ++iIndex ) {
        // TODO: make a check to do ray casting for objects with specific name
        // If name isn't valid then insert false and -1.0 into list
        fIntercept = 0.0;
        QString qsObjectName = pEngine->getObjectName( iIndex );
        if ( isSelectableObject( qsObjectName ) ) {

            objCenterObject3D = pEngine->getObjectCenter( iIndex );
            qDebug() << "Object Center: " << objCenterObject3D;

            fRadius = pEngine->getObjectRadius( iIndex );
            qDebug() << "Object Radius: " << fRadius;

            QVector3D originSubCenter3D = camOriginObject3D - objCenterObject3D;

            // Check if ray intersects sphere.
            float fB = QVector3D::dotProduct( rfRayObject3D, originSubCenter3D );

            float fCOCDotProd = QVector3D::dotProduct(originSubCenter3D,
                                                      originSubCenter3D );
            float fC = fCOCDotProd - fRadius;

            qDebug() << "(fB*fB - fC ) " << (fB*fB - fC );
            if ( (fB*fB - fC ) >= 0 ) {
                fIntercept = calcLargestObjectIntercept( fB, fC);
                CollisionData data;
                data.qsObjectName = qsObjectName;
                data.fIntercept = fIntercept;
                rDataList << data;
            }
        }
    }
}



float CollisionDetector::calcLargestObjectIntercept( float fB, float fC ) {
    float fBSq2 = fB * fB;
    float fSqrt = float (qSqrt( ( fBSq2 - fC ) ));
    float fT1 = (-fB) + fSqrt;
    float fT2 = (-fB) - fSqrt;
    return qMax( fT1, fT2 );

}


QString CollisionDetector::findIntersectingObject( QList<CollisionData>& rDataList )
{
    float fMaxIntercept = FLT_MIN;
    QString qsObjectName = QString();

    for ( int iIndex = 0; iIndex < rDataList.size(); ++iIndex ) {
        CollisionData data = rDataList[iIndex];

        if ( data.fIntercept > fMaxIntercept  ) {
            fMaxIntercept = data.fIntercept;
            qsObjectName = data.qsObjectName;
        }

    }
    return qsObjectName;
}

