HEADERS += \
    $$PWD/collisiondetector.h \
    $$PWD/geometryengine.h \
    $$PWD/objprocessor.h

SOURCES += \
    $$PWD/collisiondetector.cpp \
    $$PWD/geometryengine.cpp \
    $$PWD/objprocessor.cpp
