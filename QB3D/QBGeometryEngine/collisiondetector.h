/**************************************************
 *  File: CollisionDetector.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QObject>
#include <QMatrix4x4>
#include <QPoint>

struct CollisionData;

class CollisionDetector : public QObject {
    Q_OBJECT
public:
    explicit CollisionDetector( const QMatrix4x4& rModelMat,
                                const QMatrix4x4& rViewMat,
                                const QMatrix4x4& rProjMat,
                                QObject *parent = 0 );

    ~CollisionDetector();

    static bool     isSelectableObject( const QString& rqsObjectName );
    void            setModelMatrix( const QMatrix4x4& rModelMat ) {
        m_ModelMatrix = rModelMat;
    }

    void            setViewMatrix( const QMatrix4x4& rViewMat ) {
        m_ViewMatrix = rViewMat;
    }

    void            setProjMatrix( const QMatrix4x4& rProjMat ) {
        m_ProjMatrix = rProjMat;
    }

    void            setModelViewMatrix( const QMatrix4x4& rModelMat,
                                        const QMatrix4x4& rViewMat ) {
        setModelMatrix( rModelMat );
        setViewMatrix( rViewMat );
    }

    bool          detectObjectCollision( const QPoint& rRayPoint,
                                         const QSize& rViewportSize );


    // Returns a null string if no object has been collided with the ray.
    QString          selectedObjectName() {
        return m_qsCollisionObject;
    }


private:
    QMatrix4x4      m_ModelMatrix;
    QMatrix4x4      m_ViewMatrix;
    QMatrix4x4      m_ProjMatrix;
    QString         m_qsCollisionObject;


    QVector3D        calculateRayCast( const QPoint& rRayPoint,
                                       const QSize& rViewportSize );

    void             intersectsBoundingSpheres( QVector3D& rfRayWorld,
                                                QList<CollisionData>& rDataList );
    float            calcLargestObjectIntercept( float fB, float fC );
    QString          findIntersectingObject( QList<CollisionData>& rDataList );


signals:

public slots:
};

