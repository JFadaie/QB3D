/***************************************
 *  File: objprocesser.cpp
 *  Author: Josh Fadaie
 *  Date: 02/08/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include <assert.h>

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <qmath.h>
#include <QVector2D>
#include <QtGlobal>
#include <QByteArray>

#include "objprocessor.h"

#define     NUM_VERT_DIM    3
#define     NUM_NORM_DIM    3
#define     NUM_TEXEL_DIM   2
#define     MAX_DIM         3



/***************************************************************
 * Function: Constructor
 *
 * Description: Constructor
 *
 *
 *
 *
 *
 ****************************************************************/

OBJProcessor::OBJProcessor(const QString& rqsFileName,
                           QObject* pParent,
                           ProcessType eType ) :
                                QObject( pParent )
{

    // Temporary storage containers.
    QVector<QVector3D> fPosVector;
    QVector<QVector3D> fNormalsVector;
    QVector<QVector2D> fTexelsVector;
    QVector<QVector3D> iFacesVector;

    QFile objFile( rqsFileName );

    if ( !objFile.open( QIODevice::ReadWrite | QIODevice::Text )) {
         qDebug() << "The File did not open successfully... returning.";
         return;
    }

    QTextStream inStream( &objFile );
    int iNewObject = -1;  // Looks for 'o' in file. Used to store data in different
                         // buffer for each seperate object. Starts at -1 so internal
                         // loop does not restart initially once 'o' is found. Once count is
                        // to 1, indicating a new object, the count is reset to zero. Acts as a bool,
                        // except for the first object.

    // Read each line of text until the end of the file.
    while ( !inStream.atEnd() ) {

        // Read each line of text until a new object or the end of the file.
        while( iNewObject < 1 && !inStream.atEnd() ) {
            QString qsLine = inStream.readLine();
           // qDebug() << qsLine;
            if ( !qsLine.isEmpty() && qsLine[0] == 'o' ) {
                iNewObject++;

                // Get object's name here.
                QString qsObjName = qsLine.mid(2).trimmed();
                m_CubeObjectNames  << qsObjName;
                continue;
            }
            if ( !qsLine.isNull() ) {
                getOBJLineData( qsLine,
                                fPosVector,
                                fNormalsVector,
                                fTexelsVector,
                                iFacesVector );
            }
        }

        switch( eType ) {
            case OBJProcessor::All:
                processAllOBJData( fPosVector,
                                   fNormalsVector,
                                   fTexelsVector,
                                   iFacesVector );
                break;
            case OBJProcessor::VertNorms:
                processVNOBJData(fPosVector,
                                 fNormalsVector,
                                 iFacesVector );
                break;
            default:
                processVNOBJData(fPosVector,
                                 fNormalsVector,
                                 iFacesVector );
                break;
        }

        // Clear face vector as data at indices have already been stored.
        iFacesVector.clear();

        // Reset flags and increment buffer index.
        iNewObject = 0;
     }

     objFile.close();
}



/***************************************************************
 * Function: Destructor
 *
 * Description: Destructor
 *
 *
 *
 *
 *
 */

OBJProcessor::~OBJProcessor()
{
    // Start iterating from the back.
    for (int iIndex = m_CubeObjectBuffer.size() - 1;
         iIndex >= 0; --iIndex ) {

        // TODO: determine if I use delete or delete[].
        ObjDataPtrs objPtrs = m_CubeObjectBuffer[iIndex];
        delete objPtrs.pVertexData;
        delete objPtrs.pTexelData;
        delete objPtrs.pNormalData;
    }
}



/***************************************************************
 * Function: OBJProcessor::getOBJLineData
 *
 * Description: Processes the line of data provided token by token,
 *              which are seperated by whitespace. If the data contained
 *              in each specifier is valid it is stored in the appropriate
 *              data buffer.
 *
 * Parameters:
 *      rqsLine: The QString reference that is processed. The line
 *               cannot be null, but
 ****************************************************************/

void OBJProcessor::getOBJLineData( QString& rqsLine,
                                   QVector<QVector3D>& rfPosVector,
                                   QVector<QVector3D>& rfNormalsVector,
                                   QVector<QVector2D>& rfTexelsVector,
                                   QVector<QVector3D>& riFacesVector ) {

    assert( !rqsLine.isNull() );

    QString qsType = rqsLine.mid( 0 , 2 );
    QString qsData = rqsLine.mid( 2 );
    qsData = qsData.trimmed();

    // Vertex, Texel, or Normal data.
    if(  qsType.compare("v ") == 0 ||
         qsType.compare("vt") == 0 ||
         qsType.compare("vn") == 0 ) {
        QStringList qsList = qsData.split( " " );
        QVector3D dataVec3D;
        dataVec3D.setX( qsList[0].toFloat() );
        dataVec3D.setY( qsList[1].toFloat() );
        if ( qsList.size() >= MAX_DIM ) {
            dataVec3D.setZ( qsList[2].toFloat() );
        }


        if( qsType.compare("v ") == 0 ) {
             rfPosVector << dataVec3D;
            // qDebug() << "V: " << dataVec3D;
        } else if( qsType.compare("vt") == 0 ) {
             QVector2D dataVec2D = dataVec3D.toVector2D();
            // qDebug() << "Vt: " << dataVec2D;
             rfTexelsVector << dataVec2D;
        } else if( qsType.compare("vn") == 0 ) {
            // qDebug() << "Vn: " << dataVec3D;
             rfNormalsVector << dataVec3D;
        }

    } else if( qsType.compare("f ") == 0 ) {

        // Split data into list of v/vt/vn first
        QStringList qsList = qsData.split( " " );
        QStringListIterator iGroup( qsList );

        // Split the line of face data into 3 seperate int values.
        while( iGroup.hasNext()) {
            QString qsFaceData = iGroup.next();
            //qDebug() << qsFaceData;
            QStringList qsList = qsFaceData.split( "/" );
            QVector3D faceDataVec3D;
            faceDataVec3D.setX( qsList[0].toFloat());
            faceDataVec3D.setY( qsList[1].toFloat());
            faceDataVec3D.setZ( qsList[2].toFloat());

           // qDebug() << "F: " << faceDataVec3D;
            // store 3D vector containing v/vt/n data.
            riFacesVector << faceDataVec3D;
        }
    }


}



/***************************************************************
 * Function: OBJProcessor::processAllOBJData
 *
 * Description:
 *
 * Parameters:
 *
 *
 ****************************************************************/

void OBJProcessor::processAllOBJData( QVector<QVector3D> &rfPosVector,
                                      QVector<QVector3D> &rfNormalsVector,
                                      QVector<QVector2D> &rfTexelsVector,
                                      QVector<QVector3D> &riFacesVector)
{
    // Containers for the new object's vertex, texel, and normal data.
    QVector<QVector3D>* pObjPosData = new QVector<QVector3D>();
    QVector<QVector2D>* pObjTexelData = new QVector<QVector2D>();
    QVector<QVector3D>* pObjNormalData = new QVector<QVector3D>();

    // Set object data pointers.
    ObjDataPtrs objData;
    objData.pVertexData = pObjPosData;
    objData.pTexelData = pObjTexelData;
    objData.pNormalData = pObjNormalData;

    // Store new pointers at appropriate index of object data buffer.
    m_CubeObjectBuffer << objData;

    qDebug() << "\n\n" << "BEGIN PROCESSING";
    for ( int iIndex = 0; iIndex < riFacesVector.size(); ++iIndex ) {

        // Get the first three values v, vt, vn.
        // Decrement value by 1, and multiply by offset. Remember each,
        // indice represents a group of vertex, normal, and texel dimensions.
        // Vertex: XYZ, Normal: IJK, Texels: UV.
        QVector3D faceData3D = riFacesVector[iIndex];

        int iVIndex0 = faceData3D.x() - 1;   // 'v'
        int iVtIndex0 = faceData3D.y() - 1;  // 'vt'
        int iNIndex0 = faceData3D.z() - 1;   // 'vn'

        // Get 3D Vector values at index. Create copy and
        // store data in object's data pointer.
        QVector3D posData3D = rfPosVector[iVIndex0];
        (*pObjPosData ) << posData3D;

        QVector3D normalData3D = rfNormalsVector[iNIndex0];
        (*pObjNormalData ) << normalData3D;

        QVector2D texelData3D = rfTexelsVector[iVtIndex0];
        (*pObjTexelData) << texelData3D;
    }

    qDebug() << "END PROCESSING" << "\n\n";
}


/***************************************************************
 * Function: OBJProcessor::processVNOBJData
 *
 * Description:
 *
 * Parameters:
 *
 *
 ****************************************************************/

void OBJProcessor::processVNOBJData( QVector<QVector3D> &rfPosVector,
                                     QVector<QVector3D> &rfNormalsVector,
                                     QVector<QVector3D> &riFacesVector)
{
    // Containers for the new object's vertex, texel, and normal data.
    QVector<QVector3D>* pObjPosData = new QVector<QVector3D>();
    QVector<QVector2D>* pObjTexelData = 0;
    QVector<QVector3D>* pObjNormalData = new QVector<QVector3D>();

    // Set object data pointers.
    ObjDataPtrs objData;
    objData.pVertexData = pObjPosData;
    objData.pTexelData = pObjTexelData;
    objData.pNormalData = pObjNormalData;

    // Store new pointers at appropriate index of object data buffer.
    m_CubeObjectBuffer << objData;

    qDebug() << "\n\n" << "BEGIN PROCESSING";
    for ( int iIndex = 0; iIndex < riFacesVector.size(); ++iIndex ) {

        // Get the v and vn values.
        // Decrement value by 1, and multiply by offset. Remember each,
        // indice represents a group of vertex, normal, and texel dimensions.
        // Vertex: XYZ, Normal: IJK, Texels: UV.
        QVector3D faceData3D = riFacesVector[iIndex];

        int iVIndex0 = faceData3D.x() - 1;   // 'v'
        int iNIndex0 = faceData3D.z() - 1;   // 'vn'

        // Get 3D Vector values at index. Create copy and
        // store data in object's data pointer.
        QVector3D posData3D = rfPosVector[iVIndex0];
      //  qDebug() << "V Index: " << iVIndex0;
      //  qDebug() << "V Value: " << posData3D;
        (*pObjPosData ) << posData3D;

        QVector3D normalData3D = rfNormalsVector[iNIndex0];
      //  qDebug() << "N Index: " << iNIndex0;
      //  qDebug() << "N Value: " << normalData3D;
        (*pObjNormalData ) << normalData3D;
    }

    qDebug() << "END PROCESSING" << "\n\n";
}

/***************************************************************
 * Function: OBJProcessor::getVerticesData
 *           OBJProcessor::getNormalsData
 *           OBJProcessor::getTexelsData
 *
 * Description: Retrieve the data pointers corresponding to the
 *              object at the provided index. Assertes that the
 *              index provided is within the bounds of the vector.
 *
 * Parameters:
 *      iObjIndex: index of the object.
 *
 *
 ****************************************************************/

QVector<QVector3D>* OBJProcessor::getVerticesData(int iObjIndex )
{ 
   Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_CubeObjectBuffer.size() );

   ObjDataPtrs objData = m_CubeObjectBuffer[iObjIndex];
   return objData.pVertexData;
}

QVector<QVector3D>* OBJProcessor::getNormalsData( int iObjIndex )
{
   Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_CubeObjectBuffer.size() );

   ObjDataPtrs objData = m_CubeObjectBuffer[iObjIndex];
   return objData.pNormalData;
}

QVector<QVector2D>* OBJProcessor::getTexelsData( int iObjIndex )
{
   Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_CubeObjectBuffer.size() );

   ObjDataPtrs objData = m_CubeObjectBuffer[iObjIndex];
   return objData.pTexelData;
}



/***************************************************************
 * Function: OBJProcessor::getObjCenterAndRadius
 *
 * Description: Retrieve the objects mid point in the x, y, and
 *              z direction. If the data at the provided index
 *              is null or empty return a null 3D vector.
 *
 * Parameters:
 *      iObjIndex: index of the object
 *
 *
 ****************************************************************/

void OBJProcessor::getObjCenterAndRadius( int iObjIndex,
                                          QVector3D& rCenter3D,
                                          float& rfRadius )
{
    QVector<QVector3D>* pPosData3D = getVerticesData( iObjIndex );


    if ( pPosData3D == 0 || pPosData3D->isEmpty() ) {
        qDebug() << "Vector is emtpy at index: " << iObjIndex;
        return;
    }

    // Get first vertex in list. Prime the min and max.
    QVector3D vertex3D0 = pPosData3D->at( 0 );
    float fMinX = vertex3D0.x();
    float fMinY = vertex3D0.y();
    float fMinZ = vertex3D0.z();

    float fMaxX = fMinX;
    float fMaxY = fMinY;
    float fMaxZ = fMinZ;

    for ( int iIndex = 1; iIndex < pPosData3D->size(); ++iIndex ) {
        QVector3D vertex3D = pPosData3D->at( iIndex );
        fMinX = qMin( vertex3D.x(), fMinX );
        fMinY = qMin( vertex3D.y(), fMinY );
        fMinZ = qMin( vertex3D.z(), fMinZ );

        fMaxX = qMax( vertex3D.x(), fMaxX );
        fMaxY = qMax( vertex3D.y(), fMaxY );
        fMaxZ = qMax( vertex3D.z(), fMaxZ );
    }

    float fMidX = (fMaxX + fMinX)/2;
    float fMidY = (fMaxY + fMinY)/2;
    float fMidZ = (fMaxZ + fMinZ)/2;

    rCenter3D = QVector3D(fMidX, fMidY, fMidZ );

    float fRadX = fMaxX - fMidX;
    float fRadY = fMaxY - fMidY;
    float fRadZ = fMaxZ - fMidZ;

    // Store the maximum radius. Assumes a fairly spherical shape.
    rfRadius = qMax( fRadX, qMax( fRadY, fRadZ));
}




#if 0

void OBJProcessor::processPosOBJData( QVector<float>& rfPosVector,
                        QVector<int>& riFacesVector )
{

}

void OBJProcessor::processNormalsOBJData( QVector<float>& rfNormalsVector,
                            QVector<int>& riFacesVector )
{

}

void OBJProcessor::processTexelsOBJData( QVector<float>& rfTexelsVector,
                           QVector<int>& riFacesVector)
{

}

#endif

