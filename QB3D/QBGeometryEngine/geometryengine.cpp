/***************************************
 *  File: GeometryEngine.cpp
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include "geometryengine.h"
#include "collisiondetector.h"
#include "macros.h"

#define         NUM_TRIANGLE_VERTS          3


/***************************************************************
 * Function: Constructor
 *
 * Description: Constructor
 *
 *
 *
 *
 * Note: Shader program should already be linked and bound to
 * its respective shader files.
 ****************************************************************/

GeometryEngine::GeometryEngine(QObject* pParent) :
    QObject( pParent )
{
    // These are the default values provided for the vertex, normal,
    // and texel shader attributes. It is expected that these names are
    // used inside the shader program associated with the project that
    // instanciates a GeometryEngine object if no other names are set.
    m_qsPosAttrName = QString( POS_ATTR_NAME );
    m_qsNormalAttrName = QString( NORM_ATTR_NAME );
    m_qsTexelAttrName = QString( TEXEL_ATTR_NAME );

    m_glBufferUsagePattern = QOpenGLBuffer::StreamDraw;
}


void GeometryEngine::initEngine( OBJProcessor &rProcessor,
                                 QOpenGLShaderProgram* pProgram )
{
    for ( int iIndex = 0; iIndex < rProcessor.size(); ++iIndex ) {
        VAOInfo info;
        getProcessorVAOInfo( iIndex, rProcessor, info );
        createVAO( info, pProgram );
    }

    calcCubeDimensions();

    qDebug() << pProgram->log();
}

GeometryEngine::~GeometryEngine()
{
    // Delete each of the ObjectData structs.
    for ( int iIndex = m_ObjectList.size() - 1; iIndex >= 0; --iIndex ) {
        ObjectData* pObjData = m_ObjectList[iIndex];
        // TODO: determine if QOpenGLBuffers are passed around with
        // pointers or can be used without.


        if ( pObjData->pVertexBuffer.isCreated() ) {
            qDebug() << "Vertex Buffer is destroyed!";
            pObjData->pVertexBuffer.destroy();
        }

        if ( pObjData->pNormalBuffer.isCreated() ) {
            pObjData->pNormalBuffer.destroy();
        }

        if ( pObjData->pTexelBuffer.isCreated() ) {
            pObjData->pTexelBuffer.destroy();
        }
        pObjData->pVAO->destroy();

        delete pObjData;
    }
}


void GeometryEngine::getProcessorVAOInfo( int iObjIndex,
                                          OBJProcessor& rProcessor,
                                          VAOInfo& info )
{
    info.pfVertexList = rProcessor.getVerticesData( iObjIndex );
    //printVec3DList( info.pfVertexList, "Info Vertex List" );
    info.pfNormalList = rProcessor.getNormalsData( iObjIndex );
    info.pfTexelList = rProcessor.getTexelsData( iObjIndex );
    info.qsObjName = rProcessor.getObjName( iObjIndex );

    rProcessor.getObjCenterAndRadius( iObjIndex,
                                      info.center3D,
                                      info.fRadiusFromCenter );
}



void GeometryEngine::createVAO( VAOInfo& rInfo,
                                QOpenGLShaderProgram* pProgram )
{

    Q_ASSERT( pProgram != 0 );

    if ( rInfo.pfVertexList == 0 ) {
        qDebug() << "Warning!: No vertex data... returning.";
        return;
    }

    // Stores VAO, VBO(s), Object Name, center, etc. for each object.
    ObjectData* pObjData = new ObjectData;

    // Create VAO which stores VBO information for convenient access
    // and rendering of data.
    QOpenGLVertexArrayObject* pVAO = new QOpenGLVertexArrayObject( this );
    pVAO->create();
    pVAO->bind();

    // Get the position of the Buffer Object's associated shader program
    // attribute.
    m_gluiPosAttribute = pProgram->attributeLocation( m_qsPosAttrName );

    // Create, bind and allocate data for the vertex buffer.
    QOpenGLBuffer vertexBuffer = QOpenGLBuffer( QOpenGLBuffer::VertexBuffer );
    vertexBuffer.create();
    vertexBuffer.setUsagePattern( m_glBufferUsagePattern );
    vertexBuffer.bind();
   // printVec3DList( rInfo.pfVertexList, "Vertex List - In VAO Creation" );
    vertexBuffer.allocate( (*rInfo.pfVertexList).data(),
                           rInfo.pfVertexList->size()*sizeof(QVector3D ));

    // Associate the position attribute with the currently bound buffer object.
    // Enable the attribute array pointer to this buffer object in the VAO.
    pProgram->setAttributeBuffer( m_gluiPosAttribute, GL_FLOAT, 0, 3, sizeof (QVector3D));
    pProgram->enableAttributeArray( m_gluiPosAttribute );

    // Get the size of the objects data.
    pObjData->iNumTriangles = rInfo.pfVertexList->size();
    qDebug() << "Vertex List Size " << rInfo.pfVertexList->size();
    qDebug() << "Vertex Buffer Size" << vertexBuffer.size();
    // Store reference to object's vertex buffer.
    pObjData->pVertexBuffer = vertexBuffer;

    if ( rInfo.pfNormalList != 0 ) {

        m_gluiNormalAttribute = pProgram->attributeLocation( m_qsNormalAttrName );

        QOpenGLBuffer normalBuffer = QOpenGLBuffer( QOpenGLBuffer::VertexBuffer );
        normalBuffer.create();
        normalBuffer.setUsagePattern( m_glBufferUsagePattern );
        normalBuffer.bind();
        normalBuffer.allocate( (*rInfo.pfNormalList).data(),
                               rInfo.pfNormalList->size()*sizeof(QVector3D ));

        pProgram->setAttributeBuffer( m_gluiNormalAttribute, GL_FLOAT, 0, 3, sizeof(QVector3D));
        pProgram->enableAttributeArray( m_gluiNormalAttribute );

        pObjData->pNormalBuffer = normalBuffer;

    }

    if ( rInfo.pfTexelList != 0 ) {
        m_gluiTexelAttribute = pProgram->attributeLocation( m_qsTexelAttrName );

        QOpenGLBuffer texelBuffer = QOpenGLBuffer( QOpenGLBuffer::VertexBuffer );
        texelBuffer.create();
        texelBuffer.setUsagePattern( m_glBufferUsagePattern );
        texelBuffer.bind();
        texelBuffer.allocate( rInfo.pfTexelList->data(),
                              rInfo.pfTexelList->size()*sizeof(QVector2D ));

        pProgram->setAttributeBuffer( m_gluiTexelAttribute, GL_FLOAT, 0, 3, sizeof(QVector2D));
        pProgram->enableAttributeArray( m_gluiTexelAttribute );

        pObjData->pTexelBuffer = texelBuffer;

    }

    // Store other object info: name, center, and radius.
    pObjData->center3D = rInfo.center3D;
    pObjData->fRadiusFromCenter = rInfo.fRadiusFromCenter;
    pObjData->qsObjName = rInfo.qsObjName;

    // Set VAO pointer.
    pObjData->pVAO = pVAO;
    // Add object data pointer to list containing each object's data.
    m_ObjectList << pObjData;

    // Unbind this VAO from the context.
    pVAO->release();

}



void GeometryEngine::drawObject( int iObjIndex )
{
    ObjectData* pObjData = m_ObjectList[iObjIndex];
    pObjData->pVAO->bind();

    glDrawArrays( GL_TRIANGLES, 0, pObjData->iNumTriangles );

}


ObjectData& GeometryEngine::getObjectData( int iObjIndex )
{
    Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_ObjectList.size() );
    return (*m_ObjectList[iObjIndex]);
}

QString GeometryEngine::getObjectName( int iObjIndex )
{
    Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_ObjectList.size() );
    ObjectData* pObjData = m_ObjectList[iObjIndex];
    return pObjData->qsObjName;
}


QVector3D GeometryEngine::getObjectCenter( int iObjIndex )
{
    Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_ObjectList.size() );
    ObjectData* pObjData = m_ObjectList[iObjIndex];
    return pObjData->center3D;
}

float GeometryEngine::getObjectRadius( int iObjIndex )
{
    Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_ObjectList.size() );
    ObjectData* pObjData = m_ObjectList[iObjIndex];
    return pObjData->fRadiusFromCenter;
}


/***************************************************************
 * Function: GeometryEngine::getCubeDimensions
 *
 * Description: Retrieves the dimensions of the cube based on the coordinates
 *              provided in each LED object's name.
 *
 */

void GeometryEngine::getCubeDimensions( int& riXDim,
                                        int& riYDim,
                                        int& riZDim )
{
    riXDim = m_iXDim;
    riYDim = m_iYDim;
    riZDim = m_iZDim;
}


/***************************************************************
 * Function: GeometryEngine::calcCubeDimensions
 *
 * Description: Calculates the dimensions of the cube based on the coordinates
 *              provided in each LED object's name.
 *
 */

void GeometryEngine::calcCubeDimensions()
{
    int iXMax = 0;
    int iYMax = 0;
    int iZMax = 0;


    // Initialize selected LED object map. Initially nothing is selected.
    for (int iIndex = 0; iIndex < size(); ++iIndex ) {
        QString qsObjectName = getObjectName( iIndex );
        if ( CollisionDetector::isSelectableObject( qsObjectName ) ) {
            QRegExp ledRegExp = GetLEDCoordRegex();
            if ( ledRegExp.indexIn( qsObjectName) != -1 ) {
                QString qsCoordinates = ledRegExp.cap(1);
                //qDebug() << "Name Coords: " << qsCoordinates;
                iXMax = qMax( iXMax, qsCoordinates[0].digitValue() );
                iYMax = qMax( iYMax, qsCoordinates[1].digitValue() );
                iZMax = qMax( iZMax, qsCoordinates[2].digitValue() );
             }
        }
    }

    // Increment the size by 1 because values are zero based.
    m_iXDim = iXMax + 1;
    m_iYDim = iYMax + 1;
    m_iZDim = iZMax + 1;
}


void GeometryEngine::printVec3DList( QVector<QVector3D> *pVec3DList,
                                     const QString& rqsDebugTitle )
{
    qDebug() << "\nBegin Debugging: " << rqsDebugTitle;
    for ( int iIndex = 0; iIndex < pVec3DList->size(); ++iIndex ) {
        qDebug() << pVec3DList->at( iIndex );
    }
    qDebug() << "End Debugging: " << rqsDebugTitle << "\n";
}
