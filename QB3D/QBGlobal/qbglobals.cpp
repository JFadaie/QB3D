/**************************************************
 *  File: qbglobals.cpp
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include <QVector3D>

#include "qbglobals.h"
#include "geometryengine.h"
#include "snapshot.h"


bool QB::ValidHandlerType( const QString& rqsType )
{
    QStringList typeList;
    GetHandlerTypes( typeList );
    bool bValid = typeList.contains( rqsType );
    return bValid;
}


void QB::GetHandlerTypes( QStringList& rList )
{
    rList
        << SELECTION_HANDLER
        << ANIMATION_HANDLER;
}
\

float QB::Plane(
        LEDCoords* const pC1,
        LEDCoords* const pC2,
        LEDCoords* const pC3,
        QVector3D&       rNormal3D )
{
    const QVector3D& rV12 =
                QVector3D( pC2->iX, pC2->iY, pC2->iZ ) -
                QVector3D( pC1->iX, pC1->iY, pC1->iZ );
    const QVector3D& rV13 =
                QVector3D( pC3->iX, pC3->iY, pC3->iZ ) -
                QVector3D( pC1->iX, pC1->iY, pC1->iZ );

    const QVector3D& rN = QVector3D::crossProduct( rV12, rV13 );

    // Compute the equality value for the plane "D".
    float fD =
                -1*rN.x()*pC1->iX +
                -1*rN.y()*pC1->iY +
                -1*rN.z()*pC1->iZ;
    fD = -1 * fD;

    // Return and set references equal.
    rNormal3D = rN;
    return fD;
}

void QB::CubeDimensions(
        int& riXMax,
        int& riYMax,
        int& riZMax )
{
    GeometryEngine* const pEngine = GeometryEngine::getEngine();
    pEngine->getCubeDimensions( riXMax, riYMax, riZMax );
    qDebug() << "Max Dimensions XYZ: " << riXMax << riYMax << riZMax;
}


