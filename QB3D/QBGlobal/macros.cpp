#include <QList>

#include "macros.h"


int GetObjectTypeByName( const QString& rqsObjName )
{
    QList<QString> objTypeQualifierList;

    // List in supplied in particular ordered associated with int type
    // specified in Fragment Shader. LED = 0, Base = 1, Wireframe = 2.
    objTypeQualifierList << QString( LED_TYPE_QUALIFIER )
                         << QString( BASE_TYPE_QUALIFIER )
                         << QString( WIREFRAME_TYPE_QUALIFIER);
    for ( int iIndex = 0;
          iIndex < objTypeQualifierList.size(); ++iIndex ) {
        QString qsObjQualifier = objTypeQualifierList[iIndex];
        if ( rqsObjName.contains( qsObjQualifier)) {
            return iIndex;
        }
    }
    return -1; // Will use default color.
}


QRegExp GetLEDCoordRegex()
{
    QRegExp coordRegExp( "(\\d{" + QString::number(MAX_REGEXP_DIM) + ","
                                 + QString::number(MAX_REGEXP_DIM) + "})" );

    return coordRegExp;
}
