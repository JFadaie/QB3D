
#INCLUDEPATH += $$PWD
#DEPENDPATH  += $$PWD

#HEADERS += \
#    $$PWD/qbglobals.h

#SOURCES += \
#    $$PWD/qbglobals.cpp

HEADERS += \
    $$PWD/qbglobals.h \
    $$PWD/communicationpacket.h \
    $$PWD/databuffer.h \
    $$PWD/ledarray.h \
    $$PWD/macros.h \
    $$PWD/visualtitles.h

SOURCES += \
    $$PWD/qbglobals.cpp \
    $$PWD/communicationpacket.cpp \
    $$PWD/ledarray.cpp \
    $$PWD/macros.cpp
