/**************************************************
 *  File: databuffer.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once


#include <QString>
#include "snapshot.h"

// TODO: might consider putting this inside qbglobals file

class SnapShot;

// Base Struct for passing data between SelectHandler and
// corresponding PanelGroup.
struct DATA_BUFFER {
    QString qsEffectState;
    SnapShot* pSnapshot;
    LEDCoords  ledCoord;
};


