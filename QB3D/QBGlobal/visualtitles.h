/**************************************************
 *  File: visualtitles.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QString>

// Title (key) for Effects Selecting Planes.
#define     PLANE_SELECTOR          "Plane Selection Effect"
#define     VPLANE_EFFECT           "Vertical Plane Effect"
#define     HPLANE_EFFECT           "Horizontal Plane Effect"
#define     DIAGPLANE_EFFECT        "Diagonal Plane Effect"

// Title (key) for Effects Selecting Lines.
#define     LINE_SELECTOR          "Line Selection Effect"
#define     VLINE_EFFECT           "Vertical Line Effect"
#define     HLINE_EFFECT           "Horizontal Line Effect"
#define     DIAGLINE_EFFECT        "Diagonal Line Effect"


// ------- BEGIN ANIMATION KEYS ------ //

// Used for unnamed, user defined animations.
#define     CUSTOM_ANIMATION       "Custom Animation"
