/**************************************************
 *  File: macros.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QString>
#include <QRegExp>

// Based on the naming convention currently for the LED objects defined
// in Blender. Names to LED objects are given by "LEDXYZ". Example:
// LED011 = X:0, Y:1, Z: 1. So X,Y, or Z can be 0 through 9.
// Change naming conevention if dimensions
// need to go up.
#define         MAX_REGEXP_DIM            3

#define         LED_TYPE_QUALIFIER        "LED"
#define         BASE_TYPE_QUALIFIER       "Base"
#define         WIREFRAME_TYPE_QUALIFIER  "WireFrame"

// These values are used in the shader program as attribute names.
#define         POS_ATTR_NAME               "inPosition3D"
#define         NORM_ATTR_NAME              "inNormal3D"
#define         TEXEL_ATTR_NAME             "inTexel3D"




int  GetObjectTypeByName( const QString& rqsObjName );


QRegExp GetLEDCoordRegex();
