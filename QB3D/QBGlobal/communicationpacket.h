#pragma once

#include <QtGlobal>
#include "Snapshot.h"



// This struct is used for communication between the
// GLCubeWidget and SelectionAssistant Object.
struct COMM_PACKET {
    enum UserCommand {
        NO_COMMAND,
        SELECT_LED,
        ENTER,
        NEXT_OPTION,
        PREVIOUS_OPTION,
        ESCAPE
    };

    enum Operation {
        ADDITION,
        SUBTRACTION,
        EQUAL
    };

    COMM_PACKET() {
        eCommand = NO_COMMAND;
        bSelectionMode = false;
        pSnapshot = nullptr;
        ledCoords.iX = 0;
        ledCoords.iY = 0;
        ledCoords.iZ = 0;
        eOperation = ADDITION;

    }

    UserCommand     eCommand;
    SnapShot*       pSnapshot;
    LEDCoords       ledCoords;
    bool            bSelectionMode;
    Operation       eOperation;

};


//extern COMM_PACKET gPacket;
