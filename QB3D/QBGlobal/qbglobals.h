/**************************************************
 *  File: qbglobals.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QString>
#include <QStringList>
#include <QVector3D>

// Base object types.
#define     SELECTION_HANDLER       "Selector"
#define     ANIMATION_HANDLER       "Animation"

#define     DEFAULT_NUM_SNAPSHOTS   25

#define     MAX_FPS                 20

struct LEDCoords;

namespace QB {

enum HandlerTypes {
    INVALID = 0,
    SELECTION,
    CONTROL_PANEL,
    ANIMATION
};

enum UserCommand {
    NO_COMMAND,
    SELECT_LED,
    ENTER,
    NEXT_OPTION,
    PREVIOUS_OPTION,
    ESCAPE
};

enum AnimationSpeed {
    SLOW    = 2,
    MEDIUM  = 5,
    FAST    = 10,
    FPS     = MAX_FPS   // The maximum frame rate.
};

enum SnapShotOp {
    ADD,
    SUBTRACT
};


bool ValidHandlerType( const QString& rqsType );

void GetHandlerTypes( QStringList& rList );

float Plane(
             LEDCoords* const pC1,
             LEDCoords* const pC2,
             LEDCoords* const pC3,
             QVector3D&       rNormal3D );

void  CubeDimensions(
                    int& riXMax,
                    int& riYMax,
                    int& riZMax );

//void FrameIndex( int iKeyFrameIndex, int iInitFrameIndex );

}




