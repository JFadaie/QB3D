#version 330

// Inputs.
in vec3 inPosition3D;
in vec3 inNormal3D;

// Uniform Matrices
uniform mat4 uMMat;
uniform mat4 uVMat;
uniform mat4 uPMat;

out vec3 ioPos3D_Eye;
out vec3 ioNorm3D_Eye;

void main( void )
{
    ioPos3D_Eye = vec3( uVMat*uMMat*vec4( inPosition3D, 1.0 ));
    ioNorm3D_Eye = vec3( uVMat*uMMat*vec4( inNormal3D, 0.0 ));

    gl_Position = uPMat*uVMat*uMMat*vec4( inPosition3D, 1.0 );

}
