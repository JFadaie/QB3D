/***************************************
 *  File: mainwindow.cpp
 *  Author: Josh Fadaie
 *  Date: 02/08/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "geometryengine.h"
#include "visualslibrary.h"
#include "visualtitles.h"
#include "qbglobals.h"
#include "selectionassistant.h"
#include "glcubewidget.h"
#include "communicationpacket.h"


MainWindow::MainWindow(QWidget* pParent ) :
    QMainWindow( pParent ),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Force the layout to be created. Initializes Geometry Engine.
    // NOTE: I might need an init function if this is not a good solution.
    setVisible( false );
    show();
    hide();

    // NOTE: The geometry engine is initialized in the GLCubeWidget during
    // its instantiation.
    GeometryEngine* pEngine = GeometryEngine::getEngine();
    qDebug() << "Size of Geometry Engine: " << pEngine->size();
    m_pSelAssistant = SelectionAssistant::getAssistant();

    m_pControlPanel = new ControlPanel;
    ui->m_pCtrlPanelMngr->addPage( m_pControlPanel, "Control" );

   // m_pEffectsPanel = new EffectsPanel;
   // ui->m_pEffectsPanelMngr->addPage( m_pEffectsPanel, "Effects/Animations" );

    // Connect GLCubeWidget to SelectionAssistant.
//    connect( m_pSelAssistant,       &SelectionAssistant::updateSnapShot,
//             ui->m_pGLCubeWidget,   &GLCubeWidget::onNewSnapshot );
    connect( ui->m_pGLCubeWidget,   &GLCubeWidget::userSelection,
             m_pControlPanel,       &ControlPanel::onUserSelection );
    
    // SelectionAssistant will filter specific GLCubeWidget events.
    ui->m_pGLCubeWidget->installEventFilter( m_pSelAssistant );

    // Connect EffectPanelManager to SelectionAssistant.
    connect( ui->m_pEffectsPanelMngr, &EffectsPanelManager::panelChanged,
                     m_pSelAssistant, &SelectionAssistant::onPanelChanged );
    connect( ui->m_pEffectsPanelMngr, &EffectsPanelManager::panelStateChanged,
                     m_pSelAssistant, &SelectionAssistant::onPanelStateChanged );

    // Connect ControlPanel to SelectionAssistant.
    // TODO: Change connection to control panel manager.
    connect( m_pSelAssistant, &SelectionAssistant::animationPacket,
             m_pControlPanel, &ControlPanel::onAnimationPacket );
    connect( m_pSelAssistant, &SelectionAssistant::commitEffect,
             m_pControlPanel, &ControlPanel::onCommitEffect );
    connect( m_pSelAssistant, &SelectionAssistant::undoEffect,
             m_pControlPanel, &ControlPanel::onUndoEffect );
    connect( m_pControlPanel, &ControlPanel::userSelection,
             m_pSelAssistant, &SelectionAssistant::onUserSelection );

    // Connect GLCubeWidget to ControlPanel.
    connect( m_pControlPanel,       &ControlPanel::updateSnapShot,
             ui->m_pGLCubeWidget,   &GLCubeWidget::onNewSnapshot );

   // connect( m_pControlPanel, &ControlPanel::currentSnapShotChanged,
   //          m_pSelAssistant, &SelectionAssistant::onNewSnapShot );
}

MainWindow::~MainWindow()
{
    delete ui;

    // Get the geometry engine and delete it.
    GeometryEngine* pEngine = GeometryEngine::getEngine();
    delete pEngine;

    // Get the visuals library and delete it.
    VisualsLibrary* pLib = VisualsLibrary::getLibrary();
    delete pLib;

    SelectionAssistant* pAssistant = SelectionAssistant::getAssistant();
    delete pAssistant;
}
