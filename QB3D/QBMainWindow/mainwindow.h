/***************************************
 *  File: wainwindow.h
 *  Author: Josh Fadaie
 *  Date: 02/08/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#pragma once
#include <QMainWindow>


#include "ctrlpnlmgr.h"
#include "controlpanel.h"
#include "effectspnlmgr.h"
#include "effectspanel.h"

class SelectionAssistant;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    // GLCubeWidget* ui->m_pGLCubeWidget;

    ControlPanel* m_pControlPanel;
    EffectsPanel* m_pEffectsPanel;
    SelectionAssistant*         m_pSelAssistant;


};

