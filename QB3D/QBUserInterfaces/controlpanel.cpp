#include "controlpanel.h"
#include "ui_controlpanel.h"

ControlPanel::ControlPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlPanel)
{
    ui->setupUi(this);

    m_pSnapShotManager = new SnapShotManager( this );

    ui->m_pSnapShotSlider->setMaximum( m_pSnapShotManager->size() );
    ui->m_pSnapShotSlider->setMinimum( 0 );

    connect( ui->m_pSnapShotSlider, &QSlider::sliderMoved,
             this, &ControlPanel::onSnapShotSliderMoved );
}

ControlPanel::~ControlPanel()
{
    delete ui;
}


void ControlPanel::onSnapShotSliderMoved( int iPosition )
{
    m_pSnapShotManager->setCurrentIndex( iPosition );
    SnapShot currSnapshot = m_pSnapShotManager->currentSnapShot();

    // Update GLCubeWidget with the current Snapshot.
    emit updateSnapShot( currSnapshot );
}


void ControlPanel::onAnimationPacket(
                    const AnimationPacket& rAniPacket,
                    QB::SnapShotOp         eOperation )
{
    Q_ASSERT( rAniPacket.keyFrameSize() > 0 );

    // suggest the current packet to the snapshotmanager
    // get the current snap shot from the manager
        // the function above will calculate the new snapshot
        // based on the overlapping animation packets.


}

void ControlPanel::onUndoEffect()
{


}

void ControlPanel::onCommitEffect()
{
 // if an animation is currently started
}


void ControlPanel::onUserSelection( const LEDCoords& rCoords )
{
    // TODO:
    // if ( m_pSnapShotManager->isActive() )
    //      provide a prompt box for the user telling them to commit
    //      or undo the current animation option ||
    //      undo the option automatically for the user.
    // }
    // else { // emit the signal
    // Notify the Selection Assistant of a user selection.
    emit userSelection( rCoords );

}
