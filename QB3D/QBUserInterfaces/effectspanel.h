/**************************************************
 *  File: effectspanel.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QWidget>

#define     MIN_NUM_EFFECTS     5

class PanelHandler;
class QListWidgetItem;

namespace Ui {
class EffectsPanel;
}

class EffectsPanel : public QWidget {
    Q_OBJECT

public:
    explicit EffectsPanel(
                        const QStringList& rKeyList,
                        bool               bEnabled = true,
                        QWidget*           pParent = 0);
    ~EffectsPanel();
    PanelHandler*   getPanelHandler( const int iRow ) const;
    PanelHandler*   getPanelHandler( QListWidgetItem* const pItem ) const;
    PanelHandler*   addPanelHandler( const QString& rqsKey );
    void            removePanelHandler( const QString& rqsKey );
    void            disablePanels();
    void            enablePanel( int iRow );


private:
    Ui::EffectsPanel *ui;
    QStringList     m_KeyList;
    void            initializeList( bool bEnabled );
    QListWidgetItem*   addPanel( const QString& rqsKey );
    PanelHandler*   m_pCurrentPanel;

private slots:
    void            onPanelClicked( QListWidgetItem* pPanel );
    void            onStateChanged( const QString& rqsState );
signals:
    void            panelChanged( PanelHandler* const pPanelHandler );
    void            stateChanged( PanelHandler* const pPanelHandler );
};

