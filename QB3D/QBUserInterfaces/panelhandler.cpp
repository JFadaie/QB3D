
#include <QVBoxLayout>
#include <QDebug>

#include "panelhandler.h"
#include "visualtitles.h"
#include "qbglobals.h"
#include "creationlibrary.h"
#include "databuffer.h"
#include "selectionhandler.h"

PanelHandler::PanelHandler(
            const PNLHANDLER_DATA*const pData,
            QWidget*                    pParent ) :
        QGroupBox( pParent )
{
    QVBoxLayout* const pMainVLayout = new QVBoxLayout;
    m_plbEffectTitle = new QLabel( pData->qsEffectTitle );
    m_pcxEffects = new QComboBox;
    m_pcxEffects->addItems( pData->stateList );

    pMainVLayout->addWidget( m_plbEffectTitle );
    pMainVLayout->addWidget( m_pcxEffects );

    setLayout( pMainVLayout );

    m_qsPanelKey = pData->qsHandlerKey;

    connect( m_pcxEffects, SIGNAL( currentTextChanged(const QString& )),
             this,         SIGNAL( stateChanged( const QString&) ) );
}




#if 0
QStringList effectsList;
effectsList
        << "Vertical Selection"
        << "Horizontal Selection"
        << "Diagonal Selection"
        << "2-Point Selection";
#endif
