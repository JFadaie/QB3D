/**************************************************
 *  File: effectspnlmgr.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QTabWidget>
#include <QWidget>

class EffectsPanel;
class PanelHandler;

class EffectsPanelManager : public QTabWidget {
    Q_OBJECT

public:
    EffectsPanelManager( QWidget* pParent = 0 );
    ~EffectsPanelManager();

    int         addPage(
                        EffectsPanel*   pPage,
                        const QString&  rqsTabTitle );

    int         insertPage(
                        int             iIndex,
                        EffectsPanel*   pPage,
                        const QString&  rqsTabTitle );


private slots:
    void        onPanelChanged( PanelHandler* const pPanelHandler );
    void        onStateChanged( PanelHandler* const pPanelHandler );
private:
    QString       m_qsCurrPanelKey;
    EffectsPanel* m_pCurrEPanel;
    bool        m_bExclusive;
    void        InitPanels();
    void        setExclusive( bool bExclusive = true );
signals:
    void        panelChanged(
                        const QString& rqsPanelKey,
                        const QString& rqsPanelState );
    void        panelStateChanged(
                        const QString& rqsPanelState );
 };


