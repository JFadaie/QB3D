/**************************************************
 *  File: controlpanel.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QWidget>
#include "snapshotmanager.h"


namespace Ui {
class ControlPanel;
}

class ControlPanel : public QWidget {
    Q_OBJECT

public:
    explicit ControlPanel(QWidget *parent = 0);
    ~ControlPanel();

private:
    Ui::ControlPanel *ui;
    SnapShotManager* m_pSnapShotManager;

public slots:
    void onCommitEffect();
    void onUndoEffect();
    void onUserSelection( const LEDCoords& rCoord );
    void onAnimationPacket(
                        const AnimationPacket& rAniPacket,
                        QB::SnapShotOp         eOperation = QB::ADD );
private slots:
    void onSnapShotSliderMoved( int iPosition );


signals:
    void currentSnapShotChanged( const SnapShot& rSnapShot );
    void updateSnapShot( const SnapShot& rSnapShot );
    void userSelection( const  LEDCoords& rCoord );
};

