#include "ctrlpnlmgr.h"
#include "ui_ctrlpnlmgr.h"

\
/***************************************************************
 * Function: Constructor/ Destructor
 *
 * Description: Constructs/ destructs a Control Panel Manager.
 *
 */

ControlPanelManager::ControlPanelManager(QWidget *parent) :
    QTabWidget(parent)
{
    setTabPosition( QTabWidget::South );
    setStyleSheet( " QTabWidget::tab-bar { alignment:left; }");
}

ControlPanelManager::~ControlPanelManager()
{
}


/***************************************************************
 * Function: ControlPanelManager::addPage
 *
 * Description: Appends a tabbed page to the list. Returns the index.
 *              added position added to the list.
 *
 * Parameters:
 *      pPage: pointer to the page associated with the tab.
 *      rqsTabTitle: title of the page's tab.
 */

int ControlPanelManager::addPage( QWidget* pPage, const QString& rqsTabTitle )
{
    int iIndex = addTab( pPage, rqsTabTitle );

    return iIndex;
}


/***************************************************************
 * Function: ControlPanelManager::insertPage
 *
 * Description: Inserts a tabbed page to the list. Returns the index.
 *              added position added to the list.
 *
 * Parameters:
 *      iIndex: desired index for the new page. If out of bounds, the
 *              page is appended to the list.
 *      pPage: pointer to the page associated with the tab.
 *      rqsTabTitle: title of the page's tab.
 */

int ControlPanelManager::insertPage( int iIndex,
                                     QWidget* pPage,
                                     const QString& rqsTabTitle )
{
    int iResultIndex = insertTab( iIndex, pPage, rqsTabTitle );

    return iResultIndex;

}
