/***************************************
 *  File: effectspnlmgr.h
 *  Author: Josh Fadaie
 *  Date: 03/11/2015
 *  Project: QB3D
 *
 *  Description:
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 *  TODO:
 *      - add a pop-up menu that lets panel handlers be
 *        removed, added, and changed.
 *
 ***************************************/


#include "effectspnlmgr.h"
#include "effectspanel.h"
#include "panelhandler.h"
#include "visualtitles.h"

/***************************************************************
 *
 * Function: Constructor/ Destructor
 *
 * Description: Constructs/ destructs an Effects Panel Manager.
 *
 */

EffectsPanelManager::EffectsPanelManager( QWidget* pParent ) :
            QTabWidget( pParent )
{
    setTabPosition( QTabWidget::North );
    setStyleSheet( " QTabWidget::tab-bar { "
                        "alignment:left;"
                        "top: 10px; }");

    setExclusive( true );
    m_pCurrEPanel = nullptr;

    InitPanels();
}

EffectsPanelManager::~EffectsPanelManager()
{

}


/***************************************************************
 *
 * Function: EffectsPanelManager::InitPanels
 *
 * Description: Initializes the panel manager with two EffectsPanels.
 *              the first effects panel displays Panel Handlers
 *              associated with selection handlers and the next
 *              associated with animation handlers.
 */

void EffectsPanelManager::InitPanels()
{
    QStringList selKeyList;
    selKeyList
            << PLANE_SELECTOR;

    QStringList aniKeyList;

    // Create the animation and selection effect panels.
    EffectsPanel* const pSelEffectPnl =
                new EffectsPanel( selKeyList );

    // Create the animation effects panel, but initially
    // disable its effects.
    EffectsPanel* const pAniEffectPnl =
                new EffectsPanel( aniKeyList, false );

    // Add the effect panels to the manager.
    addPage( pSelEffectPnl, "Selection Effects" );
    addPage( pAniEffectPnl, "Animation Effects" );
}


/***************************************************************
 *
 * Function: EffectsPanelManager::addPage
 *
 * Description: Appends a tabbed page to the list. Returns the index.
 *              added position added to the list.
 *
 * Parameters:
 *      pPage: pointer to the page associated with the tab.
 *      rqsTabTitle: title of the page's tab.
 */

int EffectsPanelManager::addPage( EffectsPanel* pPage, const QString& rqsTabTitle )
{
    connect( pPage, &EffectsPanel::panelChanged,
              this, &EffectsPanelManager::onPanelChanged );
    connect( pPage, &EffectsPanel::stateChanged,
              this, &EffectsPanelManager::onStateChanged );
    int iIndex = addTab( pPage, rqsTabTitle );

    return iIndex;
}


/***************************************************************
 *
 * Function: EffectsPanelManager::insertPage
 *
 * Description: Inserts a tabbed page to the list. Returns the index.
 *              added position added to the list.
 *
 * Parameters:
 *      iIndex: desired index for the new page. If out of bounds, the
 *              page is appended to the list.
 *      pPage: pointer to the page associated with the tab.
 *      rqsTabTitle: title of the page's tab.
 */

int EffectsPanelManager::insertPage( int iIndex,
                                     EffectsPanel* pPage,
                                     const QString& rqsTabTitle )
{
    connect( pPage, &EffectsPanel::panelChanged,
              this, &EffectsPanelManager::onPanelChanged );
    connect( pPage, &EffectsPanel::stateChanged,
              this, &EffectsPanelManager::onStateChanged );
    int iResultIndex = insertTab( iIndex, pPage, rqsTabTitle );

    return iResultIndex;

}

/***************************************************************
 *
 * Function: EffectsPanelManager::setExclusive
 *
 * Description: Determines whether the Effects Panels added to
 *              the manager can be controlled exclusively. Exclusive
 *              control allows only one PanelHandler to be enabled
 *              between any number of Effects Panels added to the
 *              manager. Otherwise, each Effects Panel can have a
 *              single Panel Handler enabled.
 *
 * Parameters:
 *          bExclusive: boolean enalbing or disabled exclusive
 *                      Effect Panels.
 */

void EffectsPanelManager::setExclusive( bool bExclusive)
{
    m_bExclusive = bExclusive;
}



/***************************************************************
 *
 * Function: EffectsPanelManager::onPanelChanged
 *
 * Description: Retrieves the key and effect state from the currently
 *              selected panel handler and re-emits the panelChanged
 *              signal to notify observers.
 *
 * Parameters:
 *          pPanelHandler: pointer to the currently selected panel handler.
 */

void EffectsPanelManager::onPanelChanged( PanelHandler* const pPanelHandler )
{
    // Ensure that only one Effects Panel's Panel Handler is enabled.
    // Disable the other PanelHandlers.
    if ( m_bExclusive ) {
        EffectsPanel* const pEPanelSender =
                qobject_cast< EffectsPanel* >( QObject::sender() );

        if ( m_pCurrEPanel != pEPanelSender ) {
            m_pCurrEPanel = pEPanelSender;
            for ( int i = 0; i < count(); ++i ) {
                QWidget* const pEPanelWidget = widget( i );
                EffectsPanel* const pEPanel = qobject_cast<EffectsPanel*>( pEPanelWidget );
                if ( pEPanel != m_pCurrEPanel )
                    pEPanel->disablePanels();
            }
         }
    }

    m_qsCurrPanelKey = pPanelHandler->handlerKey();
    const QString& rqsPanelState = pPanelHandler->handlerState();
    emit panelChanged( m_qsCurrPanelKey, rqsPanelState );
}


/***************************************************************
 *
 * Function: EffectsPanelManager::onStateChanged
 *
 * Description: Retrieves the key and effect state from the currently
 *              selected panel handler and re-emits the panelChanged
 *              signal to notify observers.
 *
 * Parameters:
 *          pPanelHandler: pointer to the currently selected panel handler.
 */

void EffectsPanelManager::onStateChanged( PanelHandler* const pPanelHandler )
{
    //m_qsCurrPanelKey = pPanelHandler->handlerKey();
    const QString& rqsPanelState = pPanelHandler->handlerState();
    emit panelStateChanged( rqsPanelState );
}
