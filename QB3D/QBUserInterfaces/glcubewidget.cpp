/***************************************
 *  File: GLCubeWidget.cpp
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include <QMouseEvent>
#include <QPoint>
#include <QKeyEvent>

#include "glcubewidget.h"
#include "geometryengine.h"
#include "collisiondetector.h"
#include "macros.h"
//#include "communicationpacket.h"

#define     OBJ_FILE    "/Users/joshfadaie/BlenderFiled/QB3D_V5_OBJ.obj"

GLCubeWidget::GLCubeWidget( QWidget* pParent ) :
    QOpenGLWidget( pParent )
{
    m_iXRot = 0;
    m_iYRot = 0;
    m_iZRot = 0;

    setFocusPolicy( Qt::ClickFocus );

    //m_pPacket = new COMM_PACKET;
}


void GLCubeWidget::initializeGL()
{
    initializeOpenGLFunctions();

    // Convenience function for specifying the clearing color to OpenGL.
    // Calls glClearColor (in RGBA mode) or glClearIndex (in color-index
    // mode) with the color c. Applies to this widgets GL context.
    glClearColor( 0.88, 0.88, 0.88, 1);

    // Enables depth comparisions and updating the depth buffer.
    glEnable(GL_DEPTH_TEST);

    // Culls polygons based on their winding in window coords
    glEnable(GL_CULL_FACE);

    // Smooth shading, the default, causes the computed colors of vertices
    // to be interpolated as the primitives are rasterized.
    glShadeModel(GL_SMOOTH);

    // Create shader program, add shaders, bind to the context, and link.
    m_pProgram = new QOpenGLShaderProgram( this );
    m_pProgram->addShaderFromSourceFile(QOpenGLShader::Vertex,  ":/qrc/vertexShader.vert" );
    m_pProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/qrc/fragmentShader.frag" );
    m_pProgram->bind();
    m_pProgram->link();

    // Process the blender file for object data.
    QString qsOBJFile( OBJ_FILE );
    OBJProcessor Processor( qsOBJFile );

    // Store the blender file data inside the geometry engine
    GeometryEngine* pEngine = GeometryEngine::getEngine();
    pEngine->initEngine( Processor, m_pProgram );

    // Initialize the current snap shot once the geometry engine is
    // constructed.
    m_pCurrSnapShot = new SnapShot( this );
    // TODO: Get this from the snapshot manager. Will have to be passed initially by
    // the constructor.
}


void GLCubeWidget::paintGL()
{
    // Clears indicated buffer's to preset values. Sets the bitplane area of the window to
    // values previously selected by glClearColor, glClearDepth, glClearStencil.
    // GL_COLOR_BUFFER_BIT: indicates the buffers currently enabled for color writing.
    // GL_DEPTH_BUFFER_BIT: indicates the depth buffer.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GeometryEngine* pEngine = GeometryEngine::getEngine();

    m_pProgram->bind();
    QVector3D CubeCenter3D = QVector3D( 0.0, -5.0, -10.0 );
    // Do Transformations.
    // Begin model matrix transformations.
    m_ModelMatrix = QMatrix4x4();
    m_ModelMatrix.translate( CubeCenter3D );
    m_ModelMatrix.rotate(m_iXRot / 18.0, 1.0, 0.0, 0.0);
    m_ModelMatrix.rotate(m_iYRot / 18.0, 0.0, 1.0, 0.0);
    m_ModelMatrix.rotate(m_iZRot / 18.0, 0.0, 0.0, 1.0);

    // Begin view matrix transformations.
    QVector3D eyePos3D = QVector3D( 0, 0, -50.0 );
    QVector3D upDir3D = QVector3D( 0, 1, 0 );
    m_ViewMatrix = QMatrix4x4();
    m_ViewMatrix.lookAt( eyePos3D, CubeCenter3D, upDir3D );

    // Set uniform values
    m_pProgram->setUniformValue( "uMMat", m_ModelMatrix );
    m_pProgram->setUniformValue( "uVMat", m_ViewMatrix );
    m_pProgram->setUniformValue( "uPMat", m_ProjectionMatrix );


    // Draw the geometry.
    // TODO: might need a color for subtraction.
    LEDState eState = LEDState::OFF;
    QString qsObjectName;
    for ( int iIndex = 0; iIndex < pEngine->size(); ++iIndex ) {
        eState = LEDState::OFF;

        qsObjectName = pEngine->getObjectName( iIndex );

        if ( CollisionDetector::isSelectableObject( qsObjectName ) ) {
            eState = m_pCurrSnapShot->getLEDState( qsObjectName );
        }

        int iObjType = GetObjectTypeByName( qsObjectName );
        m_pProgram->setUniformValue( "uiObjType", iObjType );
        m_pProgram->setUniformValue( "uiLEDState", eState );
        pEngine->drawObject( iIndex );
    }

}



// Sets up the OpenGL viewport, projection, etc. Gets called whenever
// the widget has been resized (and also when it is shown for the first
// time because all newly created widgets get a resize event automatically)
void GLCubeWidget::resizeGL(int w, int h)
{
    m_ViewportSize = QSize( w, h );

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 0.1, zFar = 100, fov = 65.0;

    // Reset projection
    m_ProjectionMatrix.setToIdentity();

    // Set perspective projection
    m_ProjectionMatrix.perspective(fov, aspect, zNear, zFar);

    update();
}



void GLCubeWidget::mousePressEvent(QMouseEvent* pEvent)
{
    m_qptLastPos = pEvent->pos();
}


void GLCubeWidget::mouseMoveEvent(QMouseEvent* pEvent)
{
    int dx = pEvent->x() - m_qptLastPos.x();
    int dy = pEvent->y() - m_qptLastPos.y();

    if (pEvent->buttons() & Qt::LeftButton) {
        setXRotation(m_iXRot + 18 * dy);
        setYRotation(m_iYRot + 18 * dx);
    } else if (pEvent->buttons() & Qt::RightButton) {
        setXRotation(m_iXRot + 18 * dy);
        setZRotation(m_iZRot + 18 * dx);
    }

    m_qptLastPos = pEvent->pos();
}


void GLCubeWidget::setXRotation(int iAngle)
{
    if (iAngle != m_iXRot) {
        m_iXRot = iAngle;
        update();
    }
}

void GLCubeWidget::setYRotation(int iAngle)
{
    if (iAngle != m_iYRot) {
        m_iYRot = iAngle;
        update();
    }
}

void GLCubeWidget::setZRotation(int iAngle)
{
    if (iAngle != m_iZRot) {
        m_iZRot = iAngle;
        update();
    }
}


void GLCubeWidget::mouseDoubleClickEvent(QMouseEvent* pEvent )
{

    // Step 0: 2D Viewport Coordinates between 0:width, 0:height.
    QPoint rayPoint = pEvent->pos();
    qDebug() << "ViewPort Dbl Click Coords: " << rayPoint;

    CollisionDetector cDetector( m_ModelMatrix,
                                 m_ViewMatrix,
                                 m_ProjectionMatrix );

    if ( cDetector.detectObjectCollision( rayPoint, m_ViewportSize ) ) {
        qDebug() << " An Object was selected!";
        qDebug() << "Selected Object Name " << cDetector.selectedObjectName();
        //m_pCurrSnapShot->toggleSelLED( cDetector.selectedObjectName() );
        LEDCoords ledCoords = m_pCurrSnapShot->getLEDCoords( cDetector.selectedObjectName() );
        //m_pPacket->ledCoords = ledCoords;
       // m_pPacket->eCommand = COMM_PACKET::SELECT_LED;
       // m_pPacket->pSnapshot = m_pCurrSnapShot;
        // TODO: Fix this code and modify when the glcubewidget paints.
        emit userSelection( ledCoords );
       // update();
    }
}


#if 0
void GLCubeWidget::keyPressEvent( QKeyEvent* pEvent )
{
    int eKey = pEvent->key();

    Qt::KeyboardModifiers eModifiers = pEvent->modifiers();

    switch( eKey ) {
        case Qt::Key_Escape: {
            qDebug() << "Escape Key Pressed";

            // Note: I wanted to initialize the packet variables inside
            // the case statements specifically, in case other events prempt or
            // overwrite code during other key events.
            emit userSelection( QB::ESCAPE );

        }
            break;
        case Qt::Key_Return:
        case Qt::Key_Enter: {
            qDebug() << "Return/Enter Key Pressed";
            emit userSelection( QB::ENTER );

        }
            break;
        case Qt::Key_A: {
            qDebug() << "A Key Pressed";
            emit userSelection( QB::PREVIOUS_OPTION );

        }
            break;
        case Qt::Key_D: {
            qDebug() << "D Key Pressed";
            emit userSelection( QB::NEXT_OPTION );
        }
            break;
        default:
            qDebug() << "Default Case in KeyPressEvent";
            break;
    }
}

// TODO: What about erasing a plane.
// If I set the cubes snapshot to be equal to the addition of the
// selected region and the original region and then use the selected region as
// a comparision for I still don't know if the LED was selected before hand or
// because of the selection handler. Both Snapshots LED(s) will be selected. But I still have the problem with the areas that are
// Use an operation. Subtracted or add the packets highlighted area.


void GLCubeWidget::receiveGLWidgetData( COMM_PACKET* const pPacket )
{
    COMM_PACKET::UserCommand eCommand = pPacket->eCommand;

    *m_pPacket = *pPacket;
    // TODO: Not the place for this. Should be in SelectionAssistant.
    // If there is only one option, then set the provided snapshot to
    // the current snapshot.
    if ( !m_pPacket->bSelectionMode ) {
        COMM_PACKET::Operation eOperation = pPacket->eOperation;
        switch( eOperation ) {
            case COMM_PACKET::ADDITION: {
                for ( int i = 0; i < m_pCurrSnapShot->size(); ++i ) {
                    // if provided led is on, turn turn the corresponding snap shot LED off.
                    QString qsLEDName = m_pCurrSnapShot->getLEDName( i );
                    bool bLEDState = pPacket->pSnapshot->getLEDState( i );
                    if ( bLEDState ) {
                        m_pCurrSnapShot->setLEDState( qsLEDName, bLEDState );
                    }
                 }
            }
                break;
            case COMM_PACKET::SUBTRACTION: {
                for ( int i = 0; i < m_pCurrSnapShot->size(); ++i ) {
                    // if provided led is on, turn turn the corresponding snap shot LED off.
                    QString qsLEDName = m_pCurrSnapShot->getLEDName( i );
                    bool bLEDState = pPacket->pSnapshot->getLEDState( i );
                    if ( bLEDState ) {
                        m_pCurrSnapShot->setLEDState( qsLEDName, !bLEDState );
                    }
                }
            }
                break;
            case COMM_PACKET::EQUAL: {
                *m_pCurrSnapShot = *(pPacket->pSnapshot);
            }
                    break;
        }
    }

    update();

 /*
    switch( eCommand ) {
        default:
        case COMM_PACKET::NO_COMMAND: {
            qDebug() << "SelectionAssitant: No Command Received";
        }
            break;
        case COMM_PACKET::SELECT_LED: {
            qDebug() << "SelectionAssitant: SELECT_LED Received";
        }
            break;
        case COMM_PACKET::ENTER: {
            qDebug() << "SelectionAssitant: ENTER Received";
        }
            break;
        case COMM_PACKET::NEXT_OPTION: {
            qDebug() << "SelectionAssitant: NEXT_OPTION Received";
        }
            break;
        case COMM_PACKET::PREVIOUS_OPTION: {
            qDebug() << "SelectionAssitant: PREVIOUS_OPTION Received";
        }
            break;
        case COMM_PACKET::ESCAPE: {
            qDebug() << "SelectionAssitant: ESCAPE Received";
        }
            break;
    }
    // store packet data.
 */

}
#endif


void GLCubeWidget::onNewSnapshot( const SnapShot& rSnapshot )
{
    (*m_pCurrSnapShot) = rSnapshot;
    update();
}
