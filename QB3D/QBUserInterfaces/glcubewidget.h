/***************************************
 *  File: GLCubeWidget.h
 *  Author: Josh Fadaie
 *  Date: 02/12/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/


#pragma once


#include <QOpenGLWidget>
#include <QOpenGlShaderProgram>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QVector3D>
#include <QPoint>

#include "snapshot.h"
#include "qbglobals.h"
//#include "communicationpacket.h"

struct COMM_PACKET;

class GLCubeWidget : public QOpenGLWidget,
                     public QOpenGLFunctions {
    Q_OBJECT

public:
                        GLCubeWidget( QWidget* pParent );
                       ~GLCubeWidget() {}
    void                InitCubeWidget( const QString& rqsObjFile );
protected:
    virtual void        initializeGL();
    virtual void        paintGL();
    virtual void        resizeGL(int width, int height);

    virtual void        mousePressEvent( QMouseEvent* pEvent );
    virtual void        mouseMoveEvent( QMouseEvent* pEvent );
    virtual void        mouseDoubleClickEvent( QMouseEvent* pEvent );

    //virtual void        keyPressEvent( QKeyEvent* pEvent );

    // Create a zoom in and out function
    // Create grab screen method (when you shift plus drag )

private:
    QOpenGLShaderProgram*       m_pProgram;
    QMatrix4x4                  m_ModelMatrix;
    QMatrix4x4                  m_ViewMatrix;
    QMatrix4x4                  m_ProjectionMatrix;

    int                         m_iXRot;
    int                         m_iYRot;
    int                         m_iZRot;
    QPoint                      m_qptLastPos;
    QPoint                      m_qptDblClickPos;
    QSize                       m_ViewportSize;

    SnapShot*                   m_pCurrSnapShot;
    //COMM_PACKET*                m_pPacket;


    void                setXRotation(int iAngle);
    void                setYRotation(int iAngle);
    void                setZRotation(int iAngle);
    void                prepareCommPacket( COMM_PACKET& rPacket );

signals:
   /* void    userSelection(
                    const QB::UserCommand  eUserCommand,
                    LEDCoords* const pLEDCoords = nullptr );*/
   void    userSelection( LEDCoords lEDCoords );

public slots:
    void    onNewSnapshot( const SnapShot& rSnapshot );


};

