#pragma once

#include <QTabWidget>

class QStackedWidget;
class ControlHandler;

class VisualControlManager : public QTabWidget {
    Q_OBJECT

public:
    VisualControlManager( QWidget* pParent = 0 );
    ~VisualControlManager();

public slots:
    void onControllerChange(
                const QString& rqsKey = QString(),
                const QString& rqsType = QString() );
private:
    QStackedWidget* m_pStackedWidget;

    void addControlPage(
                ControlHandler* const pControlPage );
    void removeControlPage();

};

