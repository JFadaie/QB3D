/**************************************************
 *  File: effectspanel.cpp
 *  Author: Josh Fadaie
 *  Date: 06/29/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 *  TODO:
 *      - add popup menu to remove, add, and change panelhandlers.
 **************************************************/

#include <QVariant>
#include <QStringList>

#include "effectspanel.h"
#include "ui_effectspanel.h"
#include "panelhandler.h"
#include "visualhandler.h"
#include "visualslibrary.h"
#include "visualtitles.h"
#include "qbglobals.h"

enum eRadioButton {

};


/***************************************************************
 *
 * Function: EffectsPanel::constructor/destructor
 *
 * Description: Appends a tabbed page to the list. Returns the index.
 *              added position added to the list.
 *
 * Parameters:
 *      rKeyList: List of Strings representing the key used to construct a
 *                particular animation or selection handler from the library.
 *      pParent: Pointer to the parent.
 */


EffectsPanel::EffectsPanel(
                    const QStringList& rKeyList,
                    bool               bEnabled,
                    QWidget*           pParent ) :
              QWidget( pParent ),
                   ui(new Ui::EffectsPanel)
{
    ui->setupUi(this);
    m_pCurrentPanel = nullptr;
    m_KeyList = rKeyList;
    connect( ui->m_pPanelList, SIGNAL( itemClicked( QListWidgetItem* )),
                         this,   SLOT( onPanelClicked( QListWidgetItem* )));


    setStyleSheet( "QListWidget::item:selected {"
                        "background-color: darkGray; }"
                   "QListWidget::item {"
                        "background-color: lightGray; }" );
    initializeList( bEnabled );

#if 0
    Qt::darkGray
    Qt::lightGray
    QListView::item:hover  {
        background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #FAFBFE, stop: 1 #DCDEF1);
#endif

}

EffectsPanel::~EffectsPanel()
{
    delete ui;
}


/***************************************************************
 *
 * Function: EffectsPanel::addPanel
 *
 * Description: Adds the Panel Handler with the assocaited key
 *              to the list.
 *
 * Parameters:
 *      rqsKey: QString associated with the PanelHandler's visual effect.
 */

QListWidgetItem* EffectsPanel::addPanel( const QString& rqsKey )
{
    VisualsLibrary* const pLib = VisualsLibrary::getLibrary();
    VisualHandler* const pVisHandler =
                        pLib->getVisualHandler( rqsKey );

    PNLHANDLER_DATA* const pPanelData = pVisHandler->getPanelData();

    // Construct a PanelHandler object with the Panel Data. Associate the Panel
    // with a QListWidget item, and add to the list.
    PanelHandler* const pPanel = new PanelHandler( pPanelData );
    QListWidgetItem* pPanelItem = new QListWidgetItem;
    pPanelItem->setSizeHint( pPanel->sizeHint() );
    ui->m_pPanelList->addItem( pPanelItem );
    ui->m_pPanelList->setItemWidget( pPanelItem, pPanel );
    connect( pPanel,        &PanelHandler::stateChanged,
             this,          &EffectsPanel::onStateChanged );
    delete pVisHandler;

    return pPanelItem;
}

/***************************************************************
 *
 * Function: EffectsPanel::addPanelHandler
 *
 * Description: Public function adds the Panel Handler with the assocaited key
 *              to the list. Enables the newly added Panel Handler and disables
 *              the others. Emits the item clicked signal.
 *
 * Parameters:
 *      rqsKey: QString associated with the PanelHandler's visual effect.
 */

PanelHandler* EffectsPanel::addPanelHandler( const QString& rqsKey )
{
    QListWidgetItem* const pPanelItem = addPanel( rqsKey );
    PanelHandler* const pPanelHandler = getPanelHandler( pPanelItem );
    emit ui->m_pPanelList->itemClicked( pPanelItem );
    return pPanelHandler;
}

/***************************************************************
 *
 * Function: EffectsPanel::removePanelHandler
 *
 * Description: Removes the Panel Handler with the assocaited key
 *              from the list.
 *
 * Parameters:
 *      rqsKey: QString associated with the PanelHandler's visual effect.
 */

void EffectsPanel::removePanelHandler( const QString& rqsKey )
{
    int iCount = ui->m_pPanelList->count();
    for ( int i = 0; i < iCount; ++i ) {
        PanelHandler* const pPanelHandler = getPanelHandler( i );
        if ( pPanelHandler->handlerKey() == rqsKey ) {
            QListWidgetItem* const pItem =
                            ui->m_pPanelList->takeItem( i );
            delete pPanelHandler;
            delete pItem;
            break; // Item is found and removed.
        }
    }
    if ( iCount ) {
        QListWidgetItem* const pItem0 = ui->m_pPanelList->item( 0 );
        emit ui->m_pPanelList->itemClicked( pItem0 );
    }
}


/***************************************************************
 *
 * Function: EffectsPanel::onPanelClicked  [slot]
 *
 * Description: Disables all of the PanelHandler, except for the panel
 *              clicked. Emits a signal notifying observers that the
 *              current Panel has changed. If the effects panel is
 *              disabled no signal is emitted.
 *
 * Parameters:
 *      pPanel: pointer to the list widget associated with the PanelHandler
 *              clicked.
 */

void EffectsPanel::onPanelClicked( QListWidgetItem* pPanel )
{
    PanelHandler* const pPanelHandler = getPanelHandler( pPanel );

    // Disable every panel handler except for the panel clicked.
    for ( int iRow = 0; iRow < ui->m_pPanelList->count(); ++iRow ) {
        PanelHandler* const pCurrPanel = getPanelHandler( iRow );
        if ( pPanelHandler != pCurrPanel )
            pCurrPanel->setEnabled( false );
    }

    // Enable the panel clicked.
    pPanelHandler->setEnabled( true );
    ui->m_pPanelList->setCurrentItem( pPanel );

    // Update the current panel pointer.
    m_pCurrentPanel = pPanelHandler;
    emit panelChanged( pPanelHandler );

}


/***************************************************************
 *
 * Function: EffectsPanel::getPanelHandler
 *
 * Description: Returns a pointer to the panel handler associated
 *              the provided QListWidgetItem. Otherwise returns a
 *              nullptr.
 *
 * Parameters:
 *      pItem: pointer to the associated QListWidgetItem.
 *      iRow: integer value providing the row index of the PanelHandler.
 */

PanelHandler* EffectsPanel::getPanelHandler( QListWidgetItem* const pItem ) const
{
    QWidget* pItemWidget = ui->m_pPanelList->itemWidget( pItem );
    PanelHandler* pPanel = nullptr;
    if ( pItemWidget ) {
        pPanel = qobject_cast<PanelHandler* >( pItemWidget );
    }

    return pPanel;
}


PanelHandler* EffectsPanel::getPanelHandler( const int iRow ) const
{
    QListWidgetItem* const pItem = ui->m_pPanelList->item( iRow );
    PanelHandler* pPanel = getPanelHandler( pItem );
    return pPanel;
}


/***************************************************************
 *
 * Function: EffectsPanel::initializeList
 *
 * Description: Initializes the list of visual effect types presented
 *              to the user for selection.
 */

void EffectsPanel::initializeList( bool bEnabled )
{
    // Initialize the list widget with PanelHandler's constructed from the
    // list of keys associated with a visual effect ( i.e. animation or selection
    // handler ).
    for ( int i = 0; i < m_KeyList.size(); ++i ) {
        const QString& rqsVisualKey = m_KeyList[i];
        QListWidgetItem* const pPanelItem = addPanel( rqsVisualKey );
    }

    if ( bEnabled ) {
        QListWidgetItem* const pItem0 = ui->m_pPanelList->item( 0 );
        emit ui->m_pPanelList->itemClicked( pItem0 );
    }
    else
        disablePanels(); // Disabled all the panels initially.
}


void EffectsPanel::onStateChanged( const QString& )
{
    emit stateChanged( m_pCurrentPanel );
}

void EffectsPanel::disablePanels()
{
    for ( int i = 0; i < ui->m_pPanelList->count(); ++i ) {
        PanelHandler* const pHandler = getPanelHandler( i );
        pHandler->setDisabled( true );
    }
}

void EffectsPanel::enablePanel(int iRow)
{
    disablePanels();
    PanelHandler* const pHandler = getPanelHandler( iRow );
    pHandler->setEnabled( true );
}
