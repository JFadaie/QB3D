#include <QStackedWidget>
#include <QLabel>

#include "visualctrlmngr.h"
#include "controlhandler.h"
#include "selectionassistant.h"
#include "visualslibrary.h"
#include "qbglobals.h"
#include "animationhandler.h"

enum StackedDisplay {
    NO_OPTIONS = 0,
    OPTIONS
};

VisualControlManager::VisualControlManager( QWidget* pParent ) :
    QTabWidget( pParent )
{
    setTabPosition( QTabWidget::North );
    setStyleSheet( " QTabWidget::tab-bar { "
                        "alignment:left;"
                        "top: 10px; }");

    QLabel* const m_plbNoOptions = new QLabel;
    m_plbNoOptions->setText( "No Currently Options Available." );

    m_pStackedWidget = new QStackedWidget;
    m_pStackedWidget->addWidget( m_plbNoOptions );


    addTab( m_pStackedWidget, "Visuals Control" );

}

VisualControlManager::~VisualControlManager()
{

}

void VisualControlManager::onControllerChange(
                            const QString& rqsKey,
                            const QString& rqsType )
{
    removeControlPage();
    if ( rqsKey.isNull() ) {
        m_pStackedWidget->setCurrentIndex(
                        StackedDisplay::NO_OPTIONS );
        return;
    }

    if ( rqsType == ANIMATION_HANDLER ) {
         VisualsLibrary* const pLib = VisualsLibrary::getLibrary();
         SelectionAssistant* const pAssistant =
                        SelectionAssistant::getAssistant();
         AnimationHandler* pAni = pLib->getAnimationHandler( rqsKey );

         // Let the selection assistant know that the current animation
         // has been updated. The selection assistant will need to redraw
         // the widget with the new snapshot.
       //  connect( pAni, &AnimationHandler::aniControlsUpdated,
      //          pAssistant, &SelectionAssistant::onAniUpdated );

         ControlHandler* pAniControlPage = pAni->getControlHandler();
         addControlPage( pAniControlPage );
    }
    else {
        // TODO: for selection handlers if need be.
    }
}

void VisualControlManager::addControlPage(
                        ControlHandler* const pControlPage )
{
    // set the control page in the stacked widget
    m_pStackedWidget->addWidget( pControlPage );
    m_pStackedWidget->setCurrentIndex( StackedDisplay::OPTIONS );
    SelectionAssistant* const pAssistant = SelectionAssistant::getAssistant();

  //  connect( pControlPage, &ControlHandler::controlUpdated,
  //           pAssistant,   &SelectionAssistant::onControlUpdated );
}


void VisualControlManager::removeControlPage()
{
    m_pStackedWidget->setCurrentIndex( StackedDisplay::NO_OPTIONS );
    QWidget* const pControlHandler =
                m_pStackedWidget->widget( StackedDisplay::OPTIONS );
    m_pStackedWidget->removeWidget( pControlHandler );
    delete pControlHandler;
}
