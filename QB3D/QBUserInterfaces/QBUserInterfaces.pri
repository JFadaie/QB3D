INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

FORMS += \
    $$PWD/controlpanel.ui \
    $$PWD/ctrlpnlmgr.ui \
    $$PWD/effectspanel.ui

HEADERS += \
    $$PWD/controlpanel.h \
    $$PWD/ctrlpnlmgr.h \
    $$PWD/effectspanel.h \
    $$PWD/effectspnlmgr.h \
    $$PWD/glcubewidget.h \
    $$PWD/panelhandler.h \
    $$PWD/ui_controlpanel.h \
    $$PWD/ui_effectspanel.h \
    $$PWD/visualctrlmngr.h

SOURCES += \
    $$PWD/controlpanel.cpp \
    $$PWD/ctrlpnlmgr.cpp \
    $$PWD/effectspanel.cpp \
    $$PWD/effectspnlmgr.cpp \
    $$PWD/glcubewidget.cpp \
    $$PWD/panelhandler.cpp \
    $$PWD/visualctrlmngr.cpp
