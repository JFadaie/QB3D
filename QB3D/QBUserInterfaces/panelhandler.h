/**************************************************
 *  File: panelhandler.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QGroupBox>
#include <QLabel>
#include <QComboBox>

// TODO: have the selection handler and derived classes
// include the DATA_BUFFER and its derived struct. The data
// originates from the selection handler.
// might just want both these classes to include declarations for
// DATA_BUFFER.

struct PNLHANDLER_DATA {
    QString     qsHandlerKey;
    QStringList stateList;
    QString     qsEffectTitle;
    QString     asIconFile;         // TODO: Add once icons are used.
};

class PanelHandler : public QGroupBox {
    Q_OBJECT

public:
    PanelHandler(
                const PNLHANDLER_DATA* const pData,
                QWidget*                     pParent = 0 );
    // Allows the derived class to be delete by a pointer to the base
    // class.
    virtual ~PanelHandler() {}

    const QString        handlerKey() const {
                            return m_qsPanelKey; }
    const QString        handlerState() const {
                            const QString& rqsState = m_pcxEffects->currentText();
                            return rqsState; }


protected:
    QString     m_qsPanelKey;

    QLabel*     m_plbEffectTitle;
    QComboBox*  m_pcxEffects;

 signals:
    void                stateChanged( const QString& rqsState );
    // TODO: if not needed erase.
    void panelHandlerEnabled( PanelHandler* const pPanel );

    // TODO: emit a signal deleted for library to connect to.
    // if this object which caused the signal is in the library
    // (which it should be if the object is being connected to)
    // remove it from the library using its type to cast it,
    // key to identify and pointer to remove.
};

