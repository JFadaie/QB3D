/**************************************************
 *  File: ctrlpnlmgr.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QTabWidget>


class ControlPanelManager : public QTabWidget {
    Q_OBJECT

public:
    explicit ControlPanelManager( QWidget* pParent = 0 );
    ~ControlPanelManager();

    int addPage( QWidget*       pPage,
                 const QString& rqsTabTitle );

    int insertPage( int             iIndex,
                    QWidget*        pPage,
                    const QString&  rqsTabTitle );

private:

signals:
    void        panelHandlerChange( const QString& rqsHandlerKey );
};


