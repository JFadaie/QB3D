#pragma once

#include "controlhandler.h"


class AniPlaneController : public ControlHandler {
    Q_OBJECT

public:
    AniPlaneController( QWidget* pParent = 0 );
    ~AniPlaneController();

    //virtual void   getDataBuffer( DATA_BUFFER& rData );
   // virtual void   setDataBuffer( const DATA_BUFFER& rData );

protected:
    virtual void    virtualTest();

private:
  //  static RegisterPanelType<PlaneController> s_Reg;
};
