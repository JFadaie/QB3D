#pragma once

#include "selectionhandler.h"


class PlaneSelector : public SelectionHandler {
    Q_OBJECT

public:
    PlaneSelector( QObject* pParent = 0 );
    virtual ~PlaneSelector();
    virtual void virtualTest();
    virtual bool primeSelector(
                            const SELHANDLER_DATA& rData,
                            bool&                  rbValidEffect );

protected:
   // virtual void   getDataBuffer( DATA_BUFFER& rData );
   // virtual void   setDataBuffer( const DATA_BUFFER& rData );
    QList<LEDCoords >   m_LEDCoords;

private:
    static RegisterVisualType<PlaneSelector> s_Reg;
    QString     m_qsEffectState;
    void        generateVPlanes(
                            SnapShot* pBaseSnap,
                            const LEDCoords& rCoord);
    bool        generate3PtPlane(
                            SnapShot* pBaseSnap,
                            const LEDCoords& rCoord,
                            bool             bBounded,
                            bool&            rbValidEffect );
    // TODO: emit a signal deleted for library to connect to.
    // if this object which caused the signal is in the library
    // (which it should be if the object is being connected to)
    // remove it from the library using its type to cast it,
    // key to identify and pointer to remove.
};

