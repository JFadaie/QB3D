INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/animationhandler.h \
    $$PWD/aniplanecontroller.h \
    $$PWD/controlhandler.h \
    $$PWD/planeselector.h \
    $$PWD/selectionhandler.h \
    $$PWD/visualhandler.h

SOURCES += \
    $$PWD/animationhandler.cpp \
    $$PWD/aniplanecontroller.cpp \
    $$PWD/controlhandler.cpp \
    $$PWD/planeselector.cpp \
    $$PWD/selectionhandler.cpp \
    $$PWD/visualhandler.cpp
