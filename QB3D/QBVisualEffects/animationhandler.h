#pragma once

#include <QVector>

#include "visualhandler.h"
#include "controlhandler.h"
#include "qbglobals.h"


class AnimationHandler : public VisualHandler {
    Q_OBJECT
public:
    explicit AnimationHandler( QObject *parent = 0 );
    ~AnimationHandler();

    virtual bool         primeAnimator(
                            const VISUALHANDLER_DATA& rData,
                            bool&                     bValidEffect ) = 0;
    QList<SnapShot* >*   getCurrentSnapshots();
    QList<SnapShot* >*   getPreviousSnapshots();
    QList<SnapShot* >*   getNextSnapshots();
    int                  optionCount() {
                            return m_SnapshotOptions.size(); }
    QB::AnimationSpeed   speed() { return m_eSpeed; }
signals:

protected:
    QList<QVector<SnapShot>>  m_SnapshotOptions;
    int                       m_iCurrentIndex;
    QList<LEDCoords>          m_LEDCoords;
    bool                      m_bPrimed;
    QB::AnimationSpeed        m_eSpeed;
    QB::SnapShotOp            m_eOperation;
    virtual ControlHandler* createHandler() { return nullptr; /*TODO*/ }
private slots:


private:


};


