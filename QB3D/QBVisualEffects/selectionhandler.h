/**************************************************
 *  File: selectionhandler.h
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QStringList>

#include "visualhandler.h"
#include "snapshot.h"
#include "creationlibrary.h"


struct SELHANDLER_DATA {
    QString     qsEffectState;
    SnapShot*   pSnapshot;
    LEDCoords   ledCoord;

};

class SelectionHandler : public VisualHandler {
    Q_OBJECT

public:
    SelectionHandler( QObject* pParent = 0 );
    // Allows the derived class to be deleted by a pointer to the
    // base class. TODO: if you call the base class implementation of
    // the destrutor in derived classes. Is the base class constructor
    // always called. Do I need to include QObject here and SelectionHandler
    // in PlaneSelector class?
    virtual ~SelectionHandler();
    virtual void virtualTest();

    // TODO: Determine if these functions need to have a base definition
    // and then have it be overridden if needed. It seems like a lot of
    // the beginning classes will just have a label and a combo box.
   // virtual void   getDataBuffer( DATA_BUFFER& rData ) = 0;
   // virtual void   setDataBuffer( const DATA_BUFFER& rData ) = 0;
    virtual bool   primeSelector(
                            const VISUALHANDLER_DATA& rData,
                            bool&                     bValidEffect ) = 0;
    SnapShot*      getCurrentSnapshot();
  //  SnapShot*      selCurrentSnapshot();
    SnapShot*      getPreviousSnapshot();
    SnapShot*      getNextSnapshot();
    int            optionCount() {
                        return m_SnapshotOptions.size(); }

protected:

    QList<SnapShot >    m_SnapshotOptions;
    int                 m_iCurrentIndex;
    QList<LEDCoords >   m_LEDCoord;
    bool                m_bPrimed;

    virtual ControlHandler* createHandler() { return nullptr; }
private slots:
    // TODO: might listen to selection assistnt to erase its data when a
    // user presses enter, escape, or dismisses the handler by clicking on a
    // new led.


private:


};

