#include "visualhandler.h"
#include "panelhandler.h"

VisualHandler::VisualHandler(QObject* pParent) : QObject( pParent )
{
    m_pPanelData = new PNLHANDLER_DATA;

}

VisualHandler::~VisualHandler()
{
    delete m_pPanelData;
}


ControlHandler* VisualHandler::getControlHandler()
{
    if ( m_pControlHandler != nullptr )
        return m_pControlHandler;

    // Overridden virtual function creating the derived control
    // handler type for the created animation.
    ControlHandler* const pControlHandler = createHandler();
    if ( pControlHandler == nullptr )
        return nullptr; // The createHandler() has not been overridden;
    m_pControlHandler = pControlHandler;
    connect( pControlHandler, &ControlHandler::destroyed,
             this,            &AnimationHandler::onControlHandlerDeleted );
    connect( pControlHandler, &ControlHandler::controlNotification,
                        this, &AnimationHandler::onControlNotification );

    return m_pControlHandler;
}

ControlHandler* VisualHandler::createHandler()
{
#if 0
    ControlDataBase ctrlData;
    ControlHandler* const pControlHandler = new ControlHandler( &ctrlData, this );
    return pControlHandler;
#endif
    return nullptr;
}

void VisualHandler::onControlNotification(
                                        const QString &rqsControlId )
{

    // TODO: fill and act based on the id value provided.

    // After handling, let others know that the animation has been
    // updated.
    emit visualControlsUpdated();
}
