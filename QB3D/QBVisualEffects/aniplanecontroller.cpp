#include "aniplanecontroller.h"
#include <QDebug>
#include "visualtitles.h"

AniPlaneController::AniPlaneController( QWidget* pParent ) :
        ControlHandler( pParent )
{
#if 0
    m_plbEffectTitle->setText( "Plane Effect:");

    QStringList effectsList;
    effectsList
            << "Vertical Selection"
            << "Horizontal Selection"
            << "Diagonal Selection"
            << "3-Point Selection";

    m_pcxEffects->addItems( effectsList );

    m_qsPanelKey = QString( PLANE_SELECTOR );
    qDebug() << "CREATED PLANE PANEL!";
#endif
}

AniPlaneController::~AniPlaneController()
{

}

void AniPlaneController::virtualTest()
{
    qDebug() << "I am PlanePanel";
    qDebug() << "I Derive from PanelHandler.";
}



