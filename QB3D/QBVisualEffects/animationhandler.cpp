#include "animationhandler.h"

AnimationHandler::AnimationHandler(QObject *parent) :
                VisualHandler(parent)
{
    VisualHandler::m_qsHandlerType = QString( ANIMATION_HANDLER );
    m_bPrimed = false;
    m_iCurrentIndex = -1;
    m_eSpeed = QB::MEDIUM;
    m_eOperation = QB::ADD;
}

AnimationHandler::~AnimationHandler()
{
}


QList<SnapShot* >* AnimationHandler::getCurrentSnapshots() {
    if ( m_iCurrentIndex == -1 )
        return nullptr;
    qDebug() << "SelectionHandler: Current Index " << m_iCurrentIndex;
    return &m_SnapshotOptions[m_iCurrentIndex];;
}


QList<SnapShot* >* AnimationHandler::getPreviousSnapshots() {
    if ( m_SnapshotOptions.isEmpty() || m_iCurrentIndex == -1 )
        return nullptr;

    // If at the beginning of the list, go to the end. Otherwise, point
    // to the previous Snapshot in the list.
    m_iCurrentIndex = ( m_iCurrentIndex == 0 ) ?
                        m_SnapshotOptions.size() - 1 : m_iCurrentIndex - 1;
    qDebug() << "SelectionHandler: Current Index " << m_iCurrentIndex;
    pSnapshot = &m_SnapshotOptions[m_iCurrentIndex];
    return pSnapshot;
}


QList<SnapShot* >* AnimationHandler::getNextSnapshots() {
    if ( m_SnapshotOptions.isEmpty() || m_iCurrentIndex == -1 )
        return nullptr;

    // If at the end of the list, go to the beginning. Otherwise, point
    // to the next Snapshot in the list.
    m_iCurrentIndex = ( m_iCurrentIndex == m_SnapshotOptions.size() - 1 ) ?
                        0 : m_iCurrentIndex + 1;
    qDebug() << "SelectionHandler: Current Index " << m_iCurrentIndex;

    pSnapshot = &m_SnapshotOptions[m_iCurrentIndex];
    return pSnapshot;
}
