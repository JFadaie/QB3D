#include <QDebug>

#include "planeselector.h"
#include "visualtitles.h"
#include "panelhandler.h"
#include "qbglobals.h"



RegisterVisualType<PlaneSelector> PlaneSelector::s_Reg( PLANE_SELECTOR );


PlaneSelector::PlaneSelector( QObject* pParent ) :
    SelectionHandler( pParent )
{
    VisualHandler::m_qsHandlerKey = QString( PLANE_SELECTOR );
    qDebug() << "CREATED PLANE SELECTOR!";
    m_pPanelData->stateList
            << "Vertical Selection"
            << "Horizontal Selection"
            << "Diagonal Selection"
            << "3-Point Selection"
            << "3-Pt Unbounded";
    m_pPanelData->qsEffectTitle = QString( "Plane Effect" );
    m_pPanelData->qsHandlerKey = VisualHandler::m_qsHandlerKey;
}

PlaneSelector::~PlaneSelector()
{

}

void PlaneSelector::virtualTest()
{
    qDebug() << "I am PlaneSelector";
    qDebug() << "I Derive from SelectionHandler.";
}



bool  PlaneSelector::primeSelector(
                                    const SELHANDLER_DATA& rData,
                                    bool&                  rbValid)
{

    bool bValidEffect = true;

    // TODO: only reset the variables if the Selector has been primed.
    // otherwise the
    // Reset the state of the member variables if the Selector has been
    // previously primed, or if a new effect has been chosen.
    if ( m_bPrimed || m_qsEffectState != rData.qsEffectState ) {
        m_iCurrentIndex = -1;
        m_SnapshotOptions.clear();
        m_bPrimed = false;
    }

    m_LEDCoord = rData.ledCoord;
    m_qsEffectState = rData.qsEffectState;
    if ( rData.qsEffectState == "Vertical Selection" ) {
        m_bPrimed = true;
        generateVPlanes( rData.pSnapshot, rData.ledCoord);
    }
    else if ( rData.qsEffectState == "3-Point Selection" ) {
        m_bPrimed = generate3PtPlane(
                                rData.pSnapshot,
                                rData.ledCoord,
                                true, // bounded
                                bValidEffect );
    }
    else if ( rData.qsEffectState == "3-Pt Unbounded" ) {
        m_bPrimed = generate3PtPlane(
                                rData.pSnapshot,
                                rData.ledCoord,
                                false, // bounded
                                bValidEffect );
    }
    else { // TODO: Fix by expanding options.
        m_bPrimed = true;
        generateVPlanes( rData.pSnapshot, rData.ledCoord );

    }

    rbValid = bValidEffect;
    return m_bPrimed;
}


void PlaneSelector::generateVPlanes( SnapShot* pBaseSnap, const LEDCoords& rCoord )
{
    // There are two vertical planes that intersect the provided LED coordinate
    // along a vertical line. One is a vertical plane that expands along the X and
    // Z coordinates. The other expands along the Y and Z axis.
    SnapShot resultSnap = SnapShot();
    SnapShot optionSnap = SnapShot();
    LEDArray& rLEDArray = optionSnap.getSelectableLEDS();

    // Get the XZ expanding plane.
    for ( int iX = 0; iX < pBaseSnap->sizeX(); ++iX ) {
        for ( int iZ = 0; iZ < pBaseSnap->sizeZ(); ++iZ ) {
            rLEDArray[iX][rCoord.iY][iZ] = LEDState::SELECTED;
        }
    }
    SnapShot::addOptionToOriginal( resultSnap, *pBaseSnap, optionSnap );

    m_SnapshotOptions.append( resultSnap );
    resultSnap.clear();
    optionSnap.clear();

    // Add the YZ expanding plane.
    for ( int iY = 0; iY < pBaseSnap->sizeY(); ++iY ) {
        for ( int iZ = 0; iZ < pBaseSnap->sizeZ(); ++iZ ) {
            rLEDArray[rCoord.iX][iY][iZ] = LEDState::SELECTED;
        }
    }

    SnapShot::addOptionToOriginal( resultSnap, *pBaseSnap, optionSnap );
    m_SnapshotOptions.append( resultSnap );

    // Set the index to the first SnapShot option.
    m_iCurrentIndex = 0;
}


/***************************************************************
 *
 * Function: PlaneSelector::generate3PtPlane
 *
 * Description:
 *
 * Parameters:
 *
 */

bool PlaneSelector::generate3PtPlane(
                                    SnapShot*        pBaseSnap,
                                    const LEDCoords& rCoord,
                                    bool             bBounded,
                                    bool&            rbValidEffect )
{
    m_SnapshotOptions.clear();
    for ( int i = 0; i < m_LEDCoords.size(); ++i ) {
        const LEDCoords& rLEDCoord = m_LEDCoords[i];
        if ( rLEDCoord.equal( rCoord ) ) {
            rbValidEffect = false;
            // TODO: emit an error message to the status consol saying an invalid
            // message occurred.
            qDebug() << "PlaneSelector::generate3PtPlane - Invalid point.";
            return false;
        }
    }
    m_LEDCoords.append( rCoord );
    qDebug() << "PlaneSelector::Generate3PtPlane Num Coords:" << m_LEDCoords.size();

    if ( m_LEDCoords.size() != 3 ) {
        SnapShot resultSnap = SnapShot();
        SnapShot optionSnap = SnapShot();
        LEDArray& rLEDArray = optionSnap.getSelectableLEDS();
        for ( int i = 0; i < m_LEDCoords.size(); ++i ) {
            const LEDCoords& rLEDCoord = m_LEDCoords[i];
            rLEDArray[rLEDCoord.iX][rLEDCoord.iY][rLEDCoord.iZ] = SELECTED;
        }
        SnapShot::addOptionToOriginal( resultSnap, *pBaseSnap, optionSnap );
        m_SnapshotOptions.append( resultSnap );
        // Set the index to the first SnapShot option.
        m_iCurrentIndex = 0;
        return false; // 3-points have not been tried.
    }

    // Ensure the three points used are valid.

    int iMinX, iMinY, iMinZ;
    int iMaxX, iMaxY, iMaxZ;

    if ( bBounded ) {
        LEDCoords::boundsX( m_LEDCoords, iMinX, iMaxX );
        LEDCoords::boundsY( m_LEDCoords, iMinY, iMaxY );
        LEDCoords::boundsZ( m_LEDCoords, iMinZ, iMaxZ );
    } else {
        iMinX = iMinY = iMinZ = 0;
        QB::CubeDimensions( iMaxX, iMaxY, iMaxZ );
        // Decrement by 1 so consistent equalities can be used.
        iMaxX--;
        iMaxY--;
        iMaxZ--;
    }

    // Determine equation for plane based on points.
    QVector3D vN3D; // Normal vector for the plane.
    const float fD = QB::Plane(
                        &m_LEDCoords[0], &m_LEDCoords[1], &m_LEDCoords[2],
                        vN3D );

    // Iterate over boundary
    SnapShot resultSnap = SnapShot();
    SnapShot optionSnap = SnapShot();
    LEDArray& rLEDArray = optionSnap.getSelectableLEDS();
    for ( int iX = iMinX; iX <= iMaxX; ++iX ) {
        for ( int iY = iMinY; iY <= iMaxY; ++iY ) {
            for ( int iZ = iMinZ; iZ <= iMaxZ; ++iZ ) {
                bool bValid =
                        iX*vN3D.x() + iY*vN3D.y() + iZ*vN3D.z() == fD;
                if ( bValid ) {
                    rLEDArray[iX][iY][iZ] = LEDState::SELECTED;
                }
            }
        }
    }
    SnapShot::addOptionToOriginal( resultSnap, *pBaseSnap, optionSnap );

    m_SnapshotOptions.append( resultSnap );

    // Set the index to the first SnapShot option.
    m_iCurrentIndex = 0;
    m_LEDCoords.clear();
    rbValidEffect = true;
    return true;
}

