#pragma once

#include <QWidget>

class QPushButton;

// Meant to be derived from.
struct ControlDataBase;


class ControlHandler : public QWidget {

    friend class VisualHandler;
    friend class AnimationHandler;
    friend class SelectionHandler;
    Q_OBJECT
public:
    explicit ControlHandler(
                      //  ControlDataBase* pCtrlData,
                        QWidget*         pParent = 0 );
    ~ControlHandler();



signals:
    void controlNotification(
                    const QString& rqsControlId );
    void controlUpdated();

public slots:
  //  void onControlWidget();
private:
    QPushButton* m_pbtPreview;
    QWidget*     m_pControlContainer;
};

