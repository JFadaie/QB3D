#pragma once

#include <QObject>
#include "controlhandler.h"
#include "snapshot.h"

struct PNLHANDLER_DATA;

struct VISUALHANDLER_DATA {
    QString     qsEffectState;
    LEDCoords   ledCoord;
};

class VisualHandler : public QObject {
    Q_OBJECT
public:
    explicit            VisualHandler(QObject* pParent = 0 );
                       ~VisualHandler();
    const QString       handlerKey() { return m_qsHandlerKey; }

    const QString       handlerType() { return m_qsHandlerType; }

    PNLHANDLER_DATA*    getPanelData() { return m_pPanelData; }

    void                setEffectState( const QString& rqsState ) {
                                m_qsEffectState = rqsState; }
    const QString       getEffectState() { return m_qsEffectState; }

    ControlHandler*     getControlHandler();

protected:
    QString             m_qsHandlerKey;
    QString             m_qsHandlerType;
    QString             m_qsEffectState;
    ControlHandler*     m_pControlHandler;

    PNLHANDLER_DATA*    m_pPanelData;

    virtual ControlHandler* createHandler() = 0;

signals:
    void                visualControlsUpdated();

protected slots:
    virtual void        onControlNotification( const QString& rqsControlId );

private slots:
    void                onControlHandlerDeleted( QObject* pObject ) {
                                m_pControlHandler = nullptr; }


};


