#include <QDebug>

#include "selectionhandler.h"
#include "visualtitles.h"
#include "qbglobals.h"
#include "panelhandler.h"



SelectionHandler::SelectionHandler( QObject* pParent ) :
    VisualHandler( pParent )
{
    m_bPrimed = false;
    m_iCurrentIndex = -1;
    m_SnapshotOptions = QList<SnapShot>();
    VisualHandler::m_qsHandlerType = QString( SELECTION_HANDLER );
}

SelectionHandler::~SelectionHandler()
{
}


void SelectionHandler::virtualTest()
{
    qDebug() << "I am the base class, SelectionHandler";
}


SnapShot* SelectionHandler::getCurrentSnapshot() {
    SnapShot* pSnapshot = nullptr;
    if ( m_iCurrentIndex > -1 ) {
        pSnapshot = &m_SnapshotOptions[m_iCurrentIndex];
    }
    qDebug() << "SelectionHandler: Current Index " << m_iCurrentIndex;
    return pSnapshot;
}


SnapShot* SelectionHandler::getPreviousSnapshot() {
    SnapShot* pSnapshot = nullptr;
    if ( m_SnapshotOptions.isEmpty() || m_iCurrentIndex == -1 )
        return pSnapshot;

    // If at the beginning of the list, go to the end. Otherwise, point
    // to the previous Snapshot in the list.
    m_iCurrentIndex = ( m_iCurrentIndex == 0 ) ?
                        m_SnapshotOptions.size() - 1 : m_iCurrentIndex - 1;
    qDebug() << "SelectionHandler: Current Index " << m_iCurrentIndex;
    pSnapshot = &m_SnapshotOptions[m_iCurrentIndex];
    return pSnapshot;
}


SnapShot* SelectionHandler::getNextSnapshot() {
    SnapShot* pSnapshot = nullptr;
    if ( m_SnapshotOptions.isEmpty() || m_iCurrentIndex == -1 )
        return pSnapshot;

    // If at the end of the list, go to the beginning. Otherwise, point
    // to the next Snapshot in the list.
    m_iCurrentIndex = ( m_iCurrentIndex == m_SnapshotOptions.size() - 1 ) ?
                        0 : m_iCurrentIndex + 1;
    qDebug() << "SelectionHandler: Current Index " << m_iCurrentIndex;

    pSnapshot = &m_SnapshotOptions[m_iCurrentIndex];
    return pSnapshot;
}

SnapShot* SelectionHandler::selCurrentSnapshot()
{
    SnapShot* pSnapShot = getCurrentSnapshot();
    // Create new SnapShot.
    //const QList<QString>& nameList = pSnapShot->getLEDNames();
    pSnapShot->finalize();
    return pSnapShot;
}

// Provide a function to get the state of the operation (either
// subtraction or addition ).
