// myglwidget.cpp

#include <QtWidgets>
#include <QtOpenGL>
#include <QDebug>
#include <qmath.h>

#include "objprocessor.h"
#include "myglwidget.h"


#define     OBJ_FILE    "/Users/joshfadaie/BlenderFiled/ColorCubeX2_V2.obj"

//#define     STATIC_CUBE

int gPOS_VEC_SIZE;


MyGLWidget::MyGLWidget(QWidget *parent)
// Specififies display format of OpenGL rendering context.
// Allows OpenGL context with multisampling support.Multisampling
// samples each pixel at the edge of a polygon multiple times. A slight offset
// is applied each pass (smaller than the pixel). In averaging these samples, a
// smoother color transition results at the edges.
    : QOpenGLWidget( parent )
{
   // m_pGLFuncs = context()->functions();
    //qDebug() << QGLFormat::openGLVersionFlags();
    m_iVAOToggle = 0; // Front.

}

MyGLWidget::~MyGLWidget()
{
    makeCurrent();

    m_fPositionBuffer.destroy();
    m_fNormalBuffer.destroy();
    m_iIndexBuffer.destroy();
    m_QVAO.destroy();

    m_bckfPositionBuffer.destroy();
    m_bckfNormalBuffer.destroy();
    m_bckQVAO.destroy();

    doneCurrent();
}



// Sets up the OpenGL rendering context, defines display lists, etc.
//Gets called once before the first time resizeGL() or paintGL() is called.
void MyGLWidget::initializeGL()
{
    m_iColorToggle = 0;
    m_iBckColorToggle = 0;
    qDebug() << "Viewport Size: " << size();
    m_ViewportSize = size();

    // Convenience function for specifying the clearing color to OpenGL.
    // Calls glClearColor (in RGBA mode) or glClearIndex (in color-index
    // mode) with the color c. Applies to this widgets GL context.
    glClearColor( 0.5, 0.5, 0.5, 1);

    // Enables depth comparisions and updating the depth buffer.
    glEnable(GL_DEPTH_TEST);

    // Culls polygons based on their winding in window coords
    glEnable(GL_CULL_FACE);

    // Smooth shading, the default, causes the computed colors of vertices
    // to be interpolated as the primitives are rasterized.
    glShadeModel(GL_SMOOTH);

    // If enabled and no vertex shader is active, use the current lighting
    // parameters to compute the vertex color or index. Otherwise, simply
    // associate the current color or index with each vertex. Prepares
    // openGL for lighting calculations.
    glEnable(GL_LIGHTING);

    // Enables light source 0.
    glEnable(GL_LIGHT0);

    // x, y, z and w coordinates of the light source.
    // If w == 0, the light source is directional, if w == 1,
    // it is positional.
    static GLfloat lightPosition[4] = { 0, 0, 17, 0};

    // Positions the light source 0, at 'lightPosition'. Default
    // color is white. For different color use, glLight*().
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);


    // Process OBJ info.
    QString qsFileName = OBJ_FILE;
    OBJProcessor objProcessor( qsFileName, this, OBJProcessor::All );
    QVector<float> fPosVector = objProcessor.getVerticesData();
    gPOS_VEC_SIZE = fPosVector.size();
    QVector<float> fNormVector = objProcessor.getNormalsData();

    QVector<float> fPosVector2 = objProcessor.getVerticesData2();
    QVector<float> fNormVector2 = objProcessor.getNormalsData2();

    qDebug() << "Size of Data " << fPosVector.size();
    qDebug() << "Size of Data in Bytes" << fPosVector.size() * sizeof(float);

    m_Program.addShaderFromSourceFile(QOpenGLShader::Vertex,  ":/qrc/vertexShader.vert" );
    m_Program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/qrc/fragmentShader.frag" );
    m_Program.bind();
    m_Program.link();


    // Returns location of attributes name within the shader program's
    // attribute list.
    _positionAttr = m_Program.attributeLocation( "position" );
    _normalAttr = m_Program.attributeLocation( "normal" );
/*
    // Should draw a square.
    const GLfloat glfPositionData[7][3] = {
        { -1, -1 ,0 }, { 1, -1, 0 },  // Bottom of square.
        { 1, 1, 0 }, { -1, 1 ,0 } };  // Top of square.

    const GLuint gluiIndices[6] = { 0, 1, 2, 2, 3, 0 };
*/
    // Should draw a square.
     GLfloat glfPositionData[8][3] = {
        { 1, -1 ,-1 }, { 1, -1 , 1 },  // Bottom of square.
        { 1,  1 , 1 }, { 1,  1 ,-1 },
        {-1, -1 ,-1 }, {-1, -1 , 1 },
        {-1,  1 , 1 }, {-1,  1 ,-1 } };  // Top of square.

   /* const GLuint gluiIndices[36] = { 0, 1, 3, 1, 2, 3,
                                     3, 2, 7, 2, 6, 7,
                                     0, 4, 1, 4, 5, 1,
                                     4, 7, 5, 7, 6, 5,
                                     1, 5, 2, 5, 6, 2,
                                     0, 3, 4, 3, 7, 4};
                                     */

    // Direction is counter clockwise.
    const GLuint gluiIndices[36] = { 1, 3, 2, 1, 0, 3,  // Right
                                     2, 3, 7, 2, 7, 6,  // Top
                                     1, 5, 4, 1, 4, 0,  // Bottom
                                     5, 6, 7, 5, 7, 4,  // Left
                                     2, 6, 5, 2, 5, 1,  // Front
                                     4, 3, 0, 4, 7, 3}; // Back

    // Store each of the vertices xyz data points in QVector3D.
    QVector<QVector3D> glfPosVector3D;
    for (int i = 0; i < 36; ++i ) {
        collectData( glfPositionData, gluiIndices[i], glfPosVector3D );
    }

    // Compute the normal of the 3 vertices making up a triangle mesh. Add the normal
    // three times for each vertex of the triangle.
    QVector<QVector3D> glfNormalVector3D;
    for ( int i = 0; i < 36; i += 3 ) {
        QVector3D normalVector = computeNormals( glfPosVector3D[i],
                                                 glfPosVector3D[i+1],
                                                 glfPosVector3D[i+2] );
        // Add the same vector 3 times.
        for (int j = 0; j < 3; ++j) {
            glfNormalVector3D << normalVector;
        }
    }

    QVector<QVector3D> fBckPosVector;

    QVector<QVector3D> fBckNormVector;


/*
    // Translate the vector back.
    for (int i = 0; i < fPosVector.size(); ++i ) {
        QVector3D posVec3DCpy = fPosVector[i];
        QVector4D posVec4D = m_backTranslateMatrix*QVector4D( posVec3DCpy , 1.0 );
        glfBckPosVector3D << posVec4D.toVector3D();

        QVector3D normalVec3DCpy = fNormVector[i];
        //QVector4D normVec4D = m_backTranslateMatrix*QVector4D( normalVec3DCpy, 0.0 );
        //glfBckNormalVector3D << normVec4D.toVector3D();
        fBckNormVector << normalVec3DCpy;
    }

*/
    QVector<QVector3D> posVec3D;
    posVec3D << QVector3D( 1, 0, 0) << QVector3D(0, 1, 0 )
             << QVector3D(-1, 0, 0);

    // Set up normal data.
    QVector<QVector3D> normVec3D;
    normVec3D << QVector3D( 0, 0, 1) << QVector3D( 0, 0, 1)
              << QVector3D( 0, 0, 1);

    // Creates a new Vertex Array Object
    m_QVAO.create();
    m_QVAO.bind();

    qDebug() << " Size of array" << sizeof(glfPositionData);
    qDebug() << " Size of Glfloat" << sizeof(GLfloat);

   // Create the position vertex buffer.
   m_fPositionBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);

   // Creates buffer object in OpenGL server. Similar to glGenVertexArrays(1, &m_id).
   m_fPositionBuffer.create();
   m_fPositionBuffer.setUsagePattern(QOpenGLBuffer::StreamDraw);

   // Binds the buffer associated with this object to the current opengl context.
   // Similar to calling glBindVertexArray(m_id).
   m_fPositionBuffer.bind();
#ifdef STATIC_CUBE
   // Allocates count bytes of space to the buffer, initialized to the contents of data.
   //m_fPositionBuffer.allocate(glfPositionData, sizeof(glfPositionData) );
    m_fPositionBuffer.allocate(glfPosVector3D.data(), 36*sizeof(QVector3D ) );

    // Link vertex shader attribute to the data contained in the currently
    // boud VBO. Then enable the attribute.
    m_Program.setAttributeBuffer( _positionAttr, GL_FLOAT, 0, 3, sizeof(QVector3D));
#else

    m_fPositionBuffer.allocate(fPosVector.data(), fPosVector.size()*sizeof(float ) );
    m_Program.setAttributeBuffer( _positionAttr, GL_FLOAT, 0, 3 );
#endif
    m_Program.enableAttributeArray( _positionAttr );

#if 0
   // Create the indices vertex buffer.
   m_iIndexBuffer = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
   m_iIndexBuffer.create();
   m_iIndexBuffer.setUsagePattern(QOpenGLBuffer::StreamDraw);
   m_iIndexBuffer.bind();
   m_iIndexBuffer.allocate(gluiIndices, sizeof(gluiIndices));
#endif

   // Create the indices vertex buffer.
   m_fNormalBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
   m_fNormalBuffer.create();
   m_fNormalBuffer.setUsagePattern(QOpenGLBuffer::StreamDraw);
   m_fNormalBuffer.bind();

#ifdef STATIC_CUBE
   m_fNormalBuffer.allocate(glfNormalVector3D.data(), 36*sizeof(QVector3D ));

   // Link vertex shader attribute to the data contained in the currently
   // boud VBO. Then enable the attribute.
   m_Program.setAttributeBuffer( _normalAttr, GL_FLOAT, 0, 3, sizeof(QVector3D));
#else
    m_fNormalBuffer.allocate( fNormVector.data(), fNormVector.size()*sizeof(float ));
    m_Program.setAttributeBuffer( _normalAttr, GL_FLOAT, 0, 3 );
#endif
   //m_Program.setAttributeBuffer( _positionAttr, GL_FLOAT, 0, 3 );
   m_Program.enableAttributeArray( _normalAttr );



 // ---------- VAO BACK --------- //
   // Creates a new Vertex Array Object
   m_bckQVAO.create();
   m_bckQVAO.bind();


  // Create the position vertex buffer.
  m_bckfPositionBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  m_bckfPositionBuffer.create();
  m_bckfPositionBuffer.setUsagePattern(QOpenGLBuffer::StreamDraw);
  m_bckfPositionBuffer.bind();

  m_bckfPositionBuffer.allocate(posVec3D.data(),
                                posVec3D.size()*sizeof(QVector3D ) );
  m_Program.setAttributeBuffer( _positionAttr, GL_FLOAT, 0, 3 );
  m_Program.enableAttributeArray( _positionAttr );

  // Create the indices vertex buffer.
  m_bckfNormalBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  m_bckfNormalBuffer.create();
  m_bckfNormalBuffer.setUsagePattern(QOpenGLBuffer::StreamDraw);
  m_bckfNormalBuffer.bind();

  m_bckfNormalBuffer.allocate( normVec3D.data(),
                               normVec3D.size()*sizeof(QVector3D ));
  m_Program.setAttributeBuffer( _normalAttr, GL_FLOAT, 0, 3);
  m_Program.enableAttributeArray( _normalAttr );


   qDebug() << m_Program.log();
}



// Renders the OpenGL scene. Gets called whenever the widget needs to be updated.
void MyGLWidget::paintGL()
{
    // Clears indicated buffer's to preset values. Sets the bitplane area of the window to
    // values previously selected by glClearColor, glClearDepth, glClearStencil.
    // GL_COLOR_BUFFER_BIT: indicates the buffers currently enabled for color writing.
    // GL_DEPTH_BUFFER_BIT: indicates the depth buffer.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_Program.bind();

    // Initial matrix is ModelView. ModelView matrix deals with rotations,
    // translations, etc.
   // glMatrixMode(GL_MODELVIEW);

    // Replaces the current matrix with the identity matrix. Semantically
    // equivalent to calling glLoadMatrix with the identity matrix.
   // glLoadIdentity();
    //m_backTranslateMatrix = QMatrix4x4();
    //m_backTranslateMatrix.translate( 0.0, 0.0, -10.0);
/*
    QMatrix4x4 mat4x4;
    mat4x4.translate(0.0, 0.0, -5.0);
    mat4x4.rotate(m_iXRot / 18.0, 1.0, 0.0, 0.0);
    mat4x4.rotate(m_iYRot / 18.0, 0.0, 1.0, 0.0);
    mat4x4.rotate(m_iZRot / 18.0, 0.0, 0.0, 1.0);
*/
    m_ModelView = QMatrix4x4();
    m_ModelView.translate(0.0, 0.0, -5.0);
    m_ModelView.rotate(m_iXRot / 18.0, 1.0, 0.0, 0.0);
    m_ModelView.rotate(m_iYRot / 18.0, 0.0, 1.0, 0.0);
    m_ModelView.rotate(m_iZRot / 18.0, 0.0, 0.0, 1.0);
    m_iVAOToggle = 0;

    m_QVAO.bind();
    m_Program.setUniformValue( "mvMatrix", m_ModelView );
    m_Program.setUniformValue( "pMatrix", m_Projection );
    m_Program.setUniformValue( "toggle", m_iColorToggle );
    m_Program.setUniformValue( "bcktoggle", m_iBckColorToggle );
    m_Program.setUniformValue( "VAOToggle", m_iVAOToggle );
    //m_Program.setUniformValue( "bcktMatrix", m_backTranslateMatrix );

    // Provide the vertices/ primitives to draw on the canvas.
#ifdef STATIC_CUBE
    glDrawArrays(GL_TRIANGLES, 0, 36);
#else
    glDrawArrays(GL_TRIANGLES, 0, gPOS_VEC_SIZE );
#endif
   // glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0 );
    //m_Program.disableUniformValue( "bcktMatrix", m_backTranslateMatrix );
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_iVAOToggle = 1;
    m_Program.setUniformValue( "VAOToggle", m_iVAOToggle );
    m_bckQVAO.bind();
    glDrawArrays(GL_TRIANGLES, 0, 3 );
}



// Sets up the OpenGL viewport, projection, etc. Gets called whenever
// the widget has been resized (and also when it is shown for the first
// time because all newly created widgets get a resize event automatically)
void MyGLWidget::resizeGL(int w, int h)
{
    m_ViewportSize = QSize( w, h);
    qDebug() << "resizeGL Params w: " << w << " h: " << h;

    //aint side = qMin(width, height);

    // Determines the portion of the window to which OpenGL is drawing to.
    //glViewport((width - side) / 2, (height - side) / 2, side, side);

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 0.1, zFar = 50, fov = 45.0;

    // Reset projection
    m_Projection.setToIdentity();

    // Set perspective projection
    m_Projection.perspective(fov, aspect, zNear, zFar);

    update();
}


void MyGLWidget::mousePressEvent(QMouseEvent *event)
{
    m_qptLastPos = event->pos();
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - m_qptLastPos.x();
    int dy = event->y() - m_qptLastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(m_iXRot + 18 * dy);
        setYRotation(m_iYRot + 18 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(m_iXRot + 18 * dy);
        setZRotation(m_iZRot + 18 * dx);
    }

    m_qptLastPos = event->pos();
}

void MyGLWidget::normalizeAngle(int& riAngle)
{
#if 0
    while (riAngle < 0)
        riAngle += 360 * 18;
    while (riAngle > 360)
        riAngle -= 360 * 18;
 #endif
}

void MyGLWidget::setXRotation(int iAngle)
{
    normalizeAngle(iAngle);
    if (iAngle != m_iXRot) {
        m_iXRot = iAngle;
        update();
    }
}

void MyGLWidget::setYRotation(int iAngle)
{
    normalizeAngle(iAngle);
    if (iAngle != m_iYRot) {
        m_iYRot = iAngle;
        update();
    }
}

void MyGLWidget::setZRotation(int iAngle)
{
    normalizeAngle(iAngle);
    if (iAngle != m_iZRot) {
        m_iZRot = iAngle;
        update();
    }
}


void MyGLWidget::mouseDoubleClickEvent(QMouseEvent* pEvent )
{
    // Step 0: 2D Viewport Coordinates between 0:width, 0:height.
    QPoint qptDblClickPos = pEvent->pos();
    qDebug() << "ViewPort Dbl Click Coords: " << qptDblClickPos;

    QVector3D fRayWorld = calculateRayCast( qptDblClickPos );

    float fIntercept = 0;
    float fIntercept2 = 0;
    bool bIntersects = intersectsBoundingSphere( fRayWorld, fIntercept );
    bool bIntersects2 = intersectsBoundingSphere2( fRayWorld, fIntercept2 );


    bool bUpdate = false;
    if ( bIntersects && (fIntercept > fIntercept2 || !bIntersects2) ) {
        qDebug() << " Ray Intersects Bounding Sphere!";
        m_iColorToggle += 1;
        m_iColorToggle = m_iColorToggle % 2;
        bUpdate = true;
    } else if ( bIntersects2 && ( fIntercept2 > fIntercept || !bIntersects ) ) {
        qDebug() << " Ray Intersects Bounding Sphere222!";
        m_iBckColorToggle += 1;
        m_iBckColorToggle = m_iBckColorToggle % 2;
        bUpdate = true;
    }

    if (bUpdate = true) {
        update();
    }
}


QVector3D MyGLWidget::calculateRayCast( QPoint& rqptDblClickPos )
{
    // Step 1: 3D Normalised Device Coords between [-1:1,-1:1,-1:1].
    float fNormDeviceX = ((2.0f *rqptDblClickPos.x())/m_ViewportSize.width()) - 1.0f;
    float fNormDeviceY = 1.0f - ((2.0f *rqptDblClickPos.y())/m_ViewportSize.height());
    float fNormDeviceZ = 1.0f;
    QVector3D fRayNormDevCoords( fNormDeviceX,
                               fNormDeviceY,
                               fNormDeviceZ );
    qDebug() << "Normalized Device Coords: " << fRayNormDevCoords;

    // Step 2: 4D Homogeneous Clip Coordinates between [-1:1,-1:1,-1:1, -1:1].
    QVector4D fRayHomogClipCoords( fRayNormDevCoords.x(),
                                   fRayNormDevCoords.y(),
                                   -1.0, 1.0 );
    qDebug() << "Homogeneous Clip Coords: " << fRayHomogClipCoords;

    // Step 3: 4D Eye (Camera) Coordinates between [-x:x, -y:y, -z:z, -w:w].
    bool bIsInverted = false;
    QMatrix4x4 inverseProjMat = m_Projection.inverted( &bIsInverted );
    QVector4D fRayEye;
    if ( bIsInverted || !inverseProjMat.isIdentity() ) {
        fRayEye = inverseProjMat*fRayHomogClipCoords;
        fRayEye = QVector4D(fRayEye.toVector2D(), -1.0, 0.0);
        qDebug() << " 4D Eye Coords: " << fRayEye;
    } else {
        qDebug() << "Projection matrix could not be inverted!";
        return QVector3D();
    }

    // Step 4: 4D Object Coordinates between [-x:x, -y:y, -z:z, -w:w].
    QMatrix4x4 inverseMVMat = m_ModelView.inverted( &bIsInverted );
    QVector3D fRayWorld;
    if ( bIsInverted || !inverseMVMat.isIdentity() ) {
        QVector4D fRayWorld4D = (inverseMVMat*fRayEye);
        fRayWorld = fRayWorld4D.toVector3D();
        fRayWorld = fRayWorld.normalized();
        qDebug() << " 4D World Coords: " << fRayWorld;
    } else {
        qDebug() << "ModelView matrix could not be inverted!";
        return QVector3D();
    }
    qDebug();
    return fRayWorld;
}


bool MyGLWidget::intersectsBoundingSphere( QVector3D& rfRayWorld, float& fIntercept )
{
    bool isIntersecting = false;

    // Determine center of object and radius.
    QVector4D objCenter4DVec = QVector4D(0, 0, -10, 1 );
    QVector4D camOrigin4DVec = QVector4D(0, 0, 0, 1 );

    bool bIsInverted = false;
    QMatrix4x4 inverseMVMat = m_ModelView.inverted( &bIsInverted );
    QVector3D objCenter3DVec;
    QVector3D camOrigin3DVec;
    if ( bIsInverted || !inverseMVMat.isIdentity() ) {
        objCenter4DVec = (inverseMVMat*objCenter4DVec);
        camOrigin4DVec = (inverseMVMat*camOrigin4DVec);

        objCenter3DVec = objCenter4DVec.toVector3D();
        camOrigin3DVec = camOrigin4DVec.toVector3D();
        qDebug() << "Transform Center of Obj and Camera: ";
    } else {
        qDebug() << "ModelView matrix could not be inverted!";
        return false;
    }



    // Transform center of object and radius by model-view matrix.
    QVector3D vecOMinusC = camOrigin3DVec - objCenter3DVec;

    // Check if ray intersects sphere.
    float fB = QVector3D::dotProduct( rfRayWorld, vecOMinusC );

    float fCOCDotProd = QVector3D::dotProduct(vecOMinusC, vecOMinusC );
    float fC = fCOCDotProd - 1; // NOTE: Don't forget I set the radius = 1.

    qDebug() << "(fB*fB - fC ) " << (fB*fB - fC );
    if ( (fB*fB - fC ) >= 0 ) {
        isIntersecting = true;
        fIntercept = calcLargestIntercept( fB, fC);
    };

    return isIntersecting;

}


bool MyGLWidget::intersectsBoundingSphere2( QVector3D& rfRayWorld, float& fIntercept )
{
    bool isIntersecting = false;

    // Determine center of object and radius.
    QVector4D objCenter4DVec = QVector4D(0, 0, -5, 1 );
    QVector4D camOrigin4DVec = QVector4D(0, 0, 0, 1 );

    bool bIsInverted = false;
    //QMatrix4x4 inverseBckTMat = m_backTranslateMatrix.inverted( &bIsInverted );
    QMatrix4x4 inverseMVMat = m_ModelView.inverted( &bIsInverted );
    QVector3D objCenter3DVec;
    QVector3D camOrigin3DVec;
    if ( bIsInverted || !inverseMVMat.isIdentity() ) {
        //objCenter4DVec = (inverseBckTMat*inverseMVMat*objCenter4DVec);
        objCenter4DVec = (inverseMVMat*objCenter4DVec);
        camOrigin4DVec = (inverseMVMat*camOrigin4DVec);

        objCenter3DVec = objCenter4DVec.toVector3D();
        camOrigin3DVec = camOrigin4DVec.toVector3D();
        qDebug() << "Transform Center of Obj and Camera: ";
    } else {
        qDebug() << "ModelView matrix could not be inverted!";
        return false;
    }



    // Transform center of object and radius by model-view matrix.
    QVector3D vecOMinusC = camOrigin3DVec - objCenter3DVec;

    // Check if ray intersects sphere.
    float fB = QVector3D::dotProduct( rfRayWorld, vecOMinusC );

    float fCOCDotProd = QVector3D::dotProduct(vecOMinusC, vecOMinusC );
    float fC = fCOCDotProd - 2; // NOTE: Don't forget I set the radius = 1.

    qDebug() << "(fB*fB - fC ) " << (fB*fB - fC );
    if ( (fB*fB - fC ) >= 0 ) {
        isIntersecting = true;
        fIntercept = calcLargestIntercept( fB, fC);
    };

    return isIntersecting;

}


float MyGLWidget::calcLargestIntercept( float fB, float fC ) {
    float fBSq2 = fB * fB;
    float fSqrt = float (qSqrt( ( fBSq2 - fC ) ));
    float fT1 = (-fB) + fSqrt;
    float fT2 = (-fB) - fSqrt;
    return qMax( fT1, fT2 );

}



QVector3D MyGLWidget::computeNormals( QVector3D p0, QVector3D p1, QVector3D p2 )
{
    QVector3D vA = p1 - p0;
    vA = vA.normalized();
    QVector3D vB = p2 - p0;
    vB = vB.normalized();
    QVector3D n = QVector3D::crossProduct( vB, vA );
    n = n.normalized();
    qDebug() << "Normal Vector: " << n;
    qDebug();
    return n;
}


void MyGLWidget::collectData( GLfloat pglfData[][3], int iIndex, QVector<QVector3D>& rDataVec )
{
   // qDebug() << "Index: " << iIndex;
   // qDebug() << "QVector3D: " << pglfData[iIndex][0] << " " << pglfData[iIndex][1] << " " <<  pglfData[iIndex][2];
    rDataVec << QVector3D( pglfData[iIndex][0], pglfData[iIndex][1], pglfData[iIndex][2]);
   // qDebug();
}
